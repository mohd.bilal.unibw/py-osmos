"""The config file contains global settings for the simulator.
One can import directly from config to get the desired setting.
"""
from win32api import GetSystemMetrics

# time format to be used across the simulation
TIME_FORMAT = "%Y-%m-%d:%H-%M-%S.%f"

# path to orekit data. note that this only works when the execution is performed from the base directory
OREKIT_DATA = "orekit-data.zip"

# plot settings
SCREEN_DPI = 100

# screen settings
SCREEN_W = GetSystemMetrics(0)
SCREEN_H = GetSystemMetrics(1)
