import numpy as np
import abc
from utils.decorators import initialization_required


# TODO: Not sure if we need base class (e.g., SpaceObject) for physical property calculation
class ISpaceObject(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def initialize(self):
        pass

    @abc.abstractmethod
    def compute_mass(self):
        pass

    @abc.abstractmethod
    def compute_power_consumption(self):
        pass


class SpaceObject(ISpaceObject):
    def __init__(self, name, *args, **kwargs) -> None:
        super().__init__()
        self.name = name

        ## visual characteristics
        self.image = None
        self.cad = None

        ## physical characteristics

        # dimensions
        self.length = 0.0
        self.width = 0.0
        self.height = 0.0

        # inertial properties
        self.mass = 0.0
        self.cg = np.array([[0.0], [0.0], [0.0]])
        self.is_initialized = False

    def initialize(self):
        pass

    def compute_mass(self):
        pass

    def compute_power_consumption(self):
        pass

    def set_image(self, image):
        self.image = image

    def set_length(self, length):
        self.length = length

    def set_width(self, width):
        self.width = width

    def set_height(self, height):
        self.height = height

    def set_mass(self, mass):
        self.mass = mass

    def set_cg(self, cg):
        self.cg = cg

    def get_name(self):
        return self.name

    def get_image(self):
        return self.image

    def get_cad(self):
        return self.cad

    def get_length(self):
        return self.length

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def get_dimensions(self):
        return (self.length, self.width, self.height)

    def get_mass(self):
        return self.mass

    def get_cg(self):
        return self.cg


class Satellite(SpaceObject):
    """This class represents a satellite in a predefined orbit.
    A satellite consists of payload and spacecraft bus. See `payload` and `spacecraft_bus` modules for details.
    The satellite holds reference to an orbit object. The orbit object is used to set the position and velocity of the satellite. 
    Length, height and width are computed based on the component l, b and h.

    Furthermore, the moment of inertial is also computed based on the individual CoM of each component.
    One can set them directly if internal components are not known.

    Note: All units are SI.
    """
    def __init__(self, name, *args, **kwargs):
        super().__init__(name, *args, **kwargs)

        ## orbital state
        self.orbit = None
        self.position = None
        self.velocity = None
        self.acceleration = None
        self.attitude = None

        # inertial properties
        self.moment_of_inertia = np.eye(3)

        ## subparts
        self.bus = None
        self.payloads = None

    def initialize(self):
        """computes the properties for components and performs necessary initializations.
        """
        self.compute_mass()
        self.compute_moment_of_inertia()
        self.is_initialized = True

    def compute_mass(self):
        self.mass = self.bus.get_mass() + np.sum(
            [p.get_mass() for p in self.payloads])

    def compute_power_consumption(self):
        pass

    def set_orbit(self, orbit):
        self.orbit = orbit

    @initialization_required
    def get_groundtrack(self):
        return self.orbit.get_ground_track()

    def add_event_handler(self, name):
        pass

    def compute_moment_of_inertia(self):
        pass

    @initialization_required
    def propagate(self, start_time, end_time):
        self.orbit.propagate(start_time, end_time)

    # TODO: how to add event handlers
