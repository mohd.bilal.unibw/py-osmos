import abc
import numpy as np
import logging
from mission.space_segment import SpaceObject

logger = logging.getLogger(__name__)


class IPayload(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def operate(self):
        pass


class Payload(SpaceObject, IPayload):
    def __init__(self, name, *args, **kwargs) -> None:
        super(SpaceObject, self).__init__(name, *args, **kwargs)
        super(IPayload, self).__init__()

    def operate(self):
        pass

    def compute_mass(self):
        raise NotImplementedError

    def initialize(self):
        """computes the properties for components and performs necessary initializations.
        """
        self.compute_mass()
        self.is_initialized = True