import abc


class FieldOfView:
    def __init__(self, type_, **kwargs):
        self.type_ = type_

    def get_type(self):
        return self.type_


class ISensor(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def compute_footprint(self, central_body):
        pass


class Sensor(ISensor):
    def __init__(self, name, fov, **kwargs):
        self.name = name
        self.fov = fov

    def get_name(self):
        return self.name

    def get_fov(self):
        return self.fov

    def compute_footprint(self, central_body):
        pass
