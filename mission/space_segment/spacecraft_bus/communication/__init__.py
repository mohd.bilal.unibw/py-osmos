from mission.space_segment import SpaceObject
import logging

logger = logging.getLogger(__name__)


class CommunicationSS(SpaceObject):
    def __init__(self, name, *args, **kwargs) -> None:
        super(SpaceObject, self).__init__(name, *args, **kwargs)

    def compute_link_budget(self, distance, **kwargs):
        raise NotImplementedError

    def compute_mass(self):
        raise NotImplementedError

    