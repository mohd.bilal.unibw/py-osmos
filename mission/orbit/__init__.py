import abc
import config
import datetime


class IOrbit(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def propagate(self):
        pass


class BaseOrbit(IOrbit):
    def __init__(self, central_body, *args, **kwargs):
        self.central_body = central_body
        self.propagator = None
        self.sma = None
        self.eccentricity = None
        self.inclination = None
        self.omega = None
        self.raan = None
        self.true_anomaly = None
        self.subsatellite_point = None
        self.groundtracks = []
        super().__init__()

    def get_central_body(self):
        return self.central_body

    def get_keplerian_elements(self):
        pass

    def _compute_groundtrack(self):
        pass

    def get_groundtrack(self, start=None, end=None):
        if start is None:
            return self.groundtracks

        if end is None:
            return list(
                filter(
                    lambda g: datetime.strptime(g.keys()[0], config.TIMEFORMAT)
                    >= start,
                    self.groundtracks,
                )
            )

        return list(
            filter(
                lambda g: datetime.strptime(g.keys()[0], config.TIMEFORMAT) >= start
                and datetime.strptime(g.keys()[0], config.TIMEFORMAT) <= end,
                self.groundtracks,
            )
        )

    def get_subsatellite_point(self):
        return self.subsatellite_point

    def propagate(self):
        pass
