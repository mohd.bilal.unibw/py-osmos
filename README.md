# Py-OSMOS
Py-OSMOS stands for Python Open Source Mission and Orbit Simulator
This is the repository for the mission simulator. The following steps show how to set up the repository and get it to work.

## Download, install and setup Git

### Download Git
The first step is to download git on your PC and set up the credentials. Git can be downloaded from the link below.

<a href="https://git-scm.com/downloads">GIT SCM</a>


### Set up Git
In order to avoid providing git credentials every time you need to push, run the following command from the terminal.

`git config --global user.name "Your Gitlab Username"`

`git config --global user.email yourgitlabemail`

## Clone the repository
Clone the repository in a directory of your choice. Run the following command from the terminal.

`git clone https://gitlab.com/mohd.bilal.unibw/seranis_mission_simulator.git`

## Install Python
Download  and install Python 3.9 or 3.8 from the link below.

<a href="https://www.python.org/downloads/">Python Download</a>

## Install Conda
Download Anaconda (not Miniconda) from the link below.

<a href="https://docs.conda.io/projects/conda/en/latest/user-guide/install/download.html">Anaconda Download</a>

## Create a Python Virtual Environment
Run the Anaconda Terminal by searching for Anaconda in your Windows application search bar. Create a virtual environment avoids bloating up the 
system python version with too many packages.

`conda create --name venv_seranis_simulator python=3.8`

Activate the environment.

`conda activate venv_seranis_simulator`

## Install Required Packages
The requirements.txt file is currently not working. Shall be updated soon. To get started run the following commands. 

`conda install numpy`

`conda install jupyterlab`

`conda install matplotlib`

`conda install cartopy`

`conda install -c conda-forge orekit`

## Fire up Jupyter
Change to the directory where you cloned the repository. Start jupyter with the following command. You can also use the built-in notebook viewer in VSCode.

`jupyter lab`

## VS Code 
VS Code can be downloaded from here.

<a href="https://code.visualstudio.com/">Visual Studio Code</a>

## Work Flow on Git
After you have cloned the repository and want to commit changes to the repository, follow the steps below.

### Pull the remote changes

`git pull origin branchname_local:branchname_origin`

`branchname_local` is the name of the local branch you wish to pull to.

`branchname_origin` is the name of the remote branch you wish to pull from.

### Create a new branch

`git checkout -b branchname`

This creates a local branch on your PC and not on the remote directory (whose default name is origin). Make your changes on the local branch.

### Stage changes

`git add .`

This adds the files you changed to the staging area. This change is not yet committed.

### Commit changes

`git commit -m "your message"`

`NOTE:` Do not forget the double quotes on the message.

### Push your changes to the remote directory (hosted on gitlab)

`git push origin branchname_local:branchname_origin`

`branchname_local` is the name of the local branch you wish to push from.

`branchname_origin` is the name of the remote branch you wish to push to.

This will push the changes to the remote branch.

### Create a Merge Request

Visit your GitLab repository and create a merge request by clicking the button prompted by GitLab. Once this is done the reviewer can merge the changes to the `main` branch.


**NOTE**: Let us try not to make changes directly to the main branch and follow the procedure above.
