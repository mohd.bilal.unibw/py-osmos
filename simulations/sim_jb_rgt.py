## orekit wrapper imports
from orekit.pyhelpers import setup_orekit_curdir
from utils.astro.thin_orekit_wrapper.globals import VM
from utils.astro.thin_orekit_wrapper.orbits.special_orbits import RepeatingGroundTrackOrbit

## simulator imports
from scenario import SimpleScenario
from scenario import ReportTypes

## other imports
from simulations import setup_logger
from simulations import default_forces_options
from simulations import ka_band_antenna

## python imports
import logging
import datetime
import numpy as np
import sys

logger = logging.getLogger("SIMULATION_SpaceTech")
logger.setLevel(logging.DEBUG)


def main():
    logger.info("starting simulation")

    ## set up simulation times
    start_time = datetime.datetime(2024, 1, 1)
    logger.info(f"Simulation start time is  {start_time}")
    duration = 4 * 60 * 60
    end_time = start_time + datetime.timedelta(seconds=duration)

    ## create a scenario of given name
    scenario = SimpleScenario("JB_RGT_Scenario", start_time, end_time)

    # fetch body, might be used later. default is Earth
    central_body = scenario.get_central_body()

    # add a satellite to the scenario
    seranis = scenario.add_satellite("SeRANIS")
    seranis.set_mass(100.0)
    seranis.set_dimensions(0.58, 0.45, 0.41)
    seranis.set_solar_array_area(1.5 * 1.6)
    seranis.set_solar_array_normal(np.array([0, 1, 0]))
    seranis.set_solar_array_axis(np.array([1, 0, 0]))
    seranis.set_solar_array_efficiency(0.2)
    seranis.add_environment_forces(default_forces_options)

    # add a station to the scenario
    neubiberg = scenario.add_station("Neubiberg", np.deg2rad(48.0747),
                                     np.deg2rad(11.6715), np.rad2deg(100))

    # TODO: update initial conditions using special orbits
    orbit_constraints = dict(
        subsatellite=dict(latitude=neubiberg.get_latitude(),
                          longitude=neubiberg.get_longitude(),
                          direction="descending"))
    seranis_orbit_start = RepeatingGroundTrackOrbit.get_minimum_eccentricity_rgto(
        np.deg2rad(50),
        perigee_height=500e3,
        body=central_body,
        constraints=orbit_constraints,
        epoch=start_time)

    logger.info(
        f"seranis initial orbit set to: \n {seranis_orbit_start.get_keplerian_elements()}"
    )
    logger.info(
        f"seranis initial orbit: days after which ground track repeats: {seranis_orbit_start.k_days2rep}"
    )
    logger.info(
        f"seranis initial orbit: revs after which ground track repeats: {seranis_orbit_start.k_revs2rep}"
    )
    seranis_attitude_law = "NADIR_POINTING"
    seranis_initial_state = seranis_orbit_start.get_keplerian_elements()
    seranis_initial_state.update({"attitude_law": seranis_attitude_law})

    # set initial state of the spacecraft
    seranis.set_initial_state(start_time, seranis_initial_state)

    # add sensors to the spacecraft
    scenario.add_space_sensor(seranis.get_name(), ka_band_antenna.get("name"),
                              ka_band_antenna.get("fov").get("fov_type"),
                              ka_band_antenna.get("fov").get("fov_options"))

    reports_options = {
        ReportTypes.GS_ACCESS.name: [{
            "station": "Neubiberg",
            "satellite": "SeRANIS"
        }],
        ReportTypes.ECLIPSE.name: ["SeRANIS"],
        ReportTypes.GROUND_TRACK.name: ["SeRANIS"],
        ReportTypes.SENSOR_COVERAGE.name: [{
            "satellite":
            "SeRANIS",
            "sensor":
            ka_band_antenna.get("name")
        }],
        ReportTypes.EPHEMERIS.name: ["SeRANIS"]
    }

    #  add reports to be calculated in one go
    scenario.add_reports(reports_options)

    # simulate
    data = scenario.simulate()

    # export reports
    scenario.export_access_report(seranis.get_name(), neubiberg.get_name())
    scenario.export_eclipse_report(seranis.get_name())
    scenario.export_ground_track_report(seranis.get_name())
    scenario.export_sensor_coverage_report(seranis.get_name(),
                                           ka_band_antenna.get("name"))
    scenario.export_ephemeris_report(seranis.get_name())

    # generate pdf
    # try:
    #     scenario.generate_document("Mission Analysis Document", 1.0,
    #                                f"simulations/docs/{scenario.get_name()}")
    # except:
    #     logger.info("pylatex exceptions are not handled.")
    scenario.generate_document("Mission Analysis Document", 1.0,
                               f"simulations/docs/{scenario.get_name()}")
    
    # plot
    scenario.plot_access_report(seranis.get_name(), neubiberg.get_name())
    scenario.plot_eclipse_report(seranis.get_name())
    scenario.plot_ground_track(seranis.get_name())
    scenario.plot_sensor_coverage_report(seranis.get_name(),
                                         ka_band_antenna.get("name"),
                                         filled=True)
    scenario.plot_power_report(seranis.get_name())
    scenario.plot_ephemeris(seranis.get_name())
    scenario.show_plots()


if __name__ == "__main__":
    setup_logger(logger)
    main()