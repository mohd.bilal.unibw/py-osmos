\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{seranis}[2014/08/16 Example LaTeX class]
\newcommand{\headlinecolor}{\normalcolor}
\newcommand{\stextcolor}{\normalcolor}
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{graphicx} 
\RequirePackage{ragged2e}
\RequirePackage{blindtext}
\newcommand{\version}[1]{Version #1}
\definecolor{titlecolor}{RGB}{100, 99, 99}
\definecolor{scolor}{RGB}{235,119,52}
\renewcommand{\headlinecolor}{\color{titlecolor}}

\LoadClass[titlepage]{article} 
\RequirePackage[a4paper,top=2.0cm,bottom=2cm,left=2.5cm,right=2.5cm]{geometry}
\setlength\headheight{45pt}
\RequirePackage{fancyhdr}

\renewcommand{\maketitle}{%
        \onecolumn
        \newgeometry{top=0cm,left=0cm,right=0cm}
        \begin{center}
            \includegraphics[width=\textwidth]{../../utils/reports/seranis_logos/seranis_banner.png}
        \end{center}
    \vspace{0.5in}
            \begin{center}
            \headlinecolor
        {\fontsize{26}{12} \fontseries{b}\selectfont
                \@title}  \\
                \vspace{5mm}
                {\fontsize{12}{12}\selectfont
                Seamless Radio Access Networks for Internet and Space (SeRANIS)}
            \end{center}
            \vspace{40mm}
        \newpage
        \restoregeometry
        % \stextcolor
        \setcounter{page}{1}
}
\renewcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                             {\normalfont\Large\bfseries\headlinecolor}}
\RequirePackage{hyperref}
\fancypagestyle{firstpage}{%
\fancyhf{} % clear all six fields
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
}
\fancypagestyle{followingpage}{%
\fancyhf{} % clear all six fields
\fancyhead[L]{\includegraphics[height=1.44cm]{../../utils/reports/seranis_logos/seranis_logo_black.png}}
\fancyhead[R]{\@title \\ \version{1} $\vert$ \@date}
\fancyfoot[R]{\thepage}
\renewcommand{\headrulewidth}{0.7pt}
\renewcommand{\footrulewidth}{0.7pt}
}
\AtBeginDocument{\thispagestyle{firstpage}}
\pagestyle{followingpage}
