import numpy as np
import logging
import sys


def setup_logger(logger):
    formatter = logging.Formatter(
        fmt='%(levelname)s: %(name)s: [%(asctime)s] : %(message)s',
        datefmt='%d-%b-%Y %H:%M:%S')
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logger.getEffectiveLevel())
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)


# define options for forces to be used in the simulation
default_forces_options = dict(gravity=dict(degree=21, order=21),
                              drag=dict(harris_priester=dict(cosine=4.0)),
                              lunar_3b=True,
                              solar_3b=True,
                              srp=True)

# define sensor properties
ka_band_antenna = {
    "name": "Ka_Band_Antenna",
    "fov": {
        "fov_type": "ELLIPTICAL_FOV",
        "fov_options": {
            "orientation_axis": np.array([0, 0, 1]),
            "ellipse_major_axis": np.array([1, 0, 0]),
            "half_cone_x": np.deg2rad(72),
            "half_cone_y": np.deg2rad(42),
            "margin_angle": np.deg2rad(0.1)
        }
    }
}

l_band_antenna = {
    "name": "L_Band_Antenna",
    "fov": {
        "fov_type": "ELLIPTICAL_FOV",
        "fov_options": {
            "orientation_axis": np.array([0, 0, 1]),
            "ellipse_major_axis": np.array([1, 0, 0]),
            "half_cone_x": np.deg2rad(72),
            "half_cone_y": np.deg2rad(72),
            "margin_angle": np.deg2rad(0.1)
        }
    }
}

uhf_vhf_band_antenna = {
    "name": "UHF_VHF_Antenna",
    "fov": {
        "fov_type": "ELLIPTICAL_FOV",
        "fov_options": {
            "orientation_axis": np.array([0, 0, 1]),
            "ellipse_major_axis": np.array([1, 0, 0]),
            "half_cone_x": np.deg2rad(68),
            "half_cone_y": np.deg2rad(68),
            "margin_angle": np.deg2rad(0.1)
        }
    }
}

ir_camera_a = {
    "name": "IR Camera Orbit A",
    "fov": {
        "fov_type": "ELLIPTICAL_FOV",
        "fov_options": {
            "orientation_axis": np.array([0, 0, 1]),
            "ellipse_major_axis": np.array([1, 0, 0]),
            "half_cone_x": np.deg2rad(15.757/2),
            "half_cone_y": np.deg2rad(15.757/2),
            "margin_angle": np.deg2rad(0.1)
        }
    }
}

ir_camera_b = {
    "name": "IR Camera Orbit B",
    "fov": {
        "fov_type": "ELLIPTICAL_FOV",
        "fov_options": {
            "orientation_axis": np.array([0, 0, 1]),
            "ellipse_major_axis": np.array([1, 0, 0]),
            "half_cone_x": np.deg2rad(14.143/2),
            "half_cone_y": np.deg2rad(14.143/2),
            "margin_angle": np.deg2rad(0.1)
        }
    }
}

optical_camera_a = {
    "name": "Optical Camera Orbit A",
    "fov": {
        "fov_type": "ELLIPTICAL_FOV",
        "fov_options": {
            "orientation_axis": np.array([0, 0, 1]),
            "ellipse_major_axis": np.array([1, 0, 0]),
            "half_cone_x": np.deg2rad(7.915/2),
            "half_cone_y": np.deg2rad(7.915/2),
            "margin_angle": np.deg2rad(0.1)
        }
    }
}

optical_camera_b = {
    "name": "Optical Camera Orbit B",
    "fov": {
        "fov_type": "ELLIPTICAL_FOV",
        "fov_options": {
            "orientation_axis": np.array([0, 0, 1]),
            "ellipse_major_axis": np.array([1, 0, 0]),
            "half_cone_x": np.deg2rad(7.0986/2),
            "half_cone_y": np.deg2rad(7.0986/2),
            "margin_angle": np.deg2rad(0.1)
        }
    }
}