## orekit wrapper imports
from orekit.pyhelpers import setup_orekit_curdir
from utils.astro.thin_orekit_wrapper.globals import VM
from utils.astro.thin_orekit_wrapper.orbits.special_orbits import SSO
from utils.astro.helpers import time_period_to_sma

## simulator imports
from scenario import SimpleScenario
from scenario import ReportTypes

## other imports
from simulations import setup_logger
from simulations import default_forces_options

## python imports
import logging
import datetime
import numpy as np
import sys

logger = logging.getLogger("SIMULATION_VALIDATION")
logger.setLevel(logging.DEBUG)


def main():
    logger.info("starting simulation")

    ## set up simulation times
    year = 2021
    start_time = datetime.datetime(2021, 10, 18, int(0.71108039*24))

    logger.info(f"Simulation start time is  {start_time}")
    duration = 2 * 24 * 60 * 60
    end_time = start_time + datetime.timedelta(seconds=duration)

    ## create a scenario of given name
    scenario = SimpleScenario("MB_SSO_VALIDATION", start_time, end_time)

    # fetch body, might be used later. default is Earth
    central_body = scenario.get_central_body()

    # add a satellite to the scenario
    seranis = scenario.add_satellite("SeRANIS")
    seranis.set_mass(100.0)
    seranis.set_dimensions(0.58, 0.45, 0.41)
    seranis.set_solar_array_area(1.5 * 1.6)
    seranis.set_solar_array_normal(np.array([0, -1, 0]))
    seranis.set_solar_array_axis(np.array([1, 0, 0]))
    seranis.set_solar_array_efficiency(0.2)
    seranis.add_environment_forces(default_forces_options)

    # add a station to the scenario
    neubiberg = scenario.add_station("Neubiberg", np.deg2rad(48.0747),
                                     np.deg2rad(11.6715), np.rad2deg(100))

    mean_motion = 14.97049507*2*np.pi/86400 # radians per second
    time_period = 2*np.pi/mean_motion
    print(time_period/60)
    sma = time_period_to_sma(time_period, central_body.get_mu())
    eccentricity = 0.0013697
    inclination = np.deg2rad(97.6771)
    raan = np.deg2rad(197.2816)
    perigee = np.deg2rad(54.5997)
    mean_anomaly = np.deg2rad(305.6502)

    
    seranis_attitude_law = "NADIR_POINTING"
    seranis_initial_state = {
            "sma": sma,
            "inclination": inclination,
            "eccentricity": eccentricity,
            "raan": raan,
            "perigee": perigee,
            "mean_anomaly": mean_anomaly,
            "epoch": start_time
        }
    seranis_initial_state.update({"attitude_law": seranis_attitude_law})
    logger.info(
        f"seranis initial orbit set to: \n {seranis_initial_state} \n"
    )

    # set initial state of the spacecraft
    seranis.set_initial_state(start_time,
                              seranis_initial_state)
    print(seranis.get_current_time())

    reports_options = {
        ReportTypes.GS_ACCESS.name: [{
            "station": "Neubiberg",
            "satellite": "SeRANIS"
        }],
        ReportTypes.ECLIPSE.name: ["SeRANIS"]
    }

    #  add reports to be calculated in one go
    scenario.add_reports(reports_options)

    # simulate
    data = scenario.simulate()

    # export reports
    scenario.export_access_report(seranis.get_name(), neubiberg.get_name())

    # plot
    scenario.plot_access_report(seranis.get_name(), neubiberg.get_name())
    scenario.plot_eclipse_report(seranis.get_name())
    scenario.plot_ground_track(seranis.get_name())
    scenario.plot_power_report(seranis.get_name())
    scenario.show_plots()


if __name__ == "__main__":
    setup_logger(logger)
    main()