from mission.space_segment import Satellite
from utils.astro import AstroProviders
from utils.astro.thin_orekit_wrapper.ground_objects import GroundObject
from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft
from utils.astro.thin_orekit_wrapper.events import EventType
from utils.astro.thin_orekit_wrapper.bodies import Earth
from config import TIME_FORMAT
from utils.reports import AccessReport, EclipseReport
from utils.reports import GroundTrackReport, SensorCoverageReport
from utils.reports import EphemerisReport
from utils.reports.report_makers import SingleSatMissionReportMaker
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import threading
import enum
import json
import copy

import logging

logger = logging.getLogger(__name__)

class ReportTypes(enum.Enum):
    GS_ACCESS = 0
    ECLIPSE = 1
    SENSOR_COVERAGE = 2
    GROUND_TRACK = 3
    POWER = 4
    EPHEMERIS = 5

class SimpleScenario:
    def __init__(self, name, start_time, end_time, *args, **kwargs):
        self.name = name
        self.start_time = start_time
        self.end_time = end_time
        self.duration = (self.end_time - self.start_time).total_seconds()
        self.satellites = []
        self.stations = []
        self.observation_points = []
        self.central_body = kwargs["central_body"] if "central_body" in kwargs else Earth()
        self.environment_forces = []
        self.has_graphics = False
        self.settings = {}
        self.is_initialized = False
        self.access_reports = []
        self.eclipse_reports = []
        self.ground_track_reports = []
        self.sensor_coverage_reports = []
        self.power_reports = []
        self.ephemeris_reports = []
        self.reports = {
            ReportTypes.GS_ACCESS.name: [],
            ReportTypes.ECLIPSE.name: [],
            ReportTypes.SENSOR_COVERAGE.name: [],
            ReportTypes.POWER.name: [],
            ReportTypes.EPHEMERIS.name: []
        }
        self.reports_options = {
            ReportTypes.GS_ACCESS.name: [],
            ReportTypes.ECLIPSE.name: [],
            ReportTypes.SENSOR_COVERAGE.name: [],
            ReportTypes.POWER.name: [],
            ReportTypes.EPHEMERIS.name: []
        }
        self.single_sat_report_maker = None
        self.is_threaded = kwargs['threaded'] if 'threaded' in kwargs else False
        self.astro_provider = kwargs['astro_provider'] if 'astro_provider' in kwargs else AstroProviders.OREKIT

    def _initialize(self):
        self.is_initialized = True

    def get_name(self):
        return self.name

    def get_central_body(self):
        return self.central_body
    
    def get_station(self, name):
        stations = [s for s in self.stations if s.get_name()==name]
        if not stations:
            return None
        return stations[0]
    
    def get_stations(self):
        return self.stations
    
    def get_satellite(self, name):
        satellites = [s for s in self.satellites if s.get_name()==name]
        if not satellites:
            return None
        return satellites[0]

    def get_satellites(self):
        return self.satellites

    def get_reports_options(self):
        return self.reports_options
    
    def get_reports(self):
        return self.reports

    def get_access_report(self, satellite_name, station_name):
        report = [r for r in self.access_reports if r.get_satellite_name()==satellite_name and r.get_station_name()==station_name]
        if not report:
            return None
        return report[0]

    def get_eclipse_report(self, satellite_name):
        report = [r for r in self.eclipse_reports if r.get_satellite_name()==satellite_name]
        if not report:
            return None
        return report[0]
    
    def get_ground_track_report(self, satellite_name):
        report = [r for r in self.ground_track_reports if r.get_satellite_name()==satellite_name]
        if not report:
            return None
        return report[0]

    def get_coverage_report(self, satellite_name, sensor_name):
        report = [r for r in self.sensor_coverage_reports if r.get_satellite_name()==satellite_name and r.get_sensor_name()==sensor_name]
        if not report:
            return None
        return report[0]

    def get_power_report(self, satellite_name):
        report = [r for r in self.reports.get(ReportTypes.POWER.name) if r.get("satellite")==satellite_name]
        if not report:
            return None
        return report[0]

    def get_ephemeris_report(self, satellite_name):
        report = [r for r in self.ephemeris_reports if r.get_satellite_name()==satellite_name]
        if not report:
            return None
        return report[0]     
        
    def set_central_body(self, body, update_satellites = False, update_stations = False):
        self.central_body = body
        if update_satellites:
            for satellite in self.satellites:
                satellite.set_central_body(body)
        if update_stations:
            for station in self.stations:
                station.set_body(body)
    
    def add_station(self, name, latitude, longitude, altitude, body=None, **kwargs):
        if body is None:
            logger.info("No celestial body was provided. Setting body to body of scenario.")    
            body = self.central_body
        station = GroundObject(name, body, latitude, longitude, altitude, **kwargs)
        self.stations.append(station)
        return station

    def add_stations(self, stations):
        self.stations.extend(stations)

    def add_satellite(self, name, body=None, **kwargs):
        if body is None:
            logger.info("No celestial body was provided. Setting body to body of scenario.")
            body = self.central_body
        spacecraft = Spacecraft(name, central_body = body, **kwargs)
        self.satellites.append(spacecraft)
        return spacecraft

    def add_satellites(self, satellites):
        self.satellites.extend(satellites)

    def add_space_sensor(self, satellite_name, sensor_name, fov_type, fov_options, **kwargs):
        satellite = self.get_satellite(satellite_name)
        sensor = satellite.add_sensor(sensor_name, fov_type, fov_options, **kwargs)
        return sensor

    def add_access_report(self, satellite_name, station_name):
        satellite = self.get_satellite(satellite_name)
        station = self.get_station(station_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        if station is None:
            raise Exception("The station you searched for does not exist in the scenario.")
        existing_access = self.reports_options.get(ReportTypes.GS_ACCESS.name)
        access = {"station":station_name, "satellite": satellite_name}
        if access in existing_access:
            return None
        existing_access.append(access)
        scenario_info =  {
            "name": self.name,
            "start": self.start_time,
            "end": self.end_time
        }
        satellite_data = {
            "name": satellite_name,
            "initial_conditions": satellite.get_initial_keplerian_state().copy(),
        }
        station_data = {
                "name": station_name,
                "latitude": np.rad2deg(station.get_latitude()),
                "longitude": np.rad2deg(station.get_longitude())
            }
        access_report = AccessReport(scenario_info, satellite_data, station_data)
        self.access_reports.append(access_report)
        detector = satellite.add_event_handler(EventType.ELEVATION_DETECTOR, self.elevation_handler, station=station)
        return None

    def add_eclipse_report(self, satellite_name):
        satellite = self.get_satellite(satellite_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        existing_eclipse = self.reports_options.get(ReportTypes.ECLIPSE.name)
        if satellite_name in existing_eclipse:
            return None
        existing_eclipse.append(satellite_name)
        scenario_info =  {
            "name": self.name,
            "start": self.start_time,
            "end": self.end_time
        }
        satellite_data = {
            "name": satellite_name,
            "initial_conditions": satellite.get_initial_keplerian_state().copy(),
        }
        eclipse_report = EclipseReport(scenario_info, satellite_data)
        self.eclipse_reports.append(eclipse_report)
        detector = satellite.add_event_handler(EventType.ECLIPSE_ENTER_DETECTOR, self.eclipse_enter_handler)
        detector = satellite.add_event_handler(EventType.ECLIPSE_EXIT_DETECTOR, self.eclipse_exit_handler)
        return None
    
    def add_ground_track_report(self, satellite_name, show_stations=True):
        satellite = self.get_satellite(satellite_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        scenario_info =  {
            "name": self.name,
            "start": self.start_time,
            "end": self.end_time
        }
        satellite_data = {
            "name": satellite_name,
            "initial_conditions": satellite.get_initial_keplerian_state().copy(),
        }
        station_data = []
        ground_track_tuple = satellite.get_ground_track().copy()
        gt = []
        for time_, latitude, longitude in ground_track_tuple:
            gt.append({
                "time": time_,
                "latitude": np.rad2deg(latitude),
                "longitude": np.rad2deg(longitude)
            })
        if show_stations:
            station_data = [{"name": s.get_name(), "latitude": np.rad2deg(s.get_latitude()), "longitude": np.rad2deg(s.get_longitude())}for s in self.stations]
        report = GroundTrackReport(scenario_info, satellite_data, station_data, report_data=gt)
        self.ground_track_reports.append(report)
        return None

    def add_sensor_coverage_report(self, satellite_name, sensor_name, show_stations=True):
        satellite = self.get_satellite(satellite_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        scenario_info =  {
            "name": self.name,
            "start": self.start_time,
            "end": self.end_time
        }
        satellite_data = {
            "name": satellite_name,
            "initial_conditions": satellite.get_initial_keplerian_state().copy(),
        }
        sensor_data = {
            "name": sensor_name
        }
        station_data = []
        ground_track_tuple = satellite.get_ground_track().copy()
        gt = []
        for time_, latitude, longitude in ground_track_tuple:
            gt.append({
                "time": time_,
                "latitude": np.rad2deg(latitude),
                "longitude": np.rad2deg(longitude)
            })
        if show_stations:
            station_data = [{"name": s.get_name(), "latitude": np.rad2deg(s.get_latitude()), "longitude": np.rad2deg(s.get_longitude())}for s in self.stations]
        fps = satellite.get_sensor_footprints_all().get(sensor_name).copy() if satellite.get_sensor_footprints_all().get(sensor_name) else []
        for fp in fps:
            for point in fp:
                point["latitude"] = np.rad2deg(point["latitude"])
                point["longitude"] = np.rad2deg(point["longitude"])
        report = SensorCoverageReport(scenario_info, satellite_data, sensor_data, station_data=station_data, ground_track_data=gt, report_data=fps)
        self.sensor_coverage_reports.append(report)
        return None

    def add_ephemeris_report(self, satellite_name):
        satellite = self.get_satellite(satellite_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.") 
        scenario_info =  {
            "name": self.name,
            "start": self.start_time,
            "end": self.end_time
        }
        satellite_data = {
            "name": satellite_name,
            "initial_conditions": satellite.get_initial_keplerian_state().copy(),
        }
        ephemeris = copy.deepcopy(satellite.get_propagated_keplerian_elements())
        report = EphemerisReport(scenario_info, satellite_data, report_data=ephemeris)
        self.ephemeris_reports.append(report)

    def add_power_report(self, satellite_name):
        satellite = self.get_satellite(satellite_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.") 
        power_data = {
            "satellite": satellite_name,
            "data": {
                "power": []
            }
        }
        self.reports.get(ReportTypes.POWER.name).append(power_data)
        return None

    def add_reports(self, reports_options):
        access_reports = reports_options.get(ReportTypes.GS_ACCESS.name)
        eclipse_reports = reports_options.get(ReportTypes.ECLIPSE.name)
        ground_track_reports = reports_options.get(ReportTypes.GROUND_TRACK.name)
        sensor_coverage_reports = reports_options.get(ReportTypes.SENSOR_COVERAGE.name)
        power_reports = reports_options.get(ReportTypes.POWER.name)
        ephemeris_reports = reports_options.get(ReportTypes.EPHEMERIS.name)
        if access_reports is not None:
            action = [self.add_access_report(r["satellite"], r["station"]) for r in access_reports]
        if eclipse_reports is not None:
            action = [self.add_eclipse_report(e) for e in eclipse_reports]
        if ground_track_reports is not None:
            action = [self.add_ground_track_report(g) for g in ground_track_reports]
        if sensor_coverage_reports is not None:
            action = [self.add_sensor_coverage_report(c["satellite"], c["sensor"]) for c in sensor_coverage_reports]
        if power_reports is not None:
            action = [self.add_power_report(s) for s in power_reports]
        if ephemeris_reports is not None:
            action = [self.add_ephemeris_report(s) for s in ephemeris_reports]
        return self.reports_options
    
    def export_access_report(self, satellite_name, station_name, fname="", fpath="", fmt="json"):
        report = self.get_access_report(satellite_name, station_name)
        if report:
            report.export(fname=fname, fpath=fpath, fmt=fmt)
        
    def export_eclipse_report(self, satellite_name, fname="", fpath="", fmt="json"):
        report = self.get_eclipse_report(satellite_name)
        if report:
            report.export(fname=fname, fpath=fpath, fmt=fmt)

    def export_ground_track_report(self, satellite_name, fname="", fpath="", fmt="json"):
        satellite = self.get_satellite(satellite_name)
        if not satellite:
            logger.info("Could not export ground track. No satellite found.")
            return None
        report = self.get_ground_track_report(satellite_name)
        if not report:
            logger.info("Could not export ground track. No report found.")
            return None
        ground_track = satellite.get_ground_track().copy()
        gt = []
        for time_, latitude, longitude in ground_track:
            gt.append({
                "time": time_,
                "latitude": np.rad2deg(latitude),
                "longitude": np.rad2deg(longitude)
            })
        self.update_ground_track_report(satellite_name, gt, replace=True)
        report.export(fname=fname, fpath=fpath, fmt=fmt)
    
    def export_sensor_coverage_report(self, satellite_name, sensor_name, fname="", fpath="", fmt="json"):
        satellite = self.get_satellite(satellite_name)
        if not satellite:
            logger.info("Could not export sensor data. No satellite found.")
            return None
        report = self.get_coverage_report(satellite_name, sensor_name)
        ground_track = satellite.get_ground_track().copy()
        gt = []
        for time_, latitude, longitude in ground_track:
            gt.append({
                "time": time_,
                "latitude": np.rad2deg(latitude),
                "longitude": np.rad2deg(longitude)
            })
        fps = satellite.get_sensor_footprints_all().copy()[sensor_name]
        fps_c = copy.deepcopy(fps)
        if not fps:
            raise Exception(f"{sensor_name} footprints not found. Check sensor name.")
        
        for fp in fps_c:
            for point in fp:
                point["latitude"] = np.rad2deg(point["latitude"])
                point["longitude"] = np.rad2deg(point["longitude"])
        self.update_sensor_coverage_report(satellite_name, sensor_name, gt, 'ground_track', replace=True)
        self.update_sensor_coverage_report(satellite_name, sensor_name, fps_c, 'coverage', replace=True)

        if report:
            report.export(fname=fname, fpath=fpath, fmt=fmt)

    def export_ephemeris_report(self, satellite_name, fname="", fpath="", fmt="json"):
        satellite = self.get_satellite(satellite_name)
        if not satellite:
            logger.info("Could not export ephemeris. No satellite found.")
            return None

        ephemeris = copy.deepcopy(satellite.get_propagated_keplerian_elements())
        self.update_ephemeris_report(satellite_name, ephemeris, replace=True)
        report = self.get_ephemeris_report(satellite_name)
        if report:
            report.export(fname=fname, fpath=fpath, fmt=fmt)

    def generate_document(self,  title, version, filepath, latex=False, pdf=True, **kwargs):
        self.single_sat_report_maker = SingleSatMissionReportMaker(self, title, version, **kwargs)
        self.single_sat_report_maker.create_document(filepath=filepath, latex=latex, pdf=pdf)

    def plot_access_report(self, satellite_name, station_name, plot_duration=None, **kwargs):
        report = self.get_access_report(satellite_name, station_name)
        if not report.get_accesses():
            return None
        report.plot_access_bar()
        report.plot_access_histogram()

    def plot_eclipse_report(self, satellite_name, plot_duration=None, **kwargs):
        report = self.get_eclipse_report(satellite_name)
        if not report.get_eclipses():
            return None
        report.plot_eclipse_bar(plot_duration=plot_duration, **kwargs)
        report.plot_eclipse_histogram(plot_duration=plot_duration, **kwargs)
        
    def plot_sensor_coverage_report(self, satellite_name, sensor_name, filled=False, **kwargs):
        satellite = self.get_satellite(satellite_name)
        if not satellite:
            logger.info("Could not export sensor data. No satellite found.")
            return None
        report = self.get_coverage_report(satellite_name, sensor_name)
        ground_track = satellite.get_ground_track().copy()
        gt = []
        for time_, latitude, longitude in ground_track:
            gt.append({
                "time": time_,
                "latitude": np.rad2deg(latitude),
                "longitude": np.rad2deg(longitude)
            })
        fps = copy.deepcopy(satellite.get_sensor_footprints_all()[sensor_name])
        if not fps:
            raise Exception(f"{sensor_name} footprints not found. Check sensor name.")
        
        for fp in fps:
            for point in fp:
                point["latitude"] = np.rad2deg(point["latitude"])
                point["longitude"] = np.rad2deg(point["longitude"])
        self.update_sensor_coverage_report(satellite_name, sensor_name, gt, 'ground_track', replace=True)
        self.update_sensor_coverage_report(satellite_name, sensor_name, fps, 'coverage', replace=True)

        if report:
            report.plot_flat_map(filled=filled)

    def plot_ground_track(self, satellite_name, **kwargs):
        satellite = self.get_satellite(satellite_name)
        if not satellite:
            logger.info("Could not plot ground track. No satellite found.")
            return None
        report = self.get_ground_track_report(satellite_name)
        ground_track = satellite.get_ground_track().copy()
        gt = []
        for time_, latitude, longitude in ground_track:
            gt.append({
                "time": time_,
                "latitude": np.rad2deg(latitude),
                "longitude": np.rad2deg(longitude)
            })
        self.update_ground_track_report(satellite_name, gt, replace=True)
        report.plot_flat_map()
        report.plot_polar_map()     

    def plot_power_report(self, satellite_name):
        satellite = self.get_satellite(satellite_name)
        if not satellite:
            logger.info("Could not plot power. No satellite found.")
            return None
        power = np.array(satellite.get_generated_power_history())
        figure = plt.figure(f"Power Report of Satellite: {satellite_name}")
        ax = figure.add_subplot(111)
        ax.set_title(f"Power Satellite: {satellite_name}")
        ax.set_xlabel("Time")
        ax.plot(power[:, 0], power[:, 1])
        return ax
    
    def plot_orbit(self, satellite_name):
        raise NotImplementedError

    def plot_ephemeris(self, satellite_name):
        satellite = self.get_satellite(satellite_name)
        if not satellite:
            logger.info("Could not export ephemeris. No satellite found.")
            return None

        ephemeris = copy.deepcopy(satellite.get_propagated_keplerian_elements())
        self.update_ephemeris_report(satellite_name, ephemeris, replace=True)
        report = self.get_ephemeris_report(satellite_name)
        if report:
            report.plot_ephemeris()

    def plot_reports(self):
        raise NotImplementedError("Not implemented yet. apologies!")

    def show_plots(self):
        plt.show()

    def update_access_report(self, satellite_name, station_name, start_end_flag, time):
        satellite = self.get_satellite(satellite_name)
        station = self.get_station(station_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        if station is None:
            raise Exception("The station you searched for does not exist in the scenario.")
        report = self.get_access_report(satellite_name, station_name)
        if report:
            if start_end_flag == "start":
                report.update(time, start=True)
            if start_end_flag == "end":
                report.update(time, end=True)
        return report

    def update_eclipse_report(self, satellite_name, start_end_flag, time):
        satellite = self.get_satellite(satellite_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        report = self.get_eclipse_report(satellite_name)
        if report:
            if start_end_flag == "start":
                report.update(time, start=True)
            if start_end_flag == "end":
                report.update(time, end=True)
        return report
    
    def update_ground_track_report(self, satellite_name, gt, replace=False):
        satellite = self.get_satellite(satellite_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        report = self.get_ground_track_report(satellite_name)
        if not report:
            raise Exception(f"There is no ground track report added for {satellite_name}")
        report.update(gt, replace=replace)    

    def update_sensor_coverage_report(self, satellite_name, sensor_name, data, what, replace=False):
        satellite = self.get_satellite(satellite_name)
        sensor = satellite.get_sensor_by_name(sensor_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        if sensor is None:
            raise Exception("The sensor you searched for does not exist in the scenario or does not belong to the satellite.")
        report = self.get_coverage_report(satellite_name, sensor_name)
        if what == "ground_track":
            report.update_ground_track(data, replace=replace)
        elif what == "coverage":
            report.update(data, replace=replace)
        else:
            raise Exception("Unknown what parameter.")
    
    def update_ephemeris_report(self, satellite_name, data, replace=False):
        satellite = self.get_satellite(satellite_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        report = self.get_ephemeris_report(satellite_name)
        if report:
            report.update(data, replace=replace)

    def update_power_report(self, satellite_name):
        satellite = self.get_satellite(satellite_name)
        if satellite is None:
            raise Exception("The satellite you searched for does not exist in the scenario.")
        power_report = self.get_power_report(satellite_name)
        return self.get_power_report(satellite_name)
    
    def elevation_handler(self, satellite, **kwargs):
        if "station" not in kwargs:
            raise Exception("No station was passed to handler.")
        station = kwargs["station"]
        status = kwargs["status"]
        if status:
            print(f"{satellite.get_name()}: handling entry in view to gs: {station.get_name()}")
            self.update_access_report(satellite.get_name(), station.get_name(), "start", satellite.get_current_time())
        else:
            print(f"{satellite.get_name()}: handling exit from view of gs: {station.get_name()}")
            self.update_access_report(satellite.get_name(), station.get_name(), "end", satellite.get_current_time())

    def eclipse_enter_handler(self, satellite, **kwargs):
        logger.info(f"{satellite.get_name()}: handling eclipse entry!")
        satellite.set_in_eclipse(True)
        self.update_eclipse_report(satellite.get_name(), "start", satellite.get_current_time())

    def eclipse_exit_handler(self, satellite, **kwargs):
        logger.info(f"{satellite.get_name()}: handling eclipse exit!")
        satellite.set_in_eclipse(False)
        self.update_eclipse_report(satellite.get_name(), "end", satellite.get_current_time())

    def simulate(self):
        if self.is_threaded:
            for satellite in self.satellites:
                t = threading.Thread(target=satellite.propagate, args=(self.start_time, self.end_time))
                t.start()
            return None
        data = {}
        for satellite in self.satellites:
            satellite.propagate(self.start_time, self.end_time)
            data.update({satellite.get_name(): satellite.get_base_propagated_states()})
        return data
    

