import unittest
import matplotlib.pyplot as plt
import numpy as np
from pylatex import Document, Section, Figure, NoEscape
from win32api import GetSystemMetrics
import time

class TestPylatexPyplot(unittest.TestCase):
    def test_full_screen(self):
        f = plt.figure("test")
        ax = f.add_subplot(111)

        x = np.arange(0, 100, 1)
        y = np.power(x, 2)

        ax.plot(x, y)
        ax.set_xticks(np.arange(0, 100, 4))
        screen_w = GetSystemMetrics(0)
        screen_h = GetSystemMetrics(1)
        print(screen_w)
        print(screen_h)
        screen_dpi = 300
        f.set_size_inches(screen_w/screen_dpi, screen_h/screen_dpi)

        geometry_options = {"right": "2cm", "left": "2cm"}
        doc = Document("tests/integation/test_pylatex_pyplot", geometry_options=geometry_options)

        doc.append('Introduction.')

        with doc.create(Section('I am a section')):
            doc.append('Take a look at this beautiful plot:')

            with doc.create(Figure(position='htbp')) as plot:
                plot.add_plot(width=NoEscape(r'\textwidth'))
                plot.add_caption('I am a caption.')

            doc.append('Created using matplotlib.')

        doc.append('Conclusion.')

        doc.generate_pdf(clean_tex=False, compiler="pdflatex")

        plt.show()

