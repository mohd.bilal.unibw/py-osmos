from orekit.pyhelpers import setup_orekit_curdir
from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.globals import VM
from utils.astro.thin_orekit_wrapper.events import EventType
from utils.astro.thin_orekit_wrapper.ground_objects import GroundObject
from utils.astro.thin_orekit_wrapper.propagation import PropagatorTypes, PropagationType, BasePropagator
from utils.astro.thin_orekit_wrapper.attitudes import AttitudeLawType, orekit_attitude_to_quat
from utils.astro.thin_orekit_wrapper.orbits.special_orbits import SSO

from org.orekit.orbits import KeplerianOrbit as OrekitKeplerianOrbit
from org.orekit.orbits import OrbitType as OrekitOrbitType

from config import OREKIT_DATA
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import unittest
import orekit
import datetime
import logging
import numpy as np
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class TestSpacecraft(unittest.TestCase):
    def setUp(self) -> None:
        self.stream_handler = logging.StreamHandler(sys.stdout)
        logger.addHandler(self.stream_handler)
        setup_orekit_curdir(OREKIT_DATA)
        return super().setUp()
    
    def tearDown(self) -> None:
        logger.removeHandler(self.stream_handler)
        return super().tearDown()

    def test_sc_constructor(self):
        logger.info("started test")
        sc = Spacecraft("SeRANIS")
        logger.info(f"spacecraft length is: {sc.get_x_length()}")
        logger.info(f"spacecraft shape is: {sc.get_shape_type()}")
        logger.info(f"spacecraft generated power is: {sc.generated_power_history}")
    
    def test_sc_physical_properties(self):
        sc = Spacecraft("SeRANIS")
        
        logger.info(f"spacecraft x length before: {sc.get_x_length()}")
        logger.info(f"spacecraft y length before: {sc.get_y_length()}")
        logger.info(f"spacecraft z length before: {sc.get_z_length()}")

        sc.set_x_length(1.0)
        sc.set_y_length(0.7)
        sc.set_z_length(0.5)

        logger.info(f"spacecraft x length after: {sc.get_x_length()}")
        logger.info(f"spacecraft y length after: {sc.get_y_length()}")
        logger.info(f"spacecraft z length after: {sc.get_z_length()}")

        logger.info(f"solar array area before: {sc.solar_array_area}")
        logger.info(f"solar array normal before: {sc.solar_array_normal}")
        logger.info(f"solar array axis before: {sc.solar_array_axis}")
        logger.info(f"power generated before: {sc.current_generated_power}")
        sc.set_solar_array_area(2.0)
        sc.set_solar_array_normal(np.array([0, -1, 0]))
        sc.set_solar_array_axis(np.array([0, 0, 1]))

        logger.info(f"solar array area after: {sc.solar_array_area}")
        logger.info(f"solar array normal after: {sc.solar_array_normal}")
        logger.info(f"solar array axis after: {sc.solar_array_axis}")


        logger.info(f"new shape model: {sc.physical_model}")
        logger.info(f"power generated after: {sc.current_generated_power}")

    def test_sc_get_set_central_body(self):
        earth = Earth()
        sc = Spacecraft("SeRANIS")
        sc.set_central_body(earth)
        logger.info(f"spacecraft central body is {sc.get_central_body().get_name()}")

    def test_sc_set_initial_state(self):
        earth = Earth()
        sc = Spacecraft("SeRANIS")
        sc.set_central_body(earth)
        sc.set_x_length(0.58)
        sc.set_y_length(0.45)
        sc.set_z_length(0.41)
        sc.set_solar_array_area(1.5 * 1.6)
        sc.set_solar_array_normal(np.array([0, 1, 0]))
        sc.set_solar_array_axis(np.array([1, 0, 0]))
        sc.set_solar_array_efficiency(0.2)
        logger.info(f"power generated before: {sc.current_generated_power}")
        t_epoch = datetime.datetime.utcnow()
        print(f"epoch set: {t_epoch}")
        seranis_orbit_start = SSO(earth,
                                sma=6945.58829e3,
                                eccentricity=0.00,
                                hrs_from_noon=-6.0,
                                epoch=t_epoch)
        state_epoch = seranis_orbit_start.get_keplerian_elements()
        state_epoch.update({"attitude_law": AttitudeLawType.NADIR_POINTING.name})
        print(state_epoch)
        sc.set_initial_state(t_epoch, state_epoch)
        logger.info(f"spacecraft initial state is {sc.get_base_initial_state()}")
        logger.info(f"spacecraft current orbit is {sc.base_current_orbit}")
        logger.info(f"spacecraft initial attitude is: {orekit_attitude_to_quat(sc.get_base_initial_attitude())}")
        logger.info(f"spacecraft current attitude is: {orekit_attitude_to_quat(sc.get_base_current_attitude())}")
        logger.info(f"spacecraft current quaternion attitude is; {sc.get_current_attitude()}")
        logger.info(f"spacecraft initial attitude law is: {sc.get_base_attitude_law()}")
        logger.info(f"power generated after: {sc.current_generated_power}")

    def test_sc_add_forces(self):
        earth = Earth()
        sc = Spacecraft("SeRANIS")
        sc.set_mass(100.0)
        sc.set_central_body(earth)
        t_epoch = datetime.datetime.utcnow()
        state_epoch = {
            "sma": 6945.588024099e3,
            "inclination": np.deg2rad(97.7),
            "eccentricity": 0.00,
            "raan": np.deg2rad(53.58),
            "perigee": 0.0,
            "mean_anomaly": np.deg2rad(131.3468) 
        }
        sc.set_initial_state(t_epoch, state_epoch)
        forces_options = {
            "gravity": {
                "degree": 2,
                "order": 2,
            },
            "drag": {
                "harris_priester": {
                    "cosine": 4.0
                }
            },
            "lunar_3b": True,
            "solar_3b": True,
            "srp":  True
        }
        forces = sc.add_environment_forces(forces_options=forces_options)
        logger.info(forces)

    def test_sc_propagate(self):
        earth = Earth()
        sc = Spacecraft("SeRANIS")
        sc.set_mass(100.0)
        sc.set_central_body(earth)
        sc.set_x_length(0.58)
        sc.set_y_length(0.45)
        sc.set_z_length(0.41)
        sc.set_solar_array_area(1.5 * 1.6)
        sc.set_solar_array_normal(np.array([0, 1, 0]))
        sc.set_solar_array_axis(np.array([1, 0, 0]))
        sc.set_solar_array_efficiency(0.2)
        forces_options = {
            "gravity": {
                "degree": 2,
                "order": 2,
            },
            "drag": {
                "harris_priester": {
                    "cosine": 4.0
                }
            },
            "lunar_3b": True,
            "solar_3b": True,
            "srp":  True
        }
        forces = sc.add_environment_forces(forces_options=forces_options)
        logger.info(forces)
        t_epoch = datetime.datetime.utcnow()
        seranis_orbit_start = SSO(earth,
                                sma=6945.58829e3,
                                eccentricity=0.00,
                                hrs_from_noon=-6.0,
                                epoch=t_epoch)
        state_epoch = seranis_orbit_start.get_keplerian_elements()
        state_epoch.update({"attitude_law": AttitudeLawType.NADIR_POINTING.name})
        sc.set_initial_state(t_epoch, state_epoch)
        start_time = t_epoch
        duration = 24*60*60
        end_time = start_time + datetime.timedelta(seconds = duration)
        

        sc.propagate(start_time, end_time)
        sc_data = sc.get_base_propagated_states()
        # logger.info(f"data from the sc is :{sc_data}")
        sma_propagated = [s.getA()/1e3 for s in sc_data]
        attitude_propagated = sc.get_propagated_attitude()
        # logger.info(attitude_propagated)
        f_sma = plt.figure()
        ax_sma = f_sma.add_subplot(111)
        ax_sma.plot(sma_propagated)
        
        f_power = plt.figure("power")
        ax_power = f_power.add_subplot(111)
        power_history = np.array(sc.get_generated_power_history())
        print(power_history)
        ax_power.plot(power_history[:, 0], power_history[:, 1])
        plt.show()

    def test_sc_propagate_dsst(self):
        earth = Earth()
        propagator_options = dict(propagator_type=PropagatorTypes.DSST.name, propagation_type=PropagationType.MEAN.name)
        sc = Spacecraft("SeRANIS", propagator_options=propagator_options)
        sc.set_mass(100.0)
        sc.set_central_body(earth)
        sc.set_x_length(0.58)
        sc.set_y_length(0.45)
        sc.set_z_length(0.41)
        sc.set_solar_array_area(1.5 * 1.6)
        sc.set_solar_array_normal(np.array([0, 1, 0]))
        sc.set_solar_array_axis(np.array([1, 0, 0]))
        sc.set_solar_array_efficiency(0.2)
        forces_options = {
            "gravity": {
                "degree": 21,
                "order": 21,
            },
            "drag": {
                "harris_priester": {
                    "cosine": 4.0
                }
            },
            "lunar_3b": True,
            "solar_3b": True,
            "srp":  True
        }
        forces = sc.add_environment_forces(forces_options=forces_options)
        logger.info(forces)
        t_epoch = datetime.datetime.utcnow()
        state_epoch = {
            "sma": 6945.588024099e3,
            "inclination": np.deg2rad(60),
            "eccentricity": 0.0011,
            "raan": np.deg2rad(53.58),
            "perigee": 0.0,
            "mean_anomaly": np.deg2rad(131.3468) 
        }
        sc.set_initial_state(t_epoch, state_epoch)
        start_time = t_epoch
        duration = 10*60*60
        end_time = start_time + datetime.timedelta(seconds = duration)


        sc.propagate(start_time, end_time)
        sc_data = sc.get_base_propagated_states()
        # logger.info(f"data from the sc is :{sc_data}")
        sma_propagated = [s.getA()/1e3 for s in sc_data]
        equinoctial_ex = [s.getEquinoctialEx() for s in sc_data]
        plt.plot(equinoctial_ex)
        plt.show()

    def test_sc_propagation_handlers(self):
        earth = Earth()
        station = GroundObject("Test Station", earth, np.deg2rad(10.0), np.deg2rad(55.0), 100.0)
        sc = Spacecraft("SeRANIS")
        sc.set_mass(100.0)
        sc.set_central_body(earth)

        sc.set_x_length(0.58)
        sc.set_y_length(0.45)
        sc.set_z_length(0.41)
        sc.set_solar_array_area(1.5 * 1.6)
        sc.set_solar_array_normal(np.array([0, 1, 0]))
        sc.set_solar_array_axis(np.array([1, 0, 0]))
        sc.set_solar_array_efficiency(0.2)

        forces_options = {
            "gravity": {
                "degree": 2,
                "order": 2,
            },
            "drag": {
                "harris_priester": {
                    "cosine": 4.0
                }
            },
            "lunar_3b": True,
            "solar_3b": True,
            "srp":  True
        }
        forces = sc.add_environment_forces(forces_options=forces_options)
        logger.info(forces)
        t_epoch = datetime.datetime.utcnow()
        seranis_orbit_start = SSO(earth,
                            sma=6945.58829e3,
                            eccentricity=0.01,
                            hrs_from_noon=-6.0,
                            epoch=t_epoch)
        state_epoch = seranis_orbit_start.get_keplerian_elements()
        state_epoch.update({"attitude_law": AttitudeLawType.NADIR_POINTING.name})
        # state_epoch = {
        #     "sma": 7000e3,
        #     "inclination": 1.10723687,
        #     "eccentricity": 0.0009536,
        #     "raan": np.deg2rad(0.0),
        #     "perigee": np.pi/2,
        #     "mean_anomaly": np.deg2rad(0.0) 
        # }
        sc.set_initial_state(t_epoch, state_epoch)
        start_time = t_epoch
        duration = 24*60*60
        end_time = start_time + datetime.timedelta(seconds = duration)

        # define an event handler
        def eclipse_enter_handler(spacecraft):
            print(f"{spacecraft.get_name()}: handling eclipse entry!")
            spacecraft.set_in_eclipse(True)

        def eclipse_exit_handler(spacecraft):
            print(f"{spacecraft.get_name()}: handling eclipse exit!")
            spacecraft.set_in_eclipse(True)

        def apsides_handler(spacecraft, **kwargs):
            print(f"{spacecraft.get_name()}: handling {kwargs.get('apside')}!")
            keplerian = OrekitKeplerianOrbit.cast_(OrekitOrbitType.KEPLERIAN.convertType(spacecraft.get_base_current_state().getOrbit()))
            elements = {
            "sma": keplerian.getA(),
            "eccentricity": keplerian.getE(),
            "raan": keplerian.getRightAscensionOfAscendingNode(),
            "inclination": keplerian.getI(),
            "perigee": keplerian.getPerigeeArgument(),
            "mean_anomaly": keplerian.getMeanAnomaly(),
            }
            print(elements)


        # attach the event handler to this spacecraft
        event = sc.add_event_handler(EventType.ECLIPSE_ENTER_DETECTOR, eclipse_enter_handler)
        logger.info(f"Eclipse Event is {event}")

        event_exit_eclipse = sc.add_event_handler(EventType.ECLIPSE_EXIT_DETECTOR, eclipse_exit_handler)

        def elevation_handler(spacecraft, **kwargs):
            if "station" not in kwargs:
                raise Exception("No station was passed to handler")
            station = kwargs["station"]
            status = kwargs["status"]
            if status:
                print(f"{spacecraft.get_name()}: handling entry in view to gs: {station.get_name()}")
            else:
                print(f"{spacecraft.get_name()}: handling exit from view of gs: {station.get_name()}")
        elevation_event = sc.add_event_handler(EventType.ELEVATION_DETECTOR, elevation_handler, station=station)
        apsides_event = sc.add_event_handler(EventType.APSIDE_DETECTOR, apsides_handler)
        # propagate
        sc.propagate(start_time, end_time)
        sc_data = sc.get_base_propagated_states()
        sc_keplerian_data = sc.get_propagated_keplerian_elements()
        sc_keplerian_epochs = sc_keplerian_data.get("epoch")

        figure = plt.figure("Keplerian Elements")
        ax_sma = figure.add_subplot(711)
        ax_sma.plot(sc_keplerian_epochs, sc_keplerian_data.get("sma"))
        ax_sma.set_title("Semi-Major Axis")
        ax_sma.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)

        ax_ecc = figure.add_subplot(712)
        ax_ecc.plot(sc_keplerian_epochs, sc_keplerian_data.get("eccentricity"))
        ax_ecc.set_title("Eccentricity")
        ax_ecc.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)

        ax_inclination = figure.add_subplot(713)
        ax_inclination.plot(sc_keplerian_epochs, np.rad2deg(sc_keplerian_data.get("inclination")))
        ax_inclination.set_title("Inclination [deg]")
        ax_inclination.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)

        ax_raan = figure.add_subplot(714)
        ax_raan.plot(sc_keplerian_epochs, np.rad2deg(sc_keplerian_data.get("raan")))
        ax_raan.set_title("RAAN [deg]")
        ax_raan.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)

        ax_perigee = figure.add_subplot(715)
        ax_perigee.plot(sc_keplerian_epochs, np.rad2deg(sc_keplerian_data.get("perigee")))
        ax_perigee.set_title("Perigee [deg]")
        ax_perigee.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
        
        ax_ma = figure.add_subplot(716)
        ax_ma.plot(sc_keplerian_epochs, np.rad2deg(sc_keplerian_data.get("mean_anomaly")))
        ax_ma.set_title("Mean Anomaly [deg]")
        ax_ma.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)

        arg_latitude = np.array(sc_keplerian_data.get("mean_anomaly")) + np.array(sc_keplerian_data.get("perigee"))
        ax_aol = figure.add_subplot(717)
        ax_aol.plot(sc_keplerian_epochs, np.rad2deg(arg_latitude))
        ax_aol.set_title("Argument of latitude")
        ax_aol.tick_params(axis='x', which='both', bottom=True, top=False)

        figure_perigee = plt.figure("Perigee")
        ax_perigee_1 = figure_perigee.add_subplot(111)
        ax_perigee_1.plot(sc_keplerian_epochs, np.rad2deg(sc_keplerian_data.get("perigee")))
        ax_perigee_1.set_title("Perigee [deg]")

        sc_cartesian_data = sc.get_propagated_cartesian_states()
        figure_3d = plt.figure("cartesian")
        ax_3d_orbit = figure_3d.add_subplot(projection="3d")

        sc_cartesian_x = sc_cartesian_data.get("x")
        sc_cartesian_y = sc_cartesian_data.get("y")
        sc_cartesian_z = sc_cartesian_data.get("z")

        steps = len(sc_cartesian_x)

  
        def animate_orbit(n, data, lines):
            lines[0].set_data(data[0:2, :n])
            lines[0].set_3d_properties(data[2, :n])
            lines[1].set_data(data[0:2, n])
            lines[1].set_3d_properties(data[2, n])
            return lines
        plt.style.use('dark_background')
        animation_data = np.array([sc_cartesian_x, sc_cartesian_y, sc_cartesian_z])
        orbit_line = ax_3d_orbit.plot(animation_data[0], animation_data[1], animation_data[2], lw=1, color='g')[0]
        current_point = ax_3d_orbit.plot(animation_data[0, 0], animation_data[1, 0], animation_data[2, 0], lw=1, marker="+", color='r')[0]
        lines = [orbit_line, current_point]
        ax_3d_orbit.set_xlabel("X [m]")
        ax_3d_orbit.set_ylabel("Y [m]")
        ax_3d_orbit.set_zlabel("Z [m]")

        anim = FuncAnimation(figure_3d, animate_orbit, fargs=(animation_data, lines), frames=int((steps-1)/5), interval=1, blit=True)
        # anim.save('orbit_propagate.gif', writer='imagemagick')

        f_power = plt.figure("power")
        ax_power = f_power.add_subplot(111)
        power_history = np.array(sc.get_generated_power_history())
        print(power_history)
        ax_power.plot(power_history[:, 0], power_history[:, 1])
        plt.show()

        plt.tight_layout()
        plt.show()
