from utils.astro.thin_orekit_wrapper.orbits.special_orbits import sso_repeating_ground_track
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.globals import VM

import unittest
import numpy as np
import pprint


class SSORGT(unittest.TestCase):
    def setUp(self) -> None:
        self.pp = pprint.PrettyPrinter(indent=4)
        return super().setUp()

    def test_sso_rgt(self):
        earth = Earth()
        k_revs2rep = 167
        k_days2rep = 11
        rp = 500e3 +  earth.get_equatorial_radius()
        orbital_elements = sso_repeating_ground_track(k_revs2rep, k_days2rep, rp, body=earth, hrs_from_noon=6.0)
        self.pp.pprint(orbital_elements)
        print(f"inclination degrees: {np.rad2deg(orbital_elements.get('inclination'))}")
