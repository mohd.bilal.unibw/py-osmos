from utils.astro.thin_orekit_wrapper.orbits.special_orbits import repeating_ground_track
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.globals import VM

import unittest
import numpy as np
import pprint


class RGTO(unittest.TestCase):
    def setUp(self) -> None:
        self.pp = pprint.PrettyPrinter(indent=4)
        return super().setUp()

    def test_rgto(self):
        earth = Earth()
        k_revs2rep = 167
        k_days2rep = 11
        inclination = np.deg2rad(97.7)
        rp = 500e3 + earth.get_equatorial_radius()
        orbital_elements = repeating_ground_track(k_revs2rep, k_days2rep, inclination, rp, earth)
        print("\n" + "-"*100)
        self.pp.pprint(orbital_elements)
        self.assertEqual(rp, orbital_elements.get('sma')*(1 - orbital_elements.get('eccentricity')))
    
