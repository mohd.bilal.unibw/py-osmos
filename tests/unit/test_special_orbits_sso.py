from utils.astro.thin_orekit_wrapper.orbits.special_orbits import sso
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft
from utils.astro.thin_orekit_wrapper.globals import VM

import unittest
import numpy as np
import datetime

class TestSSO(unittest.TestCase):
    def test_sso_ae(self):
        earth = Earth()
        kep = sso(earth, a=6945033.309104825, e=0, hrs_from_noon=-6.0)
        print(kep)
        print(f"height in km: {(kep.get('sma') - earth.get_equatorial_radius())/1000}")
        print(f"inclination in degrees: {np.rad2deg(kep['inclination'])}")

    def test_sso_opd(self):
        earth = Earth()
        kep = sso(earth, e=0, orbits_per_day=15, hrs_from_noon=-6.0)
        print(kep)
        print(f"height in km: {(kep.get('sma') - earth.get_equatorial_radius())/1000}")
        print(f"inclination in degrees: {np.rad2deg(kep['inclination'])}")
    
    def test_sso_subsat_point(self):
        earth = Earth()
        constraints = dict(subsatellite=dict(latitude=np.deg2rad(48), longitude=np.deg2rad(10), direction="descending"))
        state_epoch = sso(earth, e=0, orbits_per_day=15, hrs_from_noon=-6.0, constraints=constraints)
        print(state_epoch)
        print(f"height in km: {(state_epoch.get('sma') - earth.get_equatorial_radius())/1000}")
        print(f"inclination in degrees: {np.rad2deg(state_epoch['inclination'])}")
        sc = Spacecraft("SeRANIS")
        sc.set_mass(100.0)
        sc.set_central_body(earth)
        t_epoch = state_epoch.get("epoch")
        sc.set_initial_state(t_epoch, state_epoch)
        print(np.rad2deg(sc.compute_current_geodetic_projection()))
