from utils.plotting import EclipseBarPlot, EclipseHistogramPlot
from config import TIME_FORMAT
import matplotlib.pyplot as plt
import unittest
import json
import datetime

class TestEclipsePlots(unittest.TestCase):
    def setUp(self):
        fname = "Scenario_JB_RGT_Scenario_eclipse_report_SeRANIS.json"
        with open('simulations/reports/'+fname) as f:
            self.data = json.load(f)
        self.data["simulation"]["start"] = datetime.datetime.strptime(self.data.get("simulation").get("start"), TIME_FORMAT)
        self.data["simulation"]["end"] = datetime.datetime.strptime(self.data.get("simulation").get("end"), TIME_FORMAT)

        for a in self.data.get('eclipse'):
            a['start'] = datetime.datetime.strptime(a['start'], TIME_FORMAT)
            a['end'] = datetime.datetime.strptime(a['end'], TIME_FORMAT)

    def test_bar_plot(self):
        bar_plot = EclipseBarPlot(self.data)
        bar_plot.create()
        plt.show()

    def test_histogram_plot(self):
        hist_plot = EclipseHistogramPlot(self.data)
        hist_plot.create()
        plt.show()