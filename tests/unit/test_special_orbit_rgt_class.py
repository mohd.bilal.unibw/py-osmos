from utils.astro.thin_orekit_wrapper.orbits.special_orbits import RepeatingGroundTrackOrbit
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft
from utils.astro.thin_orekit_wrapper.globals import VM

import unittest
import numpy as np

class TestRGTO(unittest.TestCase):
    def test_rgto_rp_kr_kd(self):
        earth = Earth()
        inclination = np.deg2rad(60)
        rgto = RepeatingGroundTrackOrbit(inclination, body=earth, k_revs2rep=196, k_days2rep=15.5, perigee_height=500e3)
        kep = rgto.get_keplerian_elements()
        print(kep)
        print(f"height in km: {(kep.get('sma') - earth.get_equatorial_radius())/1000}")
        print(f"eccentricity: {kep.get('eccentricity')}")
    
    def test_rgto_subsat_constraint(self):
        earth = Earth()
        inclination = np.deg2rad(60)
        constraints = dict(subsatellite=dict(latitude=np.deg2rad(48.747), longitude=np.deg2rad(10), direction="descending"))
        rgto = RepeatingGroundTrackOrbit(inclination, body=earth, k_revs2rep=196.0, k_days2rep=15.5, perigee_height=500e3, constraints=constraints)
        kep = rgto.get_keplerian_elements()
        print(kep)
        print(f"height in km: {(kep.get('sma') - earth.get_equatorial_radius())/1000}")
        print(f"eccentricity: {kep.get('eccentricity')}")
        sc = Spacecraft("SeRANIS")
        sc.set_mass(100.0)
        sc.set_central_body(earth)
        sc.set_initial_state(rgto.get_epoch(), kep)
        print(np.rad2deg(sc.get_current_geodetic_projection()))

    def test_rgto_min_eccentricity(self):
        earth = Earth()
        inclination = np.deg2rad(60)
        constraints = dict(subsatellite=dict(latitude=np.deg2rad(48.747), longitude=np.deg2rad(10), direction="descending"))
        rgto = RepeatingGroundTrackOrbit.get_minimum_eccentricity_rgto(inclination, perigee_height=500e3, body=earth, constraints=constraints)
        kep = rgto.get_keplerian_elements()
        print(kep)
        print(f"height in km: {(kep.get('sma') - earth.get_equatorial_radius())/1000}")
        print(f"eccentricity: {kep.get('eccentricity')}")
        sc = Spacecraft("SeRANIS")
        sc.set_mass(100.0)
        sc.set_central_body(earth)
        sc.set_initial_state(rgto.get_epoch(), kep)
        print(np.rad2deg(sc.get_current_geodetic_projection()))
