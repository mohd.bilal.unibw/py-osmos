from blocks import Block
from ports import Port, InputPort, OutputPort
import unittest
import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)


class TestBlock(unittest.TestCase):
    def test_block_init(self):
        class KeplerianOrbitBlock(Block):
            def __init__(self, name, elements, *args, **kwargs):
                super().__init__(name, *args, **kwargs)
                self.sma = elements.get("sma")

            def set_sma(self, sma):
                self.sma = sma
                self.on_property_change()

        class CoverageBlock(Block):
            def __init__(self, name, *args, **kwargs):
                super().__init__(name, *args, **kwargs)
                self.sma = None

            def set_sma(self, sma):
                self.sma = sma
                self.on_property_change()

        # create a block
        orbit_block = KeplerianOrbitBlock("Orbit", {"sma": 7600})

        # create an output port that transfers specific data, attaches the port to the block
        sma_port = OutputPort(orbit_block, "sma")

        # create another block
        coverage_block = CoverageBlock("Coverage")

        # create an input port that receives some specific data
        sma_port_coverage_block = InputPort(coverage_block, "sma")

        # connect the output sma port of orbit block to input sma port of coverage block
        orbit_block.connect(sma_port, sma_port_coverage_block)

        orbit_block.set_sma(8000)
        print(coverage_block.sma)
        print(orbit_block.__dict__)
        print(coverage_block.__dict__)