from utils.reports import AccessReport
import unittest
import json
import datetime
import matplotlib.pyplot as plt
from config import TIME_FORMAT


class TestAccessReport(unittest.TestCase):
    def setUp(self):
        fname = "Scenario_JB_RGT_Scenario_access_report_SeRANIS_Neubiberg.json"
        with open('simulations/reports/' + fname) as f:
            self.data = json.load(f)
        self.data["simulation"]["start"] = datetime.datetime.strptime(
            self.data.get("simulation").get("start"), TIME_FORMAT)
        self.data["simulation"]["end"] = datetime.datetime.strptime(
            self.data.get("simulation").get("end"), TIME_FORMAT)

        for a in self.data.get('access'):
            a['start'] = datetime.datetime.strptime(a['start'], TIME_FORMAT)
            a['end'] = datetime.datetime.strptime(a['end'], TIME_FORMAT)
        self.data['satellite']['initial_conditions'][
            'epoch'] = datetime.datetime.strptime(
                self.data['satellite']['initial_conditions']['epoch'],
                TIME_FORMAT)

    def test_access_report_constructor(self):
        report = AccessReport(self.data["simulation"], self.data["satellite"],
                              self.data["station"])
        print(report.name)
        print(report.get_satellite_data())
        print(report.get_station_data())

    def test_access_report_update(self):
        report = AccessReport(self.data["simulation"], self.data["satellite"],
                              self.data["station"])
        for a in self.data["access"]:
            report.update(a["start"], start=True)
            report.update(a["end"], end=True)
        print("-" * 100)
        print(report.get_report_data()['start'])
        print("-" * 100)
        print(report.get_report_data()['end'])
        print("-" * 100)

    def test_access_report_export(self):
        report = AccessReport(self.data["simulation"], self.data["satellite"],
                              self.data["station"])
        for a in self.data["access"]:
            report.update(a["start"], start=True)
            report.update(a["end"], end=True)
        report.export(fpath="tests/unit")

    def test_access_report_plots(self):
        report = AccessReport(self.data["simulation"], self.data["satellite"],
                              self.data["station"])
        for a in self.data["access"]:
            report.update(a["start"], start=True)
            report.update(a["end"], end=True)
        report.plot_access_bar()
        report.plot_access_histogram()
        plt.show()