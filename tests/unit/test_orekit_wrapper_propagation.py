from orekit.pyhelpers import setup_orekit_curdir
from utils.astro.thin_orekit_wrapper.globals import VM
from utils.astro.thin_orekit_wrapper.propagation import PropagatorTypes, PropagationType, BasePropagator
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft

import unittest
import numpy as np
import datetime

class TestPropagation(unittest.TestCase):
    def test_numerical_propagator(self):
        earth = Earth()
        sc = Spacecraft("SeRANIS")
        sc.set_central_body(earth)
        self.assertEqual(sc.get_propagator().get_type(), PropagatorTypes.NUMERICAL)

    def test_dsst_propagator(self):
        earth = Earth()
        propagator_options = dict(propagator_type=PropagatorTypes.DSST.name, propagation_type=PropagationType.OSCULATING.name)
        sc = Spacecraft("SeRANIS", propagator_options=propagator_options)
        sc.set_central_body(earth)
        print(sc.get_propagator().base_propagation_type)
        self.assertEqual(sc.get_propagator().get_type(), PropagatorTypes.DSST)

    def test_numerical_propagator_propagate(self):
        earth = Earth()
        sc = Spacecraft("SeRANIS")
        sc.set_central_body(earth)
        
