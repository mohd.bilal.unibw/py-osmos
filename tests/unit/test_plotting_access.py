from utils.plotting import AccessBarPlot, AccessHistogramPlot
from config import TIME_FORMAT
import matplotlib.pyplot as plt
import unittest
import json
import datetime

class TestAccessPlots(unittest.TestCase):
    def setUp(self):
        fname = "Scenario_JB_RGT_Scenario_GS_ACCESS Report SeRANIS - Neubiberg.json"
        with open('tests/unit/'+fname) as f:
            self.data = json.load(f)
        self.data["scenario"]["start"] = datetime.datetime.strptime(self.data.get("scenario").get("start"), TIME_FORMAT)
        self.data["scenario"]["end"] = datetime.datetime.strptime(self.data.get("scenario").get("end"), TIME_FORMAT)

        for a in self.data.get('access'):
            a['start'] = datetime.datetime.strptime(a['start'], TIME_FORMAT)
            a['end'] = datetime.datetime.strptime(a['end'], TIME_FORMAT)

    def test_bar_plot(self):
        # bar_plot = AccessBarPlot(self.data, plot_duration=2*86400, theme='light')
        print(self.data)
        bar_plot = AccessBarPlot(self.data)
        bar_plot.create()
        plt.show()

    def test_histogram_plot(self):
        hist_plot = AccessHistogramPlot(self.data, plot_duration=1*86400)
        hist_plot.create()
        plt.show()