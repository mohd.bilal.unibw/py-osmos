from tests import unit
import orekit
from utils.astro.thin_orekit_wrapper.globals import VM
from orekit.pyhelpers import setup_orekit_curdir
from config import OREKIT_DATA
from utils.astro.thin_orekit_wrapper.orbits.special_orbits import frozen, FrozenEccentricity
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft
from utils.astro.thin_orekit_wrapper.propagation import PropagationType, PropagatorTypes
# to run the simulation: Y:\software\seranis_mission_simulator>python -m unittest -v tests.unit.test_special_orbits_frozen.TestFrozenOrbit
import matplotlib.pyplot as plt
import unittest
import numpy as np
import datetime


class TestFrozenOrbit(unittest.TestCase):
    def test_frozen_sma(self):
        a, e, i, w = frozen(a=7198.65e03, i=np.deg2rad(98.697))
        print("useless")

    def test_frozen_class_numerical(self):
        earth = Earth()
        sma = earth.get_equatorial_radius() + 567e3
        inclination = np.deg2rad(97.7)
        orbit = FrozenEccentricity(sma,
                                   inclination,
                                   body=earth,
                                   computation_method="NUMERICAL")
        print(orbit.get_keplerian_elements())

    def test_frozen_class_numerical_propagate_dsst(self):
        # set up times
        start_time = datetime.datetime.utcnow()
        duration = 200 * 24 * 60 * 60
        end_time = start_time + datetime.timedelta(seconds=duration)
        step_size = 24.0 * 60 * 60

        # set up body
        earth = Earth()

        sma = earth.get_equatorial_radius() + 567e3
        inclination = np.deg2rad(97.7)
        orbit = FrozenEccentricity(sma,
                                   inclination,
                                   body=earth,
                                   epoch=start_time,
                                   computation_method="NUMERICAL")
        propagator_options = dict(
            propagator_type=PropagatorTypes.DSST.name,
            propagation_type=PropagationType.MEAN.name,
            initial_condition_type=PropagationType.MEAN.name,
            step_size=step_size,
            max_step=step_size,
            min_step=step_size)
        sc = Spacecraft("SeRANIS", propagator_options=propagator_options)
        sc.set_central_body(earth)
        forces_options = {
            "gravity": {
                "degree": 51,
                "order": 51,
            }
        }
        forces = sc.add_environment_forces(forces_options=forces_options)
        sc.set_initial_state(orbit.get_epoch(), orbit.get_keplerian_elements())

        sc.propagate(start_time, end_time)

        propagated_kep = sc.get_propagated_keplerian_elements()

        plt.style.use('dark_background')

        f = plt.figure()
        ax_sma = f.add_subplot(321)
        ax_sma.plot(np.array(propagated_kep.get("sma")) / 1e3)
        ax_sma.set_ylabel("SMA [km]")

        ax_ecc = f.add_subplot(322)
        ax_ecc.plot(propagated_kep.get("eccentricity"))
        ax_ecc.set_ylabel("e")
        
        ax_i = f.add_subplot(323)
        ax_i.plot(np.rad2deg(np.array(propagated_kep.get("inclination"))))
        ax_i.set_ylabel("inclination [deg]")

        ax_raan = f.add_subplot(324)
        ax_raan.plot(np.rad2deg(np.array(propagated_kep.get("raan"))))
        ax_raan.set_ylabel("raan [deg]")

        ax_perigee = f.add_subplot(325)
        ax_perigee.plot(np.rad2deg(np.array(propagated_kep.get("perigee"))))
        ax_perigee.set_ylabel("perigee [deg]")

        ax_ma = f.add_subplot(326)
        ax_ma.plot(np.rad2deg(np.array(propagated_kep.get("mean_anomaly"))))
        ax_ma.set_ylabel("mean anomaly [deg]")

        plt.tight_layout()
        plt.show()
