from utils.astro.thin_orekit_wrapper.orbits.special_orbits import SSO
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft
from utils.astro.thin_orekit_wrapper.globals import VM

import unittest
import numpy as np
import datetime

class TestSSO(unittest.TestCase):
    def test_sso_subsat_point(self):
        earth = Earth()
        constraints = dict(subsatellite=dict(latitude=np.deg2rad(48.0747), longitude=np.deg2rad(11.67), direction="descending"))
        sso = SSO(earth, eccentricity=0, orbits_per_day=15, hrs_from_noon=-6.0, constraints=constraints)
        kep = sso.get_keplerian_elements()
        print(kep)
        print(f"height in km: {(kep.get('sma') - earth.get_equatorial_radius())/1000}")
        print(f"inclination in degrees: {np.rad2deg(kep['inclination'])}")
        sc = Spacecraft("SeRANIS")
        sc.set_mass(100.0)
        sc.set_central_body(earth)
        sc.set_initial_state(sso.get_epoch(), kep)
        print(np.rad2deg(sc.compute_current_geodetic_projection()))
