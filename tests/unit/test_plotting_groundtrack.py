from utils.plotting import GroundTrackMapPlot, GroundTrackPolarPlot
from config import TIME_FORMAT
import matplotlib.pyplot as plt
import unittest
import json
import datetime

class TestGroundTrackPlots(unittest.TestCase):
    def setUp(self):
        fname = "Scenario_Airbus_SSO_Scenario_ground_track_report_SeRANIS.json"
        with open('simulations/reports/'+fname) as f:
            self.data = json.load(f)
        self.data["simulation"]["start"] = datetime.datetime.strptime(self.data.get("simulation").get("start"), TIME_FORMAT)
        self.data["simulation"]["end"] = datetime.datetime.strptime(self.data.get("simulation").get("end"), TIME_FORMAT)

        for g in self.data.get('ground_track'):
            g['time'] = datetime.datetime.strptime(g['time'], TIME_FORMAT)

    def test_gt_map_plot(self):
        gt_plot = GroundTrackMapPlot(self.data)
        gt_plot.create()
        plt.show()

    def test_gt_polar_plot(self):
        gt_plot = GroundTrackPolarPlot(self.data)
        gt_plot.create()
        gt_plot.add_location(48, 11, "Neubiberg")
        plt.show()