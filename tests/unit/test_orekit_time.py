import orekit
from orekit.pyhelpers import setup_orekit_curdir, datetime_to_absolutedate, absolutedate_to_datetime
from utils.astro.thin_orekit_wrapper.globals import VM

import unittest
import datetime


class TestOrekitTime(unittest.TestCase):
    def test_orekit_utc(self):
        t_now = datetime.datetime.utcnow()
        t_orekit = datetime_to_absolutedate(t_now)
        t_dt = absolutedate_to_datetime(t_orekit)
        print(f"current UTC: {t_now}")
        print(f"utc in orekit: {t_orekit}")
        print(f"utc converted from orekit {t_dt}")

