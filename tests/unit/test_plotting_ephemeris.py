from utils.plotting import OrbitParametersPlot
from config import TIME_FORMAT
import matplotlib.pyplot as plt
import unittest
import json
import datetime


class TestEphemerisPlots(unittest.TestCase):
    def setUp(self):
        fname = "Scenario_Airbus_SSO_Scenario_ephemeris_report_SeRANIS.json"
        with open('simulations/reports/' + fname) as f:
            self.data = json.load(f)
        self.data["simulation"]["start"] = datetime.datetime.strptime(
            self.data.get("simulation").get("start"), TIME_FORMAT)
        self.data["simulation"]["end"] = datetime.datetime.strptime(
            self.data.get("simulation").get("end"), TIME_FORMAT)
        ephemeris = self.data["ephemeris"]
        ephemeris.update({
            "epoch": [
                datetime.datetime.strptime(t, TIME_FORMAT)
                for t in ephemeris.get("epoch")
            ]
        })

    def test_ephemeris_plot(self):
        plots = OrbitParametersPlot(self.data)
        plots.create()
        plt.show()
