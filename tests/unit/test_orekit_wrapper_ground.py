from orekit.pyhelpers import setup_orekit_curdir
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.ground_objects import GroundObject
from utils.astro.thin_orekit_wrapper.globals import VM
from config import OREKIT_DATA
import unittest
import orekit
import datetime
import logging
import numpy as np
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class TestGroundObject(unittest.TestCase):
    def setUp(self) -> None:
        self.stream_handler = logging.StreamHandler(sys.stdout)
        logger.addHandler(self.stream_handler)
        return super().setUp()
    
    def tearDown(self) -> None:
        logger.removeHandler(self.stream_handler)
        return super().tearDown()

    def test_go_constructor(self):
        earth = Earth()
        station = GroundObject("Test Station", earth, np.deg2rad(10.0), np.deg2rad(55.0), 100.0)
        logger.info(f"Name of station is: {station.get_name()}")