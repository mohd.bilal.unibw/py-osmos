from utils.astro.helpers import mean2osc
from tests import unit
import orekit
from utils.astro.thin_orekit_wrapper.globals import VM
from orekit.pyhelpers import setup_orekit_curdir
from config import OREKIT_DATA
from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft
from utils.astro.thin_orekit_wrapper.bodies import Earth
import datetime
import unittest
import numpy as np

# to run the simulation: Y:\software\seranis_mission_simulator>python -m unittest -v tests.unit.test_mean2osc.TestMean2Osc

class TestMean2Osc(unittest.TestCase):  

    def test_mean2osc(self):

        body=Earth()
        R = body.get_equatorial_radius()
        J2 = 0.108265e-2
        MU = body.get_mu()
        a=7198.65e03
        i=np.deg2rad(98.697)
        raan=np.deg2rad(50)
        w=np.deg2rad(20)
        M=np.deg2rad(20)
        e=0.001

        a_osc, e_osc, i_osc, w_osc, M_osc, raan_osc = mean2osc(a, e, i, w, M, raan, R, MU, J2)
        print("the mean elements are a:", a,"e:", e, "i:", i, "raan:", raan, "w:", w, "M:", M)
        print("the osculating elements are a:", a_osc,"e:", e_osc, "i:", i_osc, "raan:", raan_osc, "w:", w_osc, "M:", M_osc)
        
        sc = Spacecraft("SeRANIS")
        sc.set_mass(100.0)
        sc.set_central_body(body)
        t_epoch = datetime.datetime.utcnow()
        state_epoch = {
            "sma": a,
            "inclination": i,
            "eccentricity": e,
            "raan": raan,
            "perigee": w,
            "mean_anomaly": M 
        }
        sc.set_initial_state(t_epoch, state_epoch)
        state=sc.get_base_initial_state()
        pv=state.getPVCoordinates()
        position=pv.getPosition()
        velocity=pv.getVelocity()
        x=position.getX()
        y=position.getY()
        z=position.getZ()
        vx=velocity.getX()
        vy=velocity.getY()
        vz=velocity.getZ()
        r_norm=position.getNorm()
        v_norm=velocity.getNorm()

        state_epoch_osc = {
            "sma": a_osc,
            "inclination": i_osc,
            "eccentricity": e_osc,
            "raan": raan_osc,
            "perigee": w_osc,
            "mean_anomaly": M_osc 
        }
        sc.set_initial_state(t_epoch, state_epoch_osc)
        state=sc.get_base_initial_state()
        pv=state.getPVCoordinates()
        position=pv.getPosition()
        velocity=pv.getVelocity()
        x_osc=position.getX()
        y_osc=position.getY()
        z_osc=position.getZ()
        vx_osc=velocity.getX()
        vy_osc=velocity.getY()
        vz_osc=velocity.getZ()
        r_norm_osc=position.getNorm()
        v_norm_osc=velocity.getNorm()

        print("Position and velocity before conversion r=",r_norm,",v=",v_norm,"x=",x,"y=",y,"z=",z,"vx=",vx,"vy=",vy,"vz=",vz)
        print("Position and velocity after conversion r=",r_norm_osc,",v=",v_norm_osc,"x=",x_osc,"y=",y_osc,"z=",z_osc,"vx=",vx_osc,"vy=",vy_osc,"vz=",vz_osc)
  
    def test_useless(self):
        print("useless")