from orekit.pyhelpers import setup_orekit_curdir
from utils.astro.thin_orekit_wrapper.globals import VM
from scenario import SimpleScenario
from scenario import ReportTypes
from config import OREKIT_DATA
import matplotlib.pyplot as plt
import numpy as np
import unittest
import orekit
import datetime
import logging
import pprint
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class TestScenario(unittest.TestCase):
    def setUp(self) -> None:
        self.stream_handler = logging.StreamHandler(sys.stdout)
        logger.addHandler(self.stream_handler)
        self.forces_options = {
            "gravity": {
                "degree": 10,
                "order": 10,
            },
            "drag": {
                "harris_priester": {
                    "cosine": 4.0
                }
            },
            "lunar_3b": True,
            "solar_3b": True,
            "srp":  True
        }


        return super().setUp()

    def test_scenario_constructor(self):
        start_time = datetime.datetime.utcnow()
        duration = 24*60*60
        end_time = start_time + datetime.timedelta(seconds = duration)
        scenario = SimpleScenario("Test Scenario", start_time, end_time)
        logger.info("\n Results")
        logger.info("-"*100)
        logger.info(f"scenario name is {scenario.get_name()}")
        logger.info(f"scenario central body is {scenario.get_central_body().get_name()}")
        logger.info(f"scenario astro provider is {scenario.astro_provider}")
        logger.info("-"*100)
        self.assertEqual(scenario.get_central_body().get_name().upper(), "EARTH")
    
    def test_add_satellite(self):
        start_time = datetime.datetime.utcnow()
        duration = 24*60*60
        end_time = start_time + datetime.timedelta(seconds = duration)
        scenario = SimpleScenario("Test Scenario", start_time, end_time)
        seranis = scenario.add_satellite("SeRANIS")
        logger.info("\nResults")
        logger.info("-"*100)
        logger.info(f"satellite name is {seranis.get_name()}")
        logger.info(f"scenario central body is {seranis.get_central_body().get_name()}")
        logger.info("-"*100)
        self.assertEqual(seranis.get_central_body(), scenario.get_central_body())
    
    def test_add_station(self):
        start_time = datetime.datetime.utcnow()
        duration = 24*60*60
        end_time = start_time + datetime.timedelta(seconds = duration)
        scenario = SimpleScenario("Test Scenario", start_time, end_time)
        neubiberg = scenario.add_station("Neubiberg", np.deg2rad(49), np.deg2rad(10), np.rad2deg(100))
        logger.info("\nResults")
        logger.info("-"*100)
        logger.info(f"station name is {neubiberg.get_name()}")
        logger.info(f"station central body is {neubiberg.get_body().get_name()}")
        logger.info("-"*100)
        self.assertEqual(neubiberg.get_body(), scenario.get_central_body())

    def test_simulate(self):
        start_time = datetime.datetime.utcnow()
        duration = 24*60*60
        end_time = start_time + datetime.timedelta(seconds = duration)
        scenario = SimpleScenario("Test Scenario", start_time, end_time)    
        seranis = scenario.add_satellite("SeRANIS")
        seranis.add_environment_forces(self.forces_options)
        seranis_state_start = dict(sma = 6945.588024099e3, inclination=np.deg2rad(97.7), 
                                   eccentricity=0.00, raan=np.deg2rad(53.58),
                                   perigee=0.0, mean_anomaly=np.deg2rad(131.3468))
        seranis.set_initial_state(start_time, seranis_state_start)

        data = scenario.simulate()
        seranis_states = data.get(seranis.get_name())
        plt.plot([s.getA()/1e3 for s in seranis_states])
        plt.show()

    def test_simulate_with_reports(self):
        start_time = datetime.datetime.utcnow()
        duration = 24*60*60
        end_time = start_time + datetime.timedelta(seconds = duration)
        scenario = SimpleScenario("Test Scenario", start_time, end_time)    
        seranis = scenario.add_satellite("SeRANIS")
        seranis.add_environment_forces(self.forces_options)
        seranis_state_start = dict(sma = 6945.588024099e3, inclination=np.deg2rad(97.7), 
                                   eccentricity=0.00, raan=np.deg2rad(53.58),
                                   perigee=0.0, mean_anomaly=np.deg2rad(131.3468))
        seranis.set_initial_state(start_time, seranis_state_start)
        neubiberg = scenario.add_station("Neubiberg", np.deg2rad(49), np.deg2rad(10), np.rad2deg(100))
        reports_options =  {
            ReportTypes.GS_ACCESS.name: [
                {
                    "station": "Neubiberg",
                    "satellite": "SeRANIS"
                }
            ],
            ReportTypes.ECLIPSE.name: [
                "SeRANIS"
            ]
        }
        #  add reports to be calculated in one go
        scenario.add_reports(reports_options)
        options_before = scenario.get_reports_options()

        # add reports individually
        scenario.add_access_report(seranis.get_name(), neubiberg.get_name())
        scenario.add_eclipse_report(seranis.get_name())
        options_after = scenario.get_reports_options()

        self.assertEqual(options_before, options_after)
        
        # show reports data
        print("scenario report data before sim: \n ")
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(scenario.get_reports())

        # simulate
        data = scenario.simulate()
        seranis_states = data.get(seranis.get_name())

        # show reports data after simulation
        print("scenario report data after sim: \n ")
        pp.pprint(scenario.get_reports())

        plt.plot([s.getA()/1e3 for s in seranis_states])
        plt.show()

    def test_simulate_plot_reports(self):
        start_time = datetime.datetime.utcnow()
        duration = 24*60*60
        end_time = start_time + datetime.timedelta(seconds = duration)
        scenario = SimpleScenario("Test Scenario", start_time, end_time)    
        seranis = scenario.add_satellite("SeRANIS")
        seranis.add_environment_forces(self.forces_options)
        seranis_state_start = dict(sma = 6945.588024099e3, inclination=np.deg2rad(97.7), 
                                   eccentricity=0.00, raan=np.deg2rad(53.58),
                                   perigee=0.0, mean_anomaly=np.deg2rad(131.3468))
        seranis.set_initial_state(start_time, seranis_state_start)
        neubiberg = scenario.add_station("Neubiberg", np.deg2rad(49), np.deg2rad(10), np.rad2deg(100))
        reports_options =  {
            ReportTypes.GS_ACCESS.name: [
                {
                    "station": "Neubiberg",
                    "satellite": "SeRANIS"
                }
            ],
            ReportTypes.ECLIPSE.name: [
                "SeRANIS"
            ]
        }

        #  add reports to be calculated in one go
        scenario.add_reports(reports_options)

        # simulate
        data = scenario.simulate()

        # get data
        # seranis_states = data.get(seranis.get_name())

        # plot
        scenario.plot_access_report(seranis.get_name(), neubiberg.get_name())
        scenario.plot_eclipse_report(seranis.get_name())
        scenario.plot_ground_track(seranis.get_name())
        scenario.show_plots()
