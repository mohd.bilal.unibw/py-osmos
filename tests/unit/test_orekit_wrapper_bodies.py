from orekit.pyhelpers import setup_orekit_curdir
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.globals import VM
from config import OREKIT_DATA
import unittest
import orekit
import datetime
import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class TestBodies(unittest.TestCase):
    def setUp(self) -> None:
        self.stream_handler = logging.StreamHandler(sys.stdout)
        logger.addHandler(self.stream_handler)
        return super().setUp()
    
    def tearDown(self) -> None:
        logger.removeHandler(self.stream_handler)
        return super().tearDown()

    def test_earth_constructor(self):
        earth = Earth()
        logger.info(f"Body name is {earth.get_name()}")
        logger.info(f"Body Gravitational Constant is {earth.get_mu()}")
        logger.info(f"Body Shape is {earth.get_base_shape()}")
        logger.info(f"Body is initialized? {earth.is_initialized}")
        logger.info(f"Body Equatorial Radius is {earth.get_equatorial_radius()} meters")

    def test_earth_get_position(self):
        earth = Earth()
        base_position = earth.get_base_position()
        position = earth.get_position()
        logger.info(f"Body current base position is {base_position}")
        logger.info(f"Body current position is {position}")

