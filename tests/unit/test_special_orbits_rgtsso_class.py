from utils.astro.thin_orekit_wrapper.orbits.special_orbits import RGTSSOrbit, RepeatingGroundTrackOrbit, SSO
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft
from utils.astro.thin_orekit_wrapper.globals import VM

import unittest
import numpy as np
import datetime

class TestRGTSSOrbit(unittest.TestCase):
    def test_rgtsso(self):
        earth = Earth()
        perigee_height = 550e3
        eccentricity = 0
        hrs_from_noon = 6.0
        sma = (earth.get_equatorial_radius() + perigee_height)/(1 - eccentricity)
        sso = SSO(body=earth, sma=sma, eccentricity=eccentricity, hrs_from_noon=hrs_from_noon)
        print(f"inclination: {np.rad2deg(sso.get_inclination())}")
        rgt = RepeatingGroundTrackOrbit.get_minimum_eccentricity_rgto(sso.get_inclination(), perigee_height)
        print(rgt.get_revolutions_to_repeat())
        print(rgt.get_days_to_repeat())
        print(rgt.get_eccentricity())
        rgtsso = RGTSSOrbit(body=earth, eccentricity=eccentricity, k_revs2rep=rgt.get_revolutions_to_repeat(), k_days2rep=rgt.get_days_to_repeat(), perigee_height=perigee_height, hrs_from_noon=hrs_from_noon)
        kep = rgtsso.get_keplerian_elements()
        print(kep)
        print(f"height in km: {(kep.get('sma') - earth.get_equatorial_radius())/1000}")
        print(f"inclination in degrees: {np.rad2deg(kep['inclination'])}")
        print(f"raan: {kep['raan']}")

    def test_rgtsso_min_ecc(self):
        earth = Earth()
        perigee_height = 500e3
        hrs_from_noon = -1.0
        rgtsso = RGTSSOrbit.get_minimum_eccentricity_rgtsso(perigee_height, hrs_from_noon, body=earth, inclination_tolerance=np.deg2rad(0.01))
        kep = rgtsso.get_keplerian_elements()
        print(kep)
        print(f"perigee height in km: {(kep.get('sma')*(1-kep.get('eccentricity')) - earth.get_equatorial_radius())/1000}")
        print(f"apogee height in km: {(kep.get('sma')*(1+kep.get('eccentricity')) - earth.get_equatorial_radius())/1000}")

        print(f"inclination in degrees: {np.rad2deg(kep['inclination'])}")
        print(f"raan: {kep['raan']}")
        print(f"revs2rep: {rgtsso.get_revolutions_to_repeat()}")
        print(f"days2rep: {rgtsso.get_days_to_repeat()}")