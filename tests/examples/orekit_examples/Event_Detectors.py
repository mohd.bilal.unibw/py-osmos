#!/usr/bin/env python
# coding: utf-8

# # Event Detectors in Orekit

# ## Authors
# 
# Lots of parts are directly from the orekit documentation on [propagation](https://www.orekit.org/site-orekit-10.1/architecture/propagation.html), with some updates, simplifications and Pythonification by Petrus Hyvönen, SSC
# 
# ## Learning Goals
# * *What are Event Detectors*: Why are these useful
# * *How do I use Event Detectors*: How is it implemented in Orekit and Python
# 
# ## Keywords
# orekit, propagation, event detectors

# In[1]:




# Initialize orkit and bring up the python-java interface

# In[2]:


import orekit
vm = orekit.initVM()


# Now set up the pointer to the orekit-data.zip file, using one of the helper files. The file should be in current directory if not specified otherwise.

# In[3]:


from orekit.pyhelpers import setup_orekit_curdir, absolutedate_to_datetime
setup_orekit_curdir()


# Now we are set up to import and use objects from the orekit library. Packages can be imported as they were native Python packages

# # Event Detectors
# 
# _Before starting this introduction, please make sure you have refreshed the tutorials on Orbit Definition and Propagation._

# The propagators in Orekit is part of an architecture that supports detecting certain discrete conditions that occur during the propagation. This can be that a spacecraft enters eclipse, becomes visible from a ground station, crosses the perigee or a number of other interesting things that may occur during the orbit.
# 
# This feature is activated by registering EventDetectors to the propagator. All proppagators in Orekit supports the EventDetector mechanism.

# Users can define their own EventDetectors but there are also several predefined EventDetectors
# already available, amongst which :
# 
# - a simple DateDetector, which is simply triggered at a predefined date, and can be reset to add new dates on the run (which is useful to set up delays starting when a previous event is been detected)
# 
# - an ElevationDetector, which is triggered at raising or setting time of a satellite with respect to a ground point, taking atmospheric refraction into account and either constant elevation or ground mask when threshold elevation is azimuth-dependent
# 
# - an ElevationExtremumDetector, which is triggered at maximum (or minimum) satellite elevation with respect to a ground point
# 
# - an AltitudeDetector which is triggered when satellite crosses a predefined altitude limit and can be used to compute easily operational forecasts
# 
# - a FieldOfViewDetector which is triggered when some target enters or exits a satellite sensor Field Of View (any shape),
# - a CircularFieldOfViewDetector which is triggered when some target enters or exits a satellite sensor Field Of View (circular shape),
# - a FootprintOverlapDetector which is triggered when a sensor Field Of View (any shape, even split in non-connected parts or containing holes) overlaps a geographic zone, which can be non-convex, split in different sub-zones, have holes, contain the pole,
# - a GeographicZoneDetector, which is triggered when the spacecraft enters or leave a zone, which can be non-convex, split in different sub-zones, have holes, contain the pole,
# - a GroundFieldOfViewDetector, which is triggered when the spacecraft enters or leave a ground based Field Of View, which can be non-convex, split in different sub-zones, have holes,
# - an EclipseDetector, which is triggered when some body enters or exits the umbra or the penumbra of another occulting body,
# - an ApsideDetector, which is triggered at apogee and perigee,
# - a NodeDetector, which is triggered at ascending and descending nodes,
# - a PositionAngleDetector, which is triggered when satellite angle on orbit crosses some value (works with either anomaly, latitude argument or longitude argument and with either true, eccentric or mean angles),
# - LatitudeCrossingDetector, LatitudeExtremumDetector, LongitudeCrossingDetector, LongitudeExtremumDetector, which are triggered when satellite position with respect to central body reaches some predefined values,
# - an AlignmentDetector, which is triggered when satellite and some body projected in the orbital plane have a specified angular separation (the term AlignmentDetector is clearly a misnomer as the angular separation may be non-zero),
# - an AngularSeparationDetector, which is triggered when angular separation between satellite and some beacon as seen by an observer goes below a threshold. The beacon is typically the Sun, the observer is typically a ground station
# - An EventShifter is also provided in order to slightly shift the events occurrences times. A typical use case is for handling operational delays before or after some physical event really occurs.
# 
# An EventSlopeFilter is provided when user is only interested in one kind of events that occurs in pairs like raising in the raising/setting pair for elevation detector, or eclipse entry in the entry/exit pair for eclipse detector. The filter does not simply ignore events after they have been detected, it filters them before they are located and hence save some computation time by not doing an accurate search for events that will ultimately be ignored.
# 
# An EventEnablingPredicateFilter is provided when user wants to filter out some events based on an external condition set up by a user-provided enabling predicate function. This allow for example to dynamically turn some events on and off during propagation or to set up some elaborate logic like triggering on elevation first time derivative (i.e. one elevation maximum) but only when elevation itself is above some threshold. 
# 
# A BooleanDetector is provided to combine several other detectors with boolean operators and, or and not. This allows for example to detect when a satellite is both visible from a ground station and out of eclipse.

# In[4]:


from org.orekit.orbits import KeplerianOrbit, PositionAngle
from org.orekit.propagation.analytical import KeplerianPropagator
from org.orekit.time import AbsoluteDate, TimeScalesFactory
from org.orekit.utils import Constants, IERSConventions
from org.orekit.frames import FramesFactory
from org.orekit.bodies import OneAxisEllipsoid, CelestialBodyFactory


# In[5]:


from math import radians, degrees


# In[6]:


utc = TimeScalesFactory.getUTC()


# Let us do a small example, based on the orbit we used in the Propagation tutorial.

# In[7]:


ra = 500 * 1000         #  Apogee
rp = 400 * 1000         #  Perigee
i = radians(87.0)      # inclination
omega = radians(20.0)   # perigee argument
raan = radians(10.0)  # right ascension of ascending node
lv = radians(0.0)    # True anomaly

epochDate = AbsoluteDate(2020, 1, 1, 0, 0, 00.000, utc)
initial_date = epochDate

a = (rp + ra + 2 * Constants.WGS84_EARTH_EQUATORIAL_RADIUS) / 2.0    
e = 1.0 - (rp + Constants.WGS84_EARTH_EQUATORIAL_RADIUS) / a

## Inertial frame where the satellite is defined
inertialFrame = FramesFactory.getEME2000()

## Orbit construction as Keplerian
initialOrbit = KeplerianOrbit(a, e, i, omega, raan, lv,
                              PositionAngle.TRUE,
                              inertialFrame, epochDate, Constants.WGS84_EARTH_MU)
initialOrbit


# In[8]:


propagator = KeplerianPropagator(initialOrbit)


# ## Adding Event Detectors

# In the first example we create an EventDetector for eclipse, when the satellite is not illuminated by the Sun, and is in the full shadow of the Earth.

# In[9]:


ITRF = FramesFactory.getITRF(IERSConventions.IERS_2010, True)
earth = OneAxisEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS, 
                         Constants.WGS84_EARTH_FLATTENING, 
                         ITRF)


# In[10]:


sun = CelestialBodyFactory.getSun()
sunRadius = 696000000.0


# In[11]:


from org.orekit.propagation.events import EclipseDetector, EventsLogger
from org.orekit.propagation.events.handlers import ContinueOnEvent


# The EclipseDetector class is documented at the [Orekit API](https://www.orekit.org/site-orekit-latest/apidocs/org/orekit/propagation/events/EclipseDetector.html) and will detect entering and leaving the full shadow (Umbra) or when some part of the Sun is covered by Earth (Penumbra). 
# 
# In the detector, we can also set which EventHandler that we want to use, we can also write our own. In this case we use an EventHandler that will just let the propagator continue after an event has been detected.

# In[12]:


eclipse_detector = EclipseDetector(sun, sunRadius, earth).withUmbra().withHandler(ContinueOnEvent())


# There are several ways to collect these events when they happen, one of the ways is to use an Orekit EventsLogger that will store the events during the propagation.

# In[13]:


logger = EventsLogger()
logged_detector = logger.monitorDetector(eclipse_detector)


# We add the eclipse detector, together with the eventslogger to our propagator.

# In[14]:


propagator.addEventDetector(logged_detector)


# The propagation is executed in same way as in previous examples.

# In[15]:


state = propagator.propagate(initial_date, initial_date.shiftedBy(3600.0 * 24))
state.getDate()


# Now we can fetch the events that the logger found.

# In[16]:


events = logger.getLoggedEvents()
events.size()


# This is a code snippet that goes through the events and store them in a Pandas DataFrame that is very useful for handling tables in Python. In this, the dates are converted also to Python DateTime objects. Please note that if any further use of the data in Orekit is to be done, it is advisable also to save the Orekit AbsoluteDate.

# In[17]:


start_time = None
result = []
for event in logger.getLoggedEvents():
   
    if not event.isIncreasing():
        start_time = event.getState().getDate()
    elif start_time:
        stop_time = event.getState().getDate()
        result.append({"Start":absolutedate_to_datetime(start_time), 
                        "Stop":absolutedate_to_datetime(stop_time), 
                        "EclipseDuration": stop_time.durationFrom(start_time)/60})
        start_time = None
print(result)


# # Exercise 1

# In this case you are to calculate the ground station visibilities for the above orbit, with a minimum elevation of 5 degrees above the horizon. The detector to use is the [ElevationDetector](https://www.orekit.org/site-orekit-latest/apidocs/org/orekit/propagation/events/ElevationDetector.html). Create a similar pandas table as above of the start / stop time of the visibilities.

# In[ ]:





# # Exercise 2

# The satellite you are simulating does not have any batteries onboard, so it needs to be fully in sunlight to operate. Perform a calculation of when your ground station has visibility to the spacecraft, and that the spacecraft is in full sunlight. 
# 
# Hint: Check the BooleanDetector, but there are other ways to do this too!
# 

# In[ ]:


import plotly.express as px
df = px.data.gapminder().query("year == 2007")
fig = px.scatter_geo(df, locations="iso_alpha",
                     size="pop", # size of markers, "pop" is one of the columns of gapminder
                     )
fig.show()




