import orekit
from org.orekit.utils import Constants 
from org.orekit.bodies import CelestialBodyFactory
from org.orekit.utils import PVCoordinatesProvider
from org.orekit.time import AbsoluteDate, TimeScalesFactory
from org.orekit.frames import FramesFactory
from orekit.pyhelpers import setup_orekit_curdir, absolutedate_to_datetime
from org.orekit.propagation.sampling import PythonOrekitFixedStepHandler 
import numpy as np

vm = orekit.initVM()
setup_orekit_curdir(
    "C:/Users/Mohd Bilal/Documents/Bilal/VSCodeWorkspace/seranis_mission_simulator/orekit-data.zip"
)

print (Constants.WGS84_EARTH_EQUATORIAL_RADIUS)


sun = CelestialBodyFactory.getSun()     # Here we get it as an CelestialBody
sun = PVCoordinatesProvider.cast_(sun)  # But we want the PVCoord interface

# Get the position of the sun in the Earth centered coordinate system EME2000
date = AbsoluteDate(2020, 2, 28, 23, 30, 0.0, TimeScalesFactory.getUTC())
print(sun.getPVCoordinates(date, FramesFactory.getEME2000()).getPosition())

class StepHandler(PythonOrekitFixedStepHandler):
    
    def __init__(self, my_value):
        self.my_value = my_value
        super(StepHandler, self).__init__()

    def init(self, s0, t, step):
        pass

    def handleStep(self, state, isLast):
        # your code goes here that is executed for every step
        pass