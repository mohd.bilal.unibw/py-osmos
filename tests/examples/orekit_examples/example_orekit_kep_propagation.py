import orekit
from org.orekit.orbits import KeplerianOrbit, PositionAngle
from org.orekit.propagation.analytical import KeplerianPropagator
from org.orekit.time import AbsoluteDate, TimeScalesFactory
from org.orekit.utils import Constants
from org.orekit.frames import FramesFactory
from org.orekit.propagation.analytical import EcksteinHechlerPropagator
from org.orekit.orbits import OrbitType
from orekit.pyhelpers import setup_orekit_curdir, absolutedate_to_datetime
import numpy as np

vm = orekit.initVM()
setup_orekit_curdir(
    "C:/Users/Mohd Bilal/Documents/Bilal/VSCodeWorkspace/seranis_mission_simulator/orekit-data.zip"
)

utc = TimeScalesFactory.getUTC()
ra = float(500.0 * 1000)  #  Apogee
rp = float(400.0 * 1000)  #  Perigee
i = float(np.deg2rad(87.0))  # inclination
omega = float(np.deg2rad(20.0))  # perigee argument
raan = float(np.deg2rad(10.0))  # right ascension of ascending node
lv = float(np.deg2rad(0.0))  # True anomaly

epochDate = AbsoluteDate(2020, 1, 1, 0, 0, 00.000, utc)
initialDate = epochDate

a = float((rp + ra + 2 * Constants.WGS84_EARTH_EQUATORIAL_RADIUS) / 2.0)
e = float(1.0 - (rp + Constants.WGS84_EARTH_EQUATORIAL_RADIUS) / a)

## Inertial frame where the satellite is defined
inertialFrame = FramesFactory.getEME2000()

## Orbit construction as Keplerian
initialOrbit = KeplerianOrbit(a, e, i, omega, raan, lv, PositionAngle.MEAN,
                              inertialFrame, epochDate,
                              Constants.WGS84_EARTH_MU)
propagator = KeplerianPropagator(initialOrbit)
print(propagator.getInitialState())


from org.orekit.propagation.analytical import EcksteinHechlerPropagator
from org.orekit.orbits import OrbitType

propagator_eh = EcksteinHechlerPropagator(initialOrbit, 
                                        Constants.EIGEN5C_EARTH_EQUATORIAL_RADIUS,
                                        Constants.EIGEN5C_EARTH_MU, Constants.EIGEN5C_EARTH_C20,
                                        Constants.EIGEN5C_EARTH_C30, Constants.EIGEN5C_EARTH_C40,
                                        Constants.EIGEN5C_EARTH_C50, Constants.EIGEN5C_EARTH_C60)

propagator_eh.getInitialState() 
end_state = propagator_eh.propagate(initialDate, initialDate.shiftedBy(3600.0 * 48))
print(end_state.getPVCoordinates())

OrbitType.KEPLERIAN.convertType(end_state.getOrbit()) 