#!/usr/bin/env python
# coding: utf-8

# In[1]:


from pylatex import Document, Section, Subsection, Command
from pylatex.utils import italic, NoEscape
from pylatex.base_classes import SpecialOptions


# In[2]:


geometry_options = {
    "head": "40pt",
    "top": "2cm",
    "bottom": "2cm",
    "left": "3cm",
    "right": "3cm",
    "includeheadfoot": True
}


# In[3]:


doc = Document("EPD", geometry_options=geometry_options)
doc.__dict__


# In[4]:


class TestReport(Document):
    def __init__(self, scenario, **kwargs):
        self.geometry_options = kwargs.get("geometry_options") or {
            "head": "40pt",
            "top": "2cm",
            "bottom": "2cm",
            "left": "3cm",
            "right": "3cm",
            "includeheadfoot": True
        }
        super().__init__(geometry_options=geometry_options)
        self.scenario = scenario
        self.documentclass = Command('documentclass',
                                     arguments=['article'],
                                     options=['titlepage'])
        self.preamble.append(Command('title', 'Mission Analysis Report'))
        self.preamble.append(Command('date', NoEscape(r'\today')))
        self.append(NoEscape(r'\maketitle'))

        self.introduction_section = None
        self.gs_access_section = None
        self.eclipse_section = None
        self.sensor_coverage_section = None
        self.ground_track_section = None
        self.power_section = None
        self.lifetime_section = None

    def _create_introduction_section(self, data):
        self.introduction_section = Section(f"Introduction")

    def _create_gs_access_section(self, satellite, station, data):
        self.gs_access_section = Section(f"Access Report of {satellite} | {station}")

    def _create_eclipse_section(self, satellite, data):
        self.eclipse_section = Section(f"Eclipse Report of {satellite}")

    def _create_sensor_coverage_section(self, satellite, sensors, data):
        self.sensor_coverage_section = Section(f"Sensor Coverage Report of {satellite}")

    def _create_ground_track_section(self, satellite, data, start=None, end=None):
        self.ground_track_section = Section(f"Ground Track Report of {satellite}")

    def _create_power_section(self, satellite, data):
        self.power_section = Section(f"Power Report of {satellite}")

    def _create_lifetime_section(self, satellite, data):
        self.lifetime_section = Section(f"Lifetime Report of {satellite}")

    def _add_section(self, section):
        with self.create(section):
            self.append("Data")

    def create_report(self):
        """Add a section, a subsection and some text to the document."""
        self._create_introduction_section("Data")
        self._create_gs_access_section("SeRANIS", "Neubiberg", "Data")
        self._create_eclipse_section("SeRANIS", "Data")
        self._create_sensor_coverage_section("SeRANIS", ["Ka Band"], "Data")
        self._create_ground_track_section("SeRANIS", "Data")
        self._create_power_section("SeRANIS", "Data")
        self._create_lifetime_section("SeRANIS", "Data")

        self._add_section(self.introduction_section)
        self._add_section(self.gs_access_section)
        self._add_section(self.eclipse_section)
        self._add_section(self.sensor_coverage_section)
        self._add_section(self.ground_track_section)
        self._add_section(self.power_section)
        self._add_section(self.lifetime_section)

        


# In[5]:


doc = TestReport("Test_Scenario")
doc.create_report()
doc.generate_pdf("test_report", compiler="pdflatex")

