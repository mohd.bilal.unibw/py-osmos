import datetime
import orekit
import logging
import numpy as np
import math
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from org.hipparchus.geometry.euclidean.threed import Line
from org.hipparchus.geometry.euclidean.threed import Vector3D
from org.orekit.utils import Constants
from org.orekit.bodies import CelestialBodyFactory, OneAxisEllipsoid, GeodeticPoint
from org.orekit.frames import FramesFactory, TopocentricFrame
from org.orekit.time import TimeScalesFactory
from org.orekit.orbits import KeplerianOrbit, PositionAngle, OrbitType
from org.orekit.propagation import SpacecraftState
from org.orekit.propagation.numerical import NumericalPropagator
from org.hipparchus.ode.nonstiff import DormandPrince853Integrator
from org.orekit.forces.gravity import HolmesFeatherstoneAttractionModel, ThirdBodyAttraction
from org.orekit.forces.gravity.potential import GravityFieldFactory
from org.orekit.forces.drag import IsotropicDrag, DragForce
from org.orekit.forces.radiation import IsotropicRadiationClassicalConvention, SolarRadiationPressure
from org.orekit.models.earth.atmosphere import HarrisPriester
from org.orekit.utils import IERSConventions
from org.orekit.propagation.events import EclipseDetector, EventDetector, ElevationDetector
from org.orekit.propagation.sampling import PythonOrekitFixedStepHandler
from org.orekit.propagation.events.handlers import EventHandler, PythonEventHandler
from org.orekit.propagation.events.handlers import ContinueOnEvent
from org.hipparchus.ode.events import Action 
import plotly.express as px
from orekit import JArray_double

vm = orekit.initVM()
from orekit.pyhelpers import setup_orekit_curdir, absolutedate_to_datetime, datetime_to_absolutedate

logger = logging.getLogger(__name__)


def UTC2JD(UTC):
    """
    calculates the Julian Day given UTC
    parameters: UTC - dateUTC.dateUTC
    return : Julian day i.e., days from 4713 BC
    """
    JD = 367 * UTC.year - int(7 * (UTC.year + int(
        (UTC.month + 9) / 12)) / 4) + int(
            275 * UTC.month / 9) + UTC.day + 1721013.5 + (
                (UTC.second / 60.0 + UTC.minute) / 60.0 + UTC.hour) / 24.0
    return JD


def JDC2GMST(t_cent):
    """
    return: Greenwich Mean Sidereal Time given the centuries of the Julian Day of the UTC
    values returned in radians
    """
    theta_gmst = 67310.54841 + (876600 * 3600 +
                                8640184.812866) * t_cent + 0.093104 * (
                                    t_cent**2) - 6.2 * 1e-6 * (t_cent**3)
    theta_gmst = theta_gmst % 86400 - 86400
    theta_gmst_deg = theta_gmst / 240
    if theta_gmst < 0:
        theta_gmst_deg = 360 + theta_gmst_deg
    return np.deg2rad(theta_gmst_deg)


# set up directory for orekit data
setup_orekit_curdir(
    "C:/Users/Mohd Bilal/Documents/Bilal/VSCodeWorkspace/seranis_mission_simulator/orekit-data.zip"
)
#  set up inertial frame
inertial_frame = FramesFactory.getEME2000()

#  set scenario time duration
utc_timescale = TimeScalesFactory.getUTC()
utc_dt = datetime.datetime.utcnow()
jd = UTC2JD(datetime.datetime.utcnow())
theta_gmst = JDC2GMST(jd)
start_time = datetime_to_absolutedate(utc_dt)
print(f"Scenario Times: \n"
      "------------------------------\n"
      f"UTC: {start_time.toString()} \n"
      f"Angle GMST: {theta_gmst}\n")
# set central body settings
earth = CelestialBodyFactory.getEarth()
mu_earth = earth.getGM()  # earth gravitational constant in m^3/s^2

## Define scenario components

# Ground Location
lat_munich = float(np.deg2rad(48.1351))
long_munich = float(np.deg2rad(11.5820))
altitude_munich = float(341.0)

# Orbit
# Let us define a repeating ground track orbit
n_days_repeat = 2
m = np.round(n_days_repeat * 86400 /
             (95 * 60))  # compute approx m in LEO 500km
orbit_period = n_days_repeat * 86400.0 / m
sma = float((mu_earth * orbit_period**2 / (4 * np.pi**2))**(1.0 / 3.0))
ecc = float(0.0145)
assert (sma * (1 - ecc) > Constants.WGS84_EARTH_EQUATORIAL_RADIUS
        )  # perigee distance should be less than Earth Radius
inc = lat_munich

# let us set the raan such that the satellite passes directly over munich
# these equations hold true for circular orbits only,
# but serve as good approx for our near circular orbit
arc_munich = np.arcsin(np.sin(lat_munich) / np.sin(inc))
long_raan = long_munich - np.arccos(np.cos(arc_munich) / np.cos(lat_munich))
raan = float(theta_gmst + long_raan)
perigee = float(arc_munich)  # set perigee over munich
nu = float(0.0)  # start simulation directly over munich

print(f"Orbit Characteristics\n"
      "------------------------------\n"
      f"type: Repeating Ground Track\n"
      f"orbit_period (mins): {orbit_period/60}\n"
      f"semi-major axis (km): {sma/1e3} \n"
      f"height (km): {(sma - Constants.WGS84_EARTH_EQUATORIAL_RADIUS)/1e3} \n"
      f"eccentricity: {ecc} \n"
      f"raan (deg): {np.rad2deg(raan)} \n"
      f"argument of perigee (deg): {np.rad2deg(perigee)} \n"
      f"true anomaly (deg): {np.rad2deg(nu)} \n"
      f"Ground track repeats (days): {n_days_repeat} \n")

# initialize orbit, initial attitude, and spacecraft state
initial_orbit = KeplerianOrbit(sma, ecc, inc, perigee, raan, nu,
                               PositionAngle.TRUE, inertial_frame, start_time,
                               Constants.WGS84_EARTH_MU)
mass_sc = float(700.0)  # mass of spacecraft in kg

# attitude laws for satellite
day_observation_law = LofOffset(initial_orbit)


sc_drag_area = 0.38  # drag area in m2
sc_cd = 2.25  # drag coefficient
sc_srp_area = 0.55  # cross section for solar radiation reflection
sc_ca = 0.5  # absorption coefficent
sc_cr = 0.5  # reflection coefficient
init_sc_state = SpacecraftState(initial_orbit, mass_sc)

# set up propagator
min_step = float(0.001)
max_step = float(10.0)
position_tolerance = float(1.0)
propagation_type = OrbitType.KEPLERIAN
tolerances = NumericalPropagator.tolerances(position_tolerance, initial_orbit,
                                            propagation_type)
integator = DormandPrince853Integrator(min_step, max_step,
                                       JArray_double.cast_(tolerances[0]),
                                       JArray_double.cast_(tolerances[1]))
propagator = NumericalPropagator(integator)
propagator.setOrbitType(propagation_type)

# define force models
earth_frame = FramesFactory.getITRF(IERSConventions.IERS_2010, True)
# gravity field
gravity_provider = GravityFieldFactory.getNormalizedProvider(10, 10)
force_model_gravity = HolmesFeatherstoneAttractionModel(
    earth_frame, gravity_provider)

# third body perturbation model
force_model_3b_moon = ThirdBodyAttraction(CelestialBodyFactory.getMoon())
force_model_3b_sun = ThirdBodyAttraction(CelestialBodyFactory.getSun())

# define drag force
earth_ellipsoid = OneAxisEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
                                   Constants.WGS84_EARTH_FLATTENING,
                                   earth_frame)
atmosphere_model = HarrisPriester(CelestialBodyFactory.getSun(),
                                  earth_ellipsoid, float(4.0))
drag_sensitive_sc = IsotropicDrag(
    sc_drag_area, sc_cd)  # replace with box and solar array spacecraft
force_model_drag = DragForce(atmosphere_model, drag_sensitive_sc)

# define solar radiation pressure
srp_sensitive_sc = IsotropicRadiationClassicalConvention(
    sc_srp_area, sc_ca, sc_cr)
force_model_srp = SolarRadiationPressure(
    CelestialBodyFactory.getSun(), Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
    srp_sensitive_sc)

# add forces models to propagator
propagator.addForceModel(force_model_gravity)
propagator.addForceModel(force_model_drag)
propagator.addForceModel(force_model_srp)

# initialize propagator
propagator.setInitialState(init_sc_state)

# define station
station = GeodeticPoint(lat_munich, long_munich, altitude_munich)
station_frame = TopocentricFrame(earth_ellipsoid, station, "Neubiberg")

# define sensors
SENSOR_HALF_ANGLE = float(np.deg2rad(35.0))
ir_camera_footprint_l = float(125000.0) # half footprint from EPD
# taking nadir pointing to compute camera cone angle
RE = Constants.WGS84_EARTH_EQUATORIAL_RADIUS
lambda_nadir = ir_camera_footprint_l/RE # half central angle
slant_length = np.sqrt(RE**2 + sma**2 - 2*RE*sma*np.cos(lambda_nadir))

ir_camera_half_angle = float(np.arcsin(RE*np.sin(lambda_nadir)/slant_length))
print(f"Half Cone of Camera is : {np.rad2deg(ir_camera_half_angle)} deg")
ir_camera_dwell_time = float(0.1) # dwell time of camera in seconds. the time the camera looks at one location
ele_min = float(np.deg2rad(5.0))

class ElevationDetectorHandler(PythonEventHandler):
    def __init__(self):
        super(ElevationDetectorHandler, self).__init__()
        self.times = {"start":[], "end":[]}

    def init(self, s, T):
        pass

    def eventOccurred(self, s, detector, increasing):
        # det = EclipseDetector.cast_(detector)
        ele = station_frame.getElevation(s.getPVCoordinates().getPosition(), inertial_frame, s.getDate())
        if increasing:
            print(f"Spacecraft in view of station")
            self.times["start"].append(absolutedate_to_datetime(s.getDate()))
        else:
            print("Spacecraft out of view...")
            self.times["end"].append(absolutedate_to_datetime(s.getDate()))
        return Action.CONTINUE

    def resetState(self, detector, oldState):
        return oldState

class myNightEclipseDetector(PythonEventHandler):
    
    def init(self, s, T):
        pass
    
    def eventOccurred(self, s, detector, increasing):
        if not increasing:
            print("\n", s.getDate()," : event occurred, entering eclipse => switching to night law\n")
        return Action.CONTINUE
    
    def resetState(self, detector, oldState):
        return oldState


class myDayEclipseDetector(PythonEventHandler):
    
    def init(self, s, T):
        pass
    
    def eventOccurred(self, s, detector, increasing):
        if increasing:
            print("\n", s.getDate()," : event occurred, exiting eclipse => switching to day law\n")
        return Action.CONTINUE
    
    def resetState(self, detector, oldState):
        return oldState

class PropagationHandler(PythonOrekitFixedStepHandler):
    
    def __init__(self, angle):
            # set up Earth model
            self.earth = OneAxisEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
                                         Constants.WGS84_EARTH_FLATTENING,
                                         FramesFactory.getITRF(IERSConventions.IERS_2010, False))
            
            # set up position offsets, using Earth radius as an arbitrary distance
            self.deltaR = Constants.WGS84_EARTH_EQUATORIAL_RADIUS * math.cos(math.radians(angle))
            self.deltaC = Constants.WGS84_EARTH_EQUATORIAL_RADIUS * math.sin(math.radians(angle))

            # prepare an empty corridor
            self.dates = []
            self.lefts = []
            self.centers = []
            self.rights = []

            # prepare empty states
            self.inert_pos = []
            self.inert_vel = []
            
            # prepare elevations
            self.elevations = []
            self.footprint_area = []
            self.access_area = []
            self.angle = angle
            
            # Initialize the superclass as well
            super(PropagationHandler, self).__init__()
            
    def init(self, s0, t, step):
        # needs to be stated to fulfill the interface specification
        pass
    
    def handleStep(self, currentState, isLast):
        # compute sub-satellite track
        date    = currentState.getDate()
        pvInert = currentState.getPVCoordinates()
        t       = currentState.getFrame().getTransformTo(self.earth.getBodyFrame(), date)
        p       = t.transformPosition(pvInert.getPosition())
        v       = t.transformVector(pvInert.getVelocity())
        center  = self.earth.transform(p, self.earth.getBodyFrame(), date)
        
        # compute left and right corridor points
        nadir      = p.normalize().negate()
        crossTrack = p.crossProduct(v).normalize()
        leftLine   = Line(p, Vector3D(1.0, p, self.deltaR, nadir,  self.deltaC, crossTrack), 1.0)
        left       = self.earth.getIntersectionPoint(leftLine, p, self.earth.getBodyFrame(), date)
        rightLine  = Line(p, Vector3D(1.0, p, self.deltaR, nadir, -self.deltaC, crossTrack), 1.0)
        right      = self.earth.getIntersectionPoint(rightLine, p, self.earth.getBodyFrame(), date)
        

        # add the corridor points
        self.dates.append(date)
        self.lefts.append(left)
        self.centers.append(center)
        self.rights.append(right)

        sc_inert_pos = [pvInert.getPosition().getX(), pvInert.getPosition().getY(), pvInert.getPosition().getZ()]
        self.inert_pos.append(sc_inert_pos)
        self.elevations.append(station_frame.getElevation(pvInert.getPosition(), inertial_frame, currentState.getDate()))
        dist_sc_earth = np.linalg.norm(sc_inert_pos)
        sin_rho = RE/(dist_sc_earth)
        epsilon = np.arccos(np.sin(self.angle)/sin_rho)
        lambda_nadir = np.pi/2 - epsilon - self.angle
        height_cone = RE*(1-np.cos(lambda_nadir))
        fp_area = 2*np.pi*RE*height_cone
        self.footprint_area.append(fp_area)

        nadir_angle_max = np.arcsin(sin_rho*np.cos(ele_min))
        central_angle_max = np.pi/2 - ele_min - nadir_angle_max
        access_area = 2.55604187e8*(1 - np.cos(central_angle_max)) # already in km2
        self.access_area.append(access_area)



sun =  CelestialBodyFactory.getSun()

dayNightEvent = EclipseDetector(sun, float(696000000.0), earth_ellipsoid)
dayNightEvent = dayNightEvent.withHandler(myNightEclipseDetector().of_(EclipseDetector))
nightDayEvent = EclipseDetector(sun, float(696000000.0), earth_ellipsoid)
nightDayEvent = nightDayEvent.withHandler(myDayEclipseDetector().of_(EclipseDetector))

ele_detector_handler = ElevationDetectorHandler().of_(ElevationDetector)
elevationEvent = ElevationDetector(station_frame).withConstantElevation(ele_min).withHandler(ele_detector_handler)
# propagator.addEventDetector(dayNightEvent)
# propagator.addEventDetector(nightDayEvent)
propagator.addEventDetector(elevationEvent)

STEP = float(60.0)
DURATION = float(5.0 * 86400)
handler = PropagationHandler(ir_camera_half_angle)
propagator.setMasterMode(STEP, handler)

# propagate
propagator.propagate(start_time, start_time.shiftedBy(DURATION))


def geoline(geopoints):
    lon = [math.degrees(x.getLongitude()) for x in geopoints]
    lat = [math.degrees(x.getLatitude()) for x in geopoints]
    return lon, lat


# Another library for mapping is cartopy, which has a real geodetic plotting, avoiding errornous lines at map borders.


fig = plt.figure("Flat")
ax = plt.axes(projection=ccrs.PlateCarree())
ax.coastlines()
lon, lat = geoline(handler.centers)
ax.plot(lon, lat, transform=ccrs.Geodetic(), alpha=0.6, color='green', zorder=3)
ground_track = {"latitude": lat, "longitude": lon}

lon, lat = geoline(handler.lefts)
ax.plot(lon, lat, transform=ccrs.Geodetic(), alpha=0.6, color='blue', zorder=3)

lon, lat = geoline(handler.rights)
ax.plot(lon, lat, transform=ccrs.Geodetic(), alpha=0.6, color='blue', zorder=3)


# Different projections makes it more readable


fig = plt.figure("Orthographic")
ax = plt.axes(projection=ccrs.Orthographic(central_longitude=0.0, central_latitude=90.0, globe=None))
ax.coastlines()

ax.set_global()

ax.gridlines()
lon, lat = geoline(handler.centers)
ax.plot(lon, lat, transform=ccrs.Geodetic(), alpha=0.6, color='green', zorder=3)

lon, lat = geoline(handler.lefts)
ax.plot(lon, lat, transform=ccrs.Geodetic(), alpha=0.6, color='blue', zorder=3)

lon, lat = geoline(handler.rights)
ax.plot(lon, lat, transform=ccrs.Geodetic(), alpha=0.6, color='blue', zorder=3)

# adding eci plots
t_vec_datetime = [utc_dt + datetime.timedelta(seconds=s) for s in range(0, int(DURATION + STEP), int(STEP))]

fig1 = plt.figure("XYZ Positions")
ax1 = fig1.add_subplot(111)
ax1.plot(t_vec_datetime, handler.inert_pos)

fig2 = plt.figure("Elevation")
ax2 = fig2.add_subplot(111)
ax2.plot(t_vec_datetime, np.rad2deg(handler.elevations))

time_in_view = []
for t_start, t_end in zip(ele_detector_handler.times.get("start"), ele_detector_handler.times.get("end")):
    ax2.plot([t_start, t_start], [-75, 90], color='green',linestyle='dashed', linewidth=1)
    ax2.plot([t_end, t_end], [-75, 90], color='red',linestyle='dashed', linewidth=1)
    time_in_view.append((t_end - t_start).total_seconds())


fig3 = plt.figure("Footprint Area")
ax3 = fig3.add_subplot(111)
ax3.plot(t_vec_datetime, np.array(handler.footprint_area)/1e6) # footprint area in km2

fig_ar = plt.figure("Access Area")
ax_ar = fig_ar.add_subplot(111)
ax_ar.plot(t_vec_datetime, handler.access_area) # access area in km2

fig4 = plt.figure("Overpasses")
ax4 = fig4.add_subplot(111)
ax4.set_ylim(-20, 20)
time_in_view = []

visibility_start_times = ele_detector_handler.times.get("start")
visibility_end_times = ele_detector_handler.times.get("end")

for t_start, t_end in zip(visibility_start_times, visibility_end_times):
    ax4.plot([t_start, t_start], [-5, 5], color='green', linewidth=2)
    ax4.plot([t_start, t_end], [0, 0], color='red', linewidth=1)
    ax4.plot([t_end, t_end], [-5, 5], color='green', linewidth=2)
    time_in_view.append((t_end - t_start).total_seconds())
avg_time_in_view = np.mean(time_in_view)

percent_coverage_station = np.sum(time_in_view)/DURATION

gap_times = np.array(visibility_start_times[1:]) - np.array(visibility_end_times[:-1])
gap_times_seconds = [g.total_seconds() for g in gap_times]
max_gap_time = np.max(gap_times_seconds)
time_avg_gap = np.linalg.norm(gap_times_seconds)**2/DURATION

print(f"avg time in view (minutes): {avg_time_in_view/60}")
print(f"Percentage Coverage for Munich is: {percent_coverage_station} \n")
print(f"maximum gap time (minutes): {max_gap_time/60}")
print(f"average gap duration (minutes):{np.mean(gap_times_seconds)/60}")
print(f"time averaged gap duration (minutes):{time_avg_gap/60}")


# TODO: 
# Compute coverage for Earth Observation 

print("simulation finished. check plots!")
fig_px = px.scatter_geo(ground_track,
                        lat='latitude',
                        lon='longitude',
                        opacity=0.3,
                        projection='orthographic')

fig_gt_flat = plt.figure("Ground Track Flat")
ax_gt_flat = plt.axes(projection=ccrs.PlateCarree())
ax_gt_flat.coastlines()
lon, lat = geoline(handler.centers)
ax_gt_flat.plot(lon, lat, transform=ccrs.Geodetic(), alpha=0.6, color='green', zorder=3)
ax_gt_flat.plot([np.rad2deg(long_munich)], [np.rad2deg(lat_munich)], transform=ccrs.Geodetic(), alpha=0.6, color='red', zorder=3, marker='o')


fig_gt = plt.figure("Ground Track Ortho")
ax_gt = plt.axes(projection=ccrs.Orthographic(central_longitude=0.0, central_latitude=90.0, globe=None))
ax_gt.coastlines()
ax_gt.set_global()
ax_gt.gridlines()
ax_gt.plot(lon, lat, transform=ccrs.Geodetic(), alpha=0.6, color='green', zorder=3)
ax_gt.plot([np.rad2deg(long_munich)], [np.rad2deg(lat_munich)], transform=ccrs.Geodetic(), alpha=0.6, color='red', zorder=3, marker='o')

# fig_px.show()
plt.show()


