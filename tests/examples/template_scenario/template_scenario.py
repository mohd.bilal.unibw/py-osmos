import datetime
import orekit
import logging
import numpy as np
import matplotlib.pyplot as plt
from org.orekit.utils import Constants
from org.orekit.bodies import CelestialBodyFactory, OneAxisEllipsoid, GeodeticPoint
from org.orekit.frames import FramesFactory, TopocentricFrame
from org.orekit.time import TimeScalesFactory
from org.orekit.orbits import KeplerianOrbit, PositionAngle, OrbitType
from org.orekit.propagation import SpacecraftState
from org.orekit.propagation.numerical import NumericalPropagator
from org.hipparchus.ode.nonstiff import DormandPrince853Integrator
from org.orekit.forces.gravity import HolmesFeatherstoneAttractionModel, ThirdBodyAttraction
from org.orekit.forces.gravity.potential import GravityFieldFactory
from org.orekit.forces.drag import IsotropicDrag, DragForce
from org.orekit.forces.radiation import IsotropicRadiationClassicalConvention, SolarRadiationPressure
from org.orekit.models.earth.atmosphere import HarrisPriester
from org.orekit.utils import IERSConventions
from orekit import JArray_double

vm = orekit.initVM()
from orekit.pyhelpers import setup_orekit_curdir, absolutedate_to_datetime, datetime_to_absolutedate

logger = logging.getLogger(__name__)


def UTC2JD(UTC):
    """
    calculates the Julian Day given UTC
    parameters: UTC - dateUTC.dateUTC
    return : Julian day i.e., days from 4713 BC
    """
    JD = 367 * UTC.year - int(7 * (UTC.year + int(
        (UTC.month + 9) / 12)) / 4) + int(
            275 * UTC.month / 9) + UTC.day + 1721013.5 + (
                (UTC.second / 60.0 + UTC.minute) / 60.0 + UTC.hour) / 24.0
    return JD


def JDC2GMST(t_cent):
    """
    return: Greenwich Mean Sidereal Time given the centuries of the Julian Day of the UTC
    values returned in radians
    """
    theta_gmst = 67310.54841 + (876600 * 3600 +
                                8640184.812866) * t_cent + 0.093104 * (
                                    t_cent**2) - 6.2 * 1e-6 * (t_cent**3)
    theta_gmst = theta_gmst % 86400 - 86400
    theta_gmst_deg = theta_gmst / 240
    if theta_gmst < 0:
        theta_gmst_deg = 360 + theta_gmst_deg
    return np.deg2rad(theta_gmst_deg)


# set up directory for orekit data
setup_orekit_curdir(
    "C:/Users/Mohd Bilal/Documents/Bilal/VSCodeWorkspace/seranis_mission_simulator/orekit-data.zip"
)
#  set up inertial frame
inertial_frame = FramesFactory.getEME2000()

#  set scenario time duration
utc_timescale = TimeScalesFactory.getUTC()
utc_dt = datetime.datetime.utcnow()
jd = UTC2JD(datetime.datetime.utcnow())
theta_gmst = JDC2GMST(jd)
start_time = datetime_to_absolutedate(utc_dt)
print(f"Scenario Times: \n"
      "------------------------------\n"
      f"UTC: {start_time.toString()} \n"
      f"Angle GMST: {theta_gmst}\n")
# set central body settings
earth = CelestialBodyFactory.getEarth()
mu_earth = earth.getGM()  # earth gravitational constant in m^3/s^2

## Define scenario components

# Ground Location
lat_munich = float(np.deg2rad(48.1351))
long_munich = float(np.deg2rad(-11.5820))
altitude_munich = float(341.0)

# Orbit
# Let us define a repeating ground track orbit
n_days_repeat = 2
m = np.round(n_days_repeat * 86400 /
             (95 * 60))  # compute approx m in LEO 500km
orbit_period = n_days_repeat * 86400.0 / m
sma = float((mu_earth * orbit_period**2 / (4 * np.pi**2))**(1.0 / 3.0))
ecc = float(0.0145)
assert (sma * (1 - ecc) > Constants.WGS84_EARTH_EQUATORIAL_RADIUS
        )  # perigee distance should be less than Earth Radius
inc = float(np.deg2rad(50.0))

# let us set the raan such that the satellite passes directly over munich
# these equations hold true for circular orbits only,
# but serve as good approx for our near circular orbit
arc_munich = np.arcsin(np.sin(lat_munich) / np.sin(inc))
long_raan = long_munich - np.arccos(np.cos(arc_munich) / np.cos(lat_munich))
raan = float(theta_gmst + long_raan)
perigee = float(arc_munich)  # set perigee over munich
nu = float(0.0)  # start simulation directly over munich

print(f"Orbit Characteristics\n"
      "------------------------------\n"
      f"type: Repeating Ground Track\n"
      f"orbit_period (mins): {orbit_period/60}\n"
      f"semi-major axis (km): {sma/1e3} \n"
      f"height (km): {(sma - Constants.WGS84_EARTH_EQUATORIAL_RADIUS)/1e3} \n"
      f"eccentricity: {ecc} \n"
      f"raan (deg): {np.rad2deg(raan)} \n"
      f"argument of perigee (deg): {np.rad2deg(perigee)} \n"
      f"true anomaly (deg): {np.rad2deg(nu)} \n"
      f"Ground track repeats (days): {n_days_repeat} \n")

# initialize ornit and spacecraft state
initial_orbit = KeplerianOrbit(sma, ecc, inc, perigee, raan, nu,
                               PositionAngle.TRUE, inertial_frame, start_time,
                               Constants.WGS84_EARTH_MU)
mass_sc = float(700.0)  # mass of spacecraft in kg
sc_drag_area = 0.38  # drag area in m2
sc_cd = 2.25  # drag coefficient
sc_srp_area = 0.55  # cross section for solar radiation reflection
sc_ca = 0.5  # absorption coefficent
sc_cr = 0.5  # reflection coefficient
init_sc_state = SpacecraftState(initial_orbit, mass_sc)

# set up propagator
min_step = float(0.001)
max_step = float(10.0)
position_tolerance = float(1.0)
propagation_type = OrbitType.KEPLERIAN
tolerances = NumericalPropagator.tolerances(position_tolerance, initial_orbit,
                                            propagation_type)
integator = DormandPrince853Integrator(min_step, max_step,
                                       JArray_double.cast_(tolerances[0]),
                                       JArray_double.cast_(tolerances[1]))
propagator = NumericalPropagator(integator)
propagator.setOrbitType(propagation_type)

# define force models
earth_frame = FramesFactory.getITRF(IERSConventions.IERS_2010, True)
# gravity field
gravity_provider = GravityFieldFactory.getNormalizedProvider(10, 10)
force_model_gravity = HolmesFeatherstoneAttractionModel(
    earth_frame, gravity_provider)

# third body perturbation model
force_model_3b_moon = ThirdBodyAttraction(CelestialBodyFactory.getMoon())
force_model_3b_sun = ThirdBodyAttraction(CelestialBodyFactory.getSun())

# define drag force
earth_ellipsoid = OneAxisEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
                                   Constants.WGS84_EARTH_FLATTENING,
                                   earth_frame)
atmosphere_model = HarrisPriester(CelestialBodyFactory.getSun(),
                                  earth_ellipsoid, float(4.0))
drag_sensitive_sc = IsotropicDrag(
    sc_drag_area, sc_cd)  # replace with box and solar array spacecraft
force_model_drag = DragForce(atmosphere_model, drag_sensitive_sc)

# define solar radiation pressure
srp_sensitive_sc = IsotropicRadiationClassicalConvention(
    sc_srp_area, sc_ca, sc_cr)
force_model_srp = SolarRadiationPressure(
    CelestialBodyFactory.getSun(), Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
    srp_sensitive_sc)

# add forces models to propagator
propagator.addForceModel(force_model_gravity)
propagator.addForceModel(force_model_drag)
propagator.addForceModel(force_model_srp)

# initialize propagator
propagator.setInitialState(init_sc_state)

# define station
station = GeodeticPoint(lat_munich, long_munich, altitude_munich)
station_frame = TopocentricFrame(earth_ellipsoid, station, "Neubiberg")

# propagate
time_step = 60
duration = 5 * 86400
t_vec = [
    start_time.shiftedBy(float(dt))
    for dt in np.arange(0, duration, time_step)
]

print("running simulation...")
sc_states = [propagator.propagate(tt) for tt in t_vec]
orbital_states = [
    KeplerianOrbit.cast_(OrbitType.KEPLERIAN.convertType(s.getOrbit()))
    for s in sc_states
]
sc_positions = [o.getPVCoordinates().getPosition() for o in orbital_states]
sc_xyz = np.array([[pos.getX(), pos.getY(), pos.getZ()]
                   for pos in sc_positions])
elevations = np.rad2deg(
    np.array([
        station_frame.getElevation(os.getPVCoordinates().getPosition(),
                                   inertial_frame, os.getDate())
        for os in orbital_states
    ]))

print("simulation finished...")
print("check plots!")
fig1 = plt.figure("XYZ Positions")
ax1 = fig1.add_subplot(111)
ax1.plot(sc_xyz)

fig2 = plt.figure("Elevation")
ax2 = fig2.add_subplot(111)
t_vec_datetime = [utc_dt + datetime.timedelta(seconds=s) for s in range(0, duration, time_step)]
ax2.plot(t_vec_datetime, elevations)

plt.show()
