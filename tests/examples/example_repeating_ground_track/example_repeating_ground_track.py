#!/usr/bin/env python
# coding: utf-8

# # Calculating Parameters for Repeating Ground Track Orbit

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import datetime
import orekit
import logging
import math
import cartopy.crs as ccrs


# In[2]:


# orekit imports
from orekit.pyhelpers import setup_orekit_curdir, absolutedate_to_datetime, datetime_to_absolutedate
from org.hipparchus.geometry.euclidean.threed import Line
from org.hipparchus.geometry.euclidean.threed import Vector3D
from org.orekit.utils import Constants, PVCoordinates, ExtendedPVCoordinatesProvider
from org.orekit.bodies import CelestialBodyFactory, OneAxisEllipsoid, GeodeticPoint
from org.orekit.frames import FramesFactory, TopocentricFrame
from org.orekit.time import TimeScalesFactory
from org.orekit.orbits import KeplerianOrbit, PositionAngle, OrbitType, CircularOrbit, EquinoctialOrbit
from org.orekit.propagation import SpacecraftState, PropagatorsParallelizer
from org.orekit.propagation.numerical import NumericalPropagator
from org.hipparchus.ode.nonstiff import DormandPrince853Integrator
from org.orekit.forces.gravity import HolmesFeatherstoneAttractionModel, ThirdBodyAttraction
from org.orekit.forces.gravity.potential import GravityFieldFactory
from org.orekit.forces.drag import IsotropicDrag, DragForce
from org.orekit.forces.radiation import IsotropicRadiationClassicalConvention, SolarRadiationPressure
from org.orekit.models.earth.atmosphere import HarrisPriester
from org.orekit.utils import IERSConventions
from org.orekit.propagation.events import EclipseDetector, EventDetector, ElevationDetector
from org.orekit.propagation.sampling import PythonOrekitFixedStepHandler
from org.orekit.propagation.events.handlers import EventHandler, PythonEventHandler
from org.orekit.propagation.events.handlers import ContinueOnEvent
from org.hipparchus.ode.events import Action
from org.orekit.forces import BoxAndSolarArraySpacecraft
from orekit import JArray_double


# In[3]:


# initialize virtual machine
vm = orekit.initVM()
# change path when changing machine or use relative path
orekit_data_path = "../../../orekit-data.zip"
setup_orekit_curdir(orekit_data_path)


# In[4]:


SIDEREAL_DAY_SECONDS =  86164.0905


# In[5]:


wE = 2*np.pi/SIDEREAL_DAY_SECONDS


# In[6]:


inertial_frame = FramesFactory.getEME2000()
earth_frame = FramesFactory.getITRF(IERSConventions.IERS_2010, True)


# In[7]:


earth = CelestialBodyFactory.getEarth()


# In[8]:


earth_pv = ExtendedPVCoordinatesProvider.cast_(earth)
earth_shape = OneAxisEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
                               Constants.WGS84_EARTH_FLATTENING, earth_frame)


# In[9]:


sun =  CelestialBodyFactory.getSun()
sun_pv = ExtendedPVCoordinatesProvider.cast_(sun)


# In[10]:


LATITUDE_NEUBIBERG = np.deg2rad(48.0747)
LONGITUDE_NEUBIBERG = np.deg2rad(11.6715)
ALTITUDE_NEUBIBERG = 550.0
station_neubiberg = GeodeticPoint(float(LATITUDE_NEUBIBERG), float(LONGITUDE_NEUBIBERG), ALTITUDE_NEUBIBERG)
station_neubiberg_frame = TopocentricFrame(earth_shape, station_neubiberg, "Neubiberg")


# In[11]:


r_neubiberg = earth_shape.transform(station_neubiberg).getNorm()


# In[12]:


altitude_perigee = 500e3


# In[13]:


r_p = r_neubiberg + altitude_perigee
r_p


# In[14]:


SENSOR_GROUND_RANGE = 250e3 # EPDv1 FOV


# In[15]:


k_revs2rep = np.floor(2*np.pi*earth_shape.getEquatorialRadius()/(10.5*SENSOR_GROUND_RANGE))
k_revs2rep


# In[16]:


k_days2rep = 1.0


# In[17]:


revs_per_day = k_revs2rep/k_days2rep
revs_per_day


# In[18]:


P0 = SIDEREAL_DAY_SECONDS/revs_per_day
P0/60


# In[19]:


n0 = 2*np.pi/P0
n0


# In[20]:


a0 = (earth.getGM()*(1/n0)**2)**(1.0/3.0)


# In[21]:


e0 = (a0 - r_p)/a0
e0


# In[22]:


inclination = LATITUDE_NEUBIBERG


# In[23]:


tolerance = 1e3


# In[24]:


J2 = 1082e-6


# In[25]:


j2k = 1.5*J2*earth_shape.getEquatorialRadius()**2


# In[26]:


raandot = -j2k*n0*(1/a0*(1-e0**2))**2*np.cos(inclination)
np.rad2deg(raandot)


# In[27]:


n = revs_per_day*(wE -  raandot)*(1 - (j2k/a0**2)*(3 - 4*np.sin(inclination)**2))
n


# In[28]:


a = (earth.getGM()*(1/n)**2)**(1.0/3.0)


# In[29]:


e = (a - r_p)/a
e


# In[30]:


anew = a


# In[31]:


a = a0


# In[32]:


while abs(anew - a) > tolerance:
    a = anew
    raandot = -j2k*n*(1/a*(1-e**2))**2*np.cos(inclination)
    n = revs_per_day*(wE -  raandot)*(1 - (j2k/a**2)*(3 - 4*np.sin(inclination)**2))
    anew = (earth.getGM()*(1/n)**2)**(1.0/3.0)
    e = (a - r_p)/a


# In[33]:


a


# In[34]:


e


# In[35]:


np.rad2deg(raandot)*SIDEREAL_DAY_SECONDS


# In[36]:


earth_shape.getEquatorialRadius()


# In[37]:


def repeating_orbit(k_revs2rep, k_days2rep, inclination, **kwargs):
    rp = kwargs["rp"] if "rp" in kwargs else None
    e = kwargs["e"] if "e" in kwargs else None
    e0 = e
    if rp is None and e is None:
        raise Exception("At least one of perigee height or eccentricity is required.")
    RE = 6378.137e3
    SIDEREAL_DAY_SECONDS =  86164.0905
    wE = 2*np.pi/SIDEREAL_DAY_SECONDS
    J2 = 1082e-6
    MUE = 3.98600442e14
    revs_per_day = k_revs2rep/k_days2rep
    P0 = SIDEREAL_DAY_SECONDS/revs_per_day
    n0 = 2*np.pi/P0
    a0 = (MUE*(1/n0)**2)**(1.0/3.0)

    if rp is None:
        rp = a0*(1 - e)
    if e0 is None:
        e0 = (a0 - rp)/a0
    # e0 = (a0 - rp)/a0
    p = a0*(1 - e0**2)
    j2k = 1.5*J2*(RE/p)**2

    raandot = -j2k*n0*np.cos(inclination)
    peridot = 0.5*j2k*n0*(4 - 5*np.sin(inclination)**2)
    m0dot = 0.5*j2k*np.sqrt(1 - e0**2)*(2 - 3*np.sin(inclination)**2)
    n = revs_per_day*(wE - raandot) - (m0dot + peridot)

    # n = revs_per_day*(wE -  raandot)*(1 - (j2k/a0**2)*(3 - 4*np.sin(inclination)**2))
    a = (MUE*(1/n)**2)**(1.0/3.0)
    e = (a - rp)/a
    tolerance = 1e3

    max_iterations = 30
    iteration = 0
    while abs(a - a0) > tolerance and iteration < max_iterations:
        a0 = a
        n0 = n
        e0 = e
        p = a0*(1 - e0**2)
        j2k = 1.5*J2*(RE/p)**2
        print(f"iteration number: {iteration} | \t sma: {a0/1000} km | \t ecc: {e0} \n")
        raandot = -j2k*n0*np.cos(inclination)
        peridot = 0.5*j2k*n0*(4 - 5*np.sin(inclination)**2)
        m0dot = 0.5*j2k*np.sqrt(1 - e0**2)*(2 - 3*np.sin(inclination)**2)
        n = revs_per_day*(wE - raandot) - (m0dot + peridot)
        # raandot = -j2k*n0*(1/p)**2*np.cos(inclination)
        # n = revs_per_day*(wE -  raandot)*(1 - (j2k/a0**2)*(3 - 4*np.sin(inclination)**2))
        a = (MUE*(1/n)**2)**(1.0/3.0)
        e = (a - rp)/a
        iteration += 1
        

    return a0, e0


# In[38]:


revs2repeat = np.arange(10, 200, 1)
days2repeat = np.arange(0.5, 20, 0.5)
sma_all = np.zeros((revs2repeat.shape[0], days2repeat.shape[0]))
ecc_all = np.zeros((revs2repeat.shape[0], days2repeat.shape[0]))

rp = 500e3 + 6378.137e3
valid_combinations = []
inclination = LATITUDE_NEUBIBERG
for i in range(revs2repeat.shape[0]):
    for j in range(days2repeat.shape[0]):
        r = revs2repeat[i]
        n = days2repeat[j]
        print(f"combination: {(i, j)}--> revs to repeat: {r} | days to repeat: {n} \n")
        print("-"*100)
        sma, ecc = repeating_orbit(r, n, inclination, e=0)
        sma_all[i, j] = sma
        ecc_all[i, j] = ecc
        if  rp <= sma*(1+ecc) <= 6378.137e3 + 2000e3  and ecc <=0.07:
            valid_combinations.append((r, n, sma, ecc))


# In[39]:


for r, n, sma, ecc in valid_combinations:
    print(r, n, sma, ecc)


# In[45]:


fig_rgt=plt.figure("RGT Orbit")

ax_rgt_sma=fig_rgt.add_subplot(211)
c_rgt_sma = ax_rgt_sma.contourf(revs2repeat, days2repeat, sma_all.T/1000, levels=np.arange(6878, 8378, 100))
fig_rgt.colorbar(c_rgt_sma, ax=ax_rgt_sma)
ax_rgt_sma.set_xticks(np.arange(10, 200, 10))
ax_rgt_sma.set_yticks(np.arange(0, 26, 2))
ax_rgt_sma.set_xlabel("Revs to repeat")
ax_rgt_sma.set_ylabel("Days to repeat")
ax_rgt_sma.set_title("Semi-Major Axis (km)")


ax_rgt_ecc=fig_rgt.add_subplot(212)
c_rgt_ecc = ax_rgt_ecc.contourf(revs2repeat, days2repeat, ecc_all.T, levels=np.arange(0, 0.08, 0.005))
fig_rgt.colorbar(c_rgt_ecc, ax=ax_rgt_ecc)
ax_rgt_ecc.set_xticks(np.arange(10, 200, 10))
ax_rgt_ecc.set_yticks(np.arange(0, 26, 2))
ax_rgt_ecc.set_xlabel("Revs to repeat")
ax_rgt_ecc.set_ylabel("Days to repeat")
ax_rgt_ecc.set_title("Eccentricity")
plt.tight_layout()
plt.show()


# In[44]:


sma, ecc = repeating_orbit(40, 10.5, inclination, e=0)
print(((sma - 6378e3)/1000, ecc))

