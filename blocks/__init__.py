"""This module implements Blocks that can act like nodes holding some data, 
which can be transferred via connections between ports. 
"""

from ports import Port, InputPort, OutputPort
import logging
import abc
import uuid

logger = logging.getLogger(__name__)


class IBlock(metaclass=abc.ABCMeta):
    """This is the Block interface that provides methods to be implemented.
    This interface must be implemented by each block.
    """
    @abc.abstractmethod
    def connect(self, output_port: OutputPort, input_port: InputPort) -> None:
        """Connects the output port of this block to the input port of another block.
        Parameters
        ----------
            output_port: OutputPort
                The output port of the current block that is to be connected.
            input_port: OutputPort
                The input port of the another block that the output port is to be connected to.
        Returns
        -------
            None
        """
        pass

    @abc.abstractmethod
    def disconnect(self, port1, port2):
        pass

    @abc.abstractmethod
    def emit(self):
        pass

    @abc.abstractmethod
    def on_property_change(self):
        pass


class Block(IBlock):
    """This class is the base class from which other Blocks can be derived.
    The major functionalities are implemented here.
    """
    def __init__(self, name, *args, **kwargs) -> None:
        super().__init__()
        self.name = name
        self.input_ports = []
        self.output_ports = []
        self.connections = {"outgoing": [], "incoming": []}

    def connect(self, output_port, input_port):
        assert isinstance(output_port, OutputPort)
        assert isinstance(input_port, InputPort)

        if output_port not in self.output_ports:
            raise Exception(f"This port does not belong to Block: {self.name}"
                            "Enter a valid output port.")
        logger.info(
            f"connecting output port:"
            f"{output_port.name} of Block {self.name}"
            f"to {input_port.name} of Block {input_port.parent_block.name}")
        self.register_outgoing_connection((output_port, input_port))
        input_port.parent_block.register_incoming_connection(
            (input_port, output_port))
        output_port.set_connection(input_port)

    def set_name(self, name):
        self.name = name
        self.on_property_change()

    def emit(self):
        logger.info("Emitting data")
        for oport, iport in self.connections.get("outgoing"):
            oport.send()

    def on_property_change(self):
        self.emit()

    def register_outgoing_connection(self, out_in_pair):
        self.connections["outgoing"].append(out_in_pair)

    def register_incoming_connection(self, in_out_pair):
        self.connections["incoming"].append(in_out_pair)

    def disconnect(self, port1, port2):
        logger.info("disconnecting ports")

    def add_input_port(self, port):
        self.input_ports.append(port)
        port.set_parent_block(self)

    def add_output_port(self, port):
        self.output_ports.append(port)
        port.set_parent_block(self)

    def on_receive(self, port, data):
        print(f"received data on port: {port.name}")
        setattr(self, port.parent_property, data)