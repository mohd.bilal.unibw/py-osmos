import functools
import logging

logger = logging.getLogger(__name__)


def initialization_required(func):
    """decorator to check if the state functional model has been initialized
    it accesses the object from the self parameter passed to instance methods
    if this is not available, the decorator doesnt work, meaning it shall be
    applied to instance methods and not class methods or static methods. Also
    normal functions cannot be decorated clearly, since there is no self.

    Parameters
    ----------
    func:   function \n
            the function to be decorated
    
    Returns
    -------
    the decorator
    """
    @functools.wraps(func)
    def wrapper_decorator(*args, **kwargs):
        object_instance = args[0]
        if not hasattr(object_instance,
                       'is_initialized') or not object_instance.is_initialized:
            try:
                logger.debug('object not initialized, doing so now..')
                object_instance.initialize()
            except:
                logger.debug('failed to initialize instance...')
                raise NotImplementedError(
                    'the state model could not be initialized, implement the initialize method'
                )
        return func(*args, **kwargs)

    return wrapper_decorator
