import orekit
from org.orekit.utils import ExtendedPVCoordinatesProvider
from org.orekit.orbits import OrbitType, KeplerianOrbit, EquinoctialOrbit, PositionAngle
from org.orekit.propagation.semianalytical.dsst import DSSTPropagator
from org.orekit.propagation import PropagationType as OrekitPropagationType
from org.orekit.propagation.semianalytical.dsst.forces import DSSTZonal
from org.orekit.propagation import SpacecraftState
from org.hipparchus.ode.nonstiff import DormandPrince853Integrator
from org.orekit.forces.gravity.potential import GravityFieldFactory
from utils.astro.thin_orekit_wrapper.propagation import Propagator
from utils.astro.thin_orekit_wrapper.propagation import PropagatorTypes, PropagationType

from orekit.pyhelpers import absolutedate_to_datetime, datetime_to_absolutedate

from utils.astro.thin_orekit_wrapper.space_objects import Spacecraft

from orekit import JArray_double

from orekit.pyhelpers import datetime_to_absolutedate

from utils.astro.helpers import UTC2JD, time_period_to_sma, sma_to_time_period
from utils.astro.helpers import raan_rate_j2, perigee_rate_j2, mean_anomaly_rate_j2
from utils.astro.constants import J2_EARTH as J2
from utils.astro.constants import J3_EARTH as J3
from utils.astro.constants import SIDEREAL_DAY
from utils.astro.thin_orekit_wrapper.globals import SUN, EME2000
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.helpers import UTC2JD, JDC2GMST, get_julian_centuries

import numpy as np
import datetime
import enum
import logging
import numpy as np
import matplotlib.pyplot as plt
from tzwhere import tzwhere
import pytz

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class SpecialOrbitTypes(enum.Enum):
    SSO = 0
    RGT = 1
    SSO_RGT = 2
    FROZEN = 3
    SSO_RGT_FROZEN = 4


class SpecialOrbit:
    def __init__(self, body=None, **kwargs):
        self.body = body
        # set up body
        if self.body is None:
            self.body = Earth()
        self.sma = kwargs.get("sma")
        self.eccentricity = kwargs.get("eccentricity")
        self.inclination = kwargs.get("inclination")
        self.raan = kwargs.get("raan") or 0.0
        self.argument_perigee = kwargs.get("perigee") or 0.0
        self.mean_anomaly = kwargs.get("mean_anomaly") or 0.0
        self.latus_rectum = 0.0
        self.keplerian_period = None
        self.mean_motion = 0.0
        self.epoch = kwargs.get("epoch") or datetime.datetime.utcnow()

    def get_sma(self):
        return self.sma

    def get_eccentricity(self):
        return self.eccentricity

    def get_inclination(self):
        return self.inclination

    def get_raan(self):
        return self.raan

    def get_argument_perigee(self):
        return self.argument_perigee

    def get_mean_anomaly(self):
        return self.mean_anomaly

    def get_keplerian_elements(self):
        return dict(sma=self.sma,
                    eccentricity=self.eccentricity,
                    inclination=self.inclination,
                    raan=self.raan,
                    perigee=self.argument_perigee,
                    mean_anomaly=self.mean_anomaly,
                    epoch=self.epoch)

    def get_epoch(self):
        return self.epoch

    def get_apogee_height(self):
        return self.sma * (
            1 + self.eccentricity) - self.body.get_equatorial_radius()

    def get_perigee_height(self):
        return self.perigee_height


class SSO(SpecialOrbit):
    def __init__(self, body=None, **kwargs):
        super().__init__(body=body, **kwargs)

        # set up parameters
        self.epoch = kwargs.get("epoch") or datetime.datetime.utcnow()
        self.hrs_from_noon = kwargs.get("hrs_from_noon") or 0.0
        self.frozen = kwargs.get("frozen") or False
        self.constraints = kwargs.get("constraints")
        self.orbits_per_day = kwargs.get("orbits_per_day")
        self.__initialize()

    def __initialize(self):
        self.compute_aei_j2()
        if self.constraints is not None and self.constraints.get(
                "subsatellite") is not None:
            self.apply_subsatellite_constraint()
        self.compute_raan_j2()

    def compute_aei_j2(self):
        # constants
        SSO_EARTH_RAANDOT = 1.991063853e-7
        R = self.body.get_equatorial_radius()
        mu = self.body.get_mu()
        k = -3 * R**2 * J2 * np.sqrt(mu) / (2 * SSO_EARTH_RAANDOT)

        if self.orbits_per_day is not None:
            self.keplerian_period = 86400.0 / self.orbits_per_day
            self.sma = time_period_to_sma(self.keplerian_period,
                                          self.body.get_mu())
        if self.eccentricity is not None and self.inclination is not None:
            self.sma = np.power(
                k * np.cos(self.inclination) / ((1 - self.eccentricity**2)**2),
                2.0 / 7.0)
        elif self.eccentricity is not None and self.sma is not None:
            self.inclination = np.arccos(
                np.power(self.sma, 7.0 / 2.0) * (1 - self.eccentricity**2)**2 /
                k)
        elif self.sma is not None and self.inclination is not None:
            self.eccentricity = np.sqrt(1 -
                                        np.sqrt(k * np.cos(self.inclination) /
                                                self.sma**(7.0 / 2.0)))
        else:
            raise ValueError(
                "Insufficient arguments to compute sso. Provide either of (a,i), (a,e), (e,i)."
            )

    def compute_raan_j2(self):
        eme = EME2000
        sun = SUN
        sun_pv = ExtendedPVCoordinatesProvider.cast_(sun)
        hr = int(12 + self.hrs_from_noon)
        mins = int(60 * self.hrs_from_noon % 60)
        # raan_epoch = datetime.datetime(self.epoch.year, self.epoch.month,
        #                                 self.epoch.day, hr, mins, 0, 0)
        sun_coordinates = sun_pv.getPVCoordinates(
            datetime_to_absolutedate(self.epoch), eme)
        sun_position = np.array([
            sun_coordinates.getPosition().getX(),
            sun_coordinates.getPosition().getY(),
            sun_coordinates.getPosition().getZ()
        ])
        theta_sun = np.arctan2(sun_position[1], sun_position[0])
        self.raan = theta_sun + self.hrs_from_noon * 2 * np.pi / 24.0
        if self.raan < 0:
            self.raan = 2 * np.pi + self.raan
        logger.debug(self.raan)
        logger.debug(theta_sun)

    def apply_subsatellite_constraint(self):
        if self.keplerian_period is None:
            self.keplerian_period = sma_to_time_period(self.sma,
                                                       self.body.get_mu())
        latitude = self.constraints.get("subsatellite").get("latitude")
        longitude = self.constraints.get("subsatellite").get("longitude")
        direction = self.constraints.get("subsatellite").get("direction")

        theta = np.pi - self.inclination if self.inclination > np.pi / 2 else self.inclination
        arc_location = np.arcsin(np.sin(latitude) / np.sin(self.inclination))
        delta_lat = np.arccos(np.cos(arc_location) / np.cos(latitude))
        delta_time = delta_lat * 86400 / (2 * np.pi)
        epoch_local = None
        if direction == "descending":
            hr = int(12 + np.abs(self.hrs_from_noon))
            epoch_local = datetime.datetime(self.epoch.year, self.epoch.month,
                                            self.epoch.day, hr, 0, 0, 0)

            epoch_local = epoch_local + datetime.timedelta(seconds=delta_time)
            self.argument_perigee = np.pi - arc_location
        elif direction == "ascending":
            hr = int(12 + self.hrs_from_noon)
            epoch_local = datetime.datetime(self.epoch.year, self.epoch.month,
                                            self.epoch.day, hr, 0, 0, 0)
            epoch_local = epoch_local - datetime.timedelta(seconds=delta_time)
            self.argument_perigee = arc_location
        else:
            raise ValueError(
                "Wrong direction. Only ascending and descending directions are possible."
            )
        self.epoch = epoch_local - datetime.timedelta(
            seconds=86400 * longitude /
            (2 * np.pi)) - datetime.timedelta(seconds=3600)


class RepeatingGroundTrackOrbit(SpecialOrbit):
    def __init__(self, inclination, body=None, **kwargs):
        super().__init__(body=body, **kwargs)

        # initialize parameters for rgto
        self.inclination = inclination
        self.groundtrack_shift = kwargs.get("lambda_rev")
        self.k_revs2rep = kwargs.get("k_revs2rep")
        self.k_days2rep = kwargs.get("k_days2rep")
        self.perigee_height = kwargs.get("perigee_height")
        self.r_perigee = None

        self.revs_per_day = None
        self.computation_method = None

        self.constraints = kwargs.get("constraints")
        self.frozen = kwargs.get("frozen") or False

        self.tolerance = kwargs.get("tolerance") or 1e2
        self.max_iterations = kwargs.get("max_iterations") or 30

        self.__initialize()

    def __initialize(self):
        if self.groundtrack_shift is None and self.k_revs2rep is None and self.k_days2rep is None:
            raise Exception(
                "At least two of lambda_rev, k_revs2rep, and k_days2rep are needed"
            )
        elif self.groundtrack_shift is not None and self.k_revs2rep is not None and self.k_days2rep is None:
            self.k_days2rep = self.groundtrack_shift * self.k_revs2rep / (
                2 * np.pi)
            self.computation_method = "groundtrack_shift"
        elif self.groundtrack_shift is not None and self.k_revs2rep is None and self.k_days2rep is not None:
            self.k_revs2rep = 2 * np.pi * self.k_days2rep / (
                self.groundtrack_shift)
            self.computation_method = "groundtrack_shift"
        elif self.groundtrack_shift is None and self.k_revs2rep is not None and self.k_days2rep is not None:
            self.groundtrack_shift = 2 * np.pi * self.k_days2rep / self.k_revs2rep
            self.computation_method = "kr_kd"
        else:
            assert self.groundtrack_shift == 2 * np.pi * self.k_days2rep / self.k_revs2rep

        self.revs_per_day = self.k_revs2rep / self.k_days2rep
        self.keplerian_period = SIDEREAL_DAY / self.revs_per_day
        self.mean_motion = 2 * np.pi / self.keplerian_period
        self.sma = time_period_to_sma(self.keplerian_period,
                                      self.body.get_mu())

        if self.perigee_height is None and self.eccentricity is not None:
            self.r_perigee = self.sma * (1 - self.eccentricity)
        elif self.perigee_height is not None and self.eccentricity is None:
            self.r_perigee = self.body.get_equatorial_radius(
            ) + self.perigee_height
            self.eccentricity = (self.sma - self.r_perigee) / self.sma
        elif self.perigee_height is not None and self.eccentricity is not None:
            self.r_perigee = self.sma * (1 - self.eccentricity)
        else:
            raise Exception(
                "Provide either eccentricity or perigee height (in meters).")

        self.latus_rectum = self.sma * (1 - self.eccentricity**2)

        if self.computation_method == "groundtrack_shift":
            self.compute_with_groundtrack_shift()
        elif self.computation_method == "kr_kd":
            self.compute_with_kr_kd()
        else:
            self.compute_with_kr_kd()
        if self.constraints is not None and self.constraints.get(
                "subsatellite") is not None:
            self.apply_subsatellite_constraint()

    def get_revolutions_to_repeat(self):
        return self.k_revs2rep

    def get_days_to_repeat(self):
        return self.k_days2rep

    def apply_subsatellite_constraint(self):
        logger.info("applying subsatellite constraint")
        latitude = self.constraints.get("subsatellite").get("latitude")
        longitude = self.constraints.get("subsatellite").get("longitude")
        direction = self.constraints.get("subsatellite").get("direction")

        arc_location = np.arcsin(np.sin(latitude) / np.sin(self.inclination))
        long_raan = longitude - np.arccos(
            np.cos(arc_location) / np.cos(latitude))
        jd = UTC2JD(self.epoch)
        theta_gmst = JDC2GMST(get_julian_centuries(jd))

        self.raan = theta_gmst + long_raan
        self.argument_perigee = arc_location
        self.mean_anomaly = 0.0

    def compute_with_kr_kd(self):
        raandot = raan_rate_j2(self.sma, self.eccentricity, self.inclination,
                               self.body.get_equatorial_radius(),
                               self.body.get_mu(), J2)
        peridot = perigee_rate_j2(self.sma, self.eccentricity,
                                  self.inclination,
                                  self.body.get_equatorial_radius(),
                                  self.body.get_mu(), J2)
        m0dot = mean_anomaly_rate_j2(self.sma, self.eccentricity,
                                     self.inclination,
                                     self.body.get_equatorial_radius(),
                                     self.body.get_mu(), J2)

        n = self.revs_per_day * (self.body.get_rotation_rate() - raandot) - (
            m0dot + peridot)
        P = 2 * np.pi / n
        a = time_period_to_sma(P, self.body.get_mu())
        e = (a - self.r_perigee) / a

        iteration = 0
        while abs(a - self.sma
                  ) > self.tolerance and iteration < self.max_iterations:
            self.sma = a
            self.mean_motion = n
            self.eccentricity = e
            raandot = raan_rate_j2(self.sma, self.eccentricity,
                                   self.inclination,
                                   self.body.get_equatorial_radius(),
                                   self.body.get_mu(), J2)
            peridot = perigee_rate_j2(self.sma, self.eccentricity,
                                      self.inclination,
                                      self.body.get_equatorial_radius(),
                                      self.body.get_mu(), J2)
            m0dot = mean_anomaly_rate_j2(self.sma, self.eccentricity,
                                         self.inclination,
                                         self.body.get_equatorial_radius(),
                                         self.body.get_mu(), J2)
            n = self.revs_per_day * (self.body.get_rotation_rate() -
                                     raandot) - (m0dot + peridot)
            P = 2 * np.pi / n
            a = time_period_to_sma(P, self.body.get_mu())
            e = (a - self.r_perigee) / a
            if e < 0 or e > 1:
                raise Exception(
                    "Eccentricity greater than 1 or less than zero. This input value has no suitable solution. Please check inputs."
                )
            iteration += 1

    def compute_with_groundtrack_shift(self):
        raandot = raan_rate_j2(self.sma, self.eccentricity, self.inclination,
                               self.body.get_equatorial_radius(),
                               self.body.get_mu(), J2)
        delta_lambda_p = 2 * np.pi * raandot / self.mean_motion
        delta_long = delta_lambda_p + self.groundtrack_shift
        n = 2 * np.pi * self.body.get_rotation_rate() / delta_long
        a = time_period_to_sma(2 * np.pi / n, self.body.get_mu())
        e = (a - self.r_perigee) / a
        iteration = 0

        while abs(a - self.sma
                  ) > self.tolerance and iteration < self.max_iterations:
            self.sma = a
            self.mean_motion = n
            self.eccentricity = e
            raandot = raan_rate_j2(self.sma, self.eccentricity,
                                   self.inclination,
                                   self.body.get_equatorial_radius(),
                                   self.body.get_mu(), J2)
            delta_lambda_p = 2 * np.pi * raandot / self.mean_motion
            delta_long = delta_lambda_p + self.groundtrack_shift
            n = 2 * np.pi * self.body.get_rotation_rate() / delta_long
            a = time_period_to_sma(2 * np.pi / n, self.body.get_mu())
            e = (a - self.r_perigee) / a
            iteration += 1

    @staticmethod
    def get_minimum_eccentricity_rgto(inclination,
                                      perigee_height,
                                      body=None,
                                      **kwargs):
        MAX_LEO_HEIGHT = 2000e3
        if body is None:
            body = Earth()
        revs2repeat = np.arange(10, 200, 1)
        days2repeat = np.arange(0.5, 20, 0.5)
        sma_all = np.zeros((revs2repeat.shape[0], days2repeat.shape[0]))
        ecc_all = np.zeros((revs2repeat.shape[0], days2repeat.shape[0]))
        r_perigee = body.get_equatorial_radius() + perigee_height
        valid_rgts = []
        for i in range(revs2repeat.shape[0]):
            for j in range(days2repeat.shape[0]):
                r = revs2repeat[i]
                n = days2repeat[j]
                try:
                    rgt = RepeatingGroundTrackOrbit(inclination,
                                                    body=body,
                                                    k_revs2rep=r,
                                                    k_days2rep=n,
                                                    perigee_height=500e3,
                                                    **kwargs)
                except:
                    rgt = None
                if rgt is not None and r_perigee <= rgt.get_sma(
                ) * (1 + rgt.get_eccentricity()) <= body.get_equatorial_radius(
                ) + MAX_LEO_HEIGHT and rgt.get_eccentricity() <= 0.08:
                    valid_rgts.append(rgt)
        min_ecc_orbit = min(valid_rgts, key=lambda rgt: rgt.eccentricity)
        return min_ecc_orbit


class RGTSSOrbit(SpecialOrbit):
    def __init__(self, body=None, **kwargs):
        super().__init__(body, **kwargs)
        # self.inclination = inclination
        self.groundtrack_shift = kwargs.get("lambda_rev")
        self.k_revs2rep = kwargs.get("k_revs2rep")
        self.k_days2rep = kwargs.get("k_days2rep")
        self.perigee_height = kwargs.get("perigee_height")
        self.hrs_from_noon = kwargs.get("hrs_from_noon") or 0.0

        self.__initialize(**kwargs)

    def __initialize(self, **kwargs):
        if self.eccentricity is None:
            self.eccentricity = 0.0
        if self.sma is None:
            self.sma = (self.body.get_equatorial_radius() +
                        self.perigee_height) / (1 - self.eccentricity)
        kwargs_rgt = kwargs.copy()
        kwargs_sso = kwargs.copy()
        try:
            kwargs_sso.pop("eccentricity")
            kwargs_rgt.pop("perigee_height")
            kwargs_rgt.pop("eccentricity")
            kwargs_sso.pop("sma")
        except KeyError as ke:
            logger.info("Key perigee_height was not found.")
        sso = SSO(self.body,
                  sma=self.sma,
                  eccentricity=self.eccentricity,
                  **kwargs_sso)
        self.inclination = sso.get_inclination()
        print(f"sso inclination: {np.rad2deg(self.inclination)}")
        print(f"kwargs: {kwargs_rgt}")
        rgt = RepeatingGroundTrackOrbit(self.inclination,
                                        body=self.body,
                                        perigee_height=self.perigee_height,
                                        **kwargs_rgt)

        self.sma = rgt.get_sma()
        self.eccentricity = rgt.get_eccentricity()
        self.perigee_height = self.sma * (
            1 - self.eccentricity) - self.body.get_equatorial_radius()

        sso = SSO(self.body,
                  sma=self.sma,
                  eccentricity=self.eccentricity,
                  **kwargs_sso)
        iteration = 0
        while abs(sso.get_inclination() - self.inclination) > 1e-7:
            self.inclination = sso.get_inclination()

            # use the new inclination to compute a new sma for rgt
            rgt = RepeatingGroundTrackOrbit(self.inclination,
                                            body=self.body,
                                            perigee_height=self.perigee_height,
                                            **kwargs_rgt)
            self.sma = rgt.get_sma()
            self.eccentricity = rgt.get_eccentricity()

            # use the new sma to compute a new inclination
            sso = SSO(self.body,
                      sma=self.sma,
                      eccentricity=self.eccentricity,
                      **kwargs_sso)
            # self.inclination = sso.get_inclination()
            iteration += 1
            print(f"iteration: {iteration}")

        self.raan = sso.get_raan()
        self.argument_perigee = sso.get_argument_perigee()
        self.mean_anomaly = sso.get_mean_anomaly()

    @staticmethod
    def get_minimum_eccentricity_rgtsso(perigee_height,
                                        hrs_from_noon,
                                        body=None,
                                        **kwargs):
        if body is None:
            body = Earth()
        max_iterations = kwargs.get("max_iterations") or 30
        inclination_tolerance = kwargs.get("inclination_tolerance") or np.deg2rad(1e-3)
        eccentricity = 0.0
        sma = (body.get_equatorial_radius() + perigee_height) / (1 -
                                                                 eccentricity)
        sso = SSO(body=body,
                  sma=sma,
                  eccentricity=eccentricity,
                  hrs_from_noon=hrs_from_noon,
                  **kwargs)
        inclination = sso.get_inclination()
        rgt = RepeatingGroundTrackOrbit.get_minimum_eccentricity_rgto(
            inclination, perigee_height)
        sma = rgt.get_sma()
        eccentricity = rgt.get_eccentricity()
        print(f"iteration 0: {np.rad2deg(inclination)}")
        sso = SSO(body=body,
                  sma=sma,
                  eccentricity=eccentricity,
                  hrs_from_noon=hrs_from_noon,
                  **kwargs)
        iteration = 0
        while abs(sso.get_inclination() - inclination
                  ) > inclination_tolerance and iteration <= max_iterations:
            inclination = sso.get_inclination()
            # use the new inclination to compute a new sma for rgt
            rgt = RepeatingGroundTrackOrbit.get_minimum_eccentricity_rgto(
                inclination, perigee_height)
            sma = rgt.get_sma()
            eccentricity = rgt.get_eccentricity()
            perigee_height = sma * (
                1 - eccentricity) - body.get_equatorial_radius()

            # use the new sma to compute a new inclination
            sso = SSO(body=body,
                      sma=sma,
                      eccentricity=eccentricity,
                      hrs_from_noon=hrs_from_noon,
                      **kwargs)
            iteration += 1
            print(f"iteration: {iteration}")
            print(f"inclination: {np.rad2deg(inclination)}")

        raan = sso.get_raan()
        argument_perigee = sso.get_argument_perigee()
        mean_anomaly = sso.get_mean_anomaly()
        kwargs.update({
            'raan': raan,
            'perigee': argument_perigee,
            'mean_anomaly': mean_anomaly
        })

        return RepeatingGroundTrackOrbit(
            inclination,
            body=body,
            k_revs2rep=rgt.get_revolutions_to_repeat(),
            k_days2rep=rgt.get_days_to_repeat(),
            perigee_height=perigee_height,
            **kwargs)

class FrozenEccentricity(SpecialOrbit):
    def __init__(self, sma, inclination, body=None, **kwargs):
        super().__init__(body=body, **kwargs)
        
        # initialize parameters for frozen orbit
        self.sma = sma
        self.inclination = inclination
        self.computation_method = kwargs.get("computation_method") or "NUMERICAL"
        self.geopotential_degree = kwargs.get("geopotential_degree") or 51
        self.geopotential_order = kwargs.get("geopotential_order") or 51

        # extract constraints
        self.constraints = kwargs.get("constraints")
        self.__initialize()

    def __initialize(self):
        self.argument_perigee = np.deg2rad(90)
        self._compute_ecc_j2_j3()
        if self.computation_method.upper() == "NUMERICAL":
            self._update_eccentricity_numerical()
    def _compute_ecc_j2_j3(self):
        self.eccentricity = -0.5 * J3 / J2 * (self.body.get_equatorial_radius() / self.sma) * np.sin(self.inclination) * np.sin(self.argument_perigee)
    
    def _update_eccentricity_numerical(self):
        forces_options = {
            "gravity": {
                "degree": self.geopotential_degree,
                "order": self.geopotential_order,
            }
        }
        start_time = self.epoch
        duration = 200 * 24 * 60 * 60
        end_time = start_time + datetime.timedelta(seconds = duration)
        step_size = 24.0 * 60 * 60
        propagator_options = dict(propagator_type=PropagatorTypes.DSST.name, propagation_type=PropagationType.MEAN.name, initial_condition_type=PropagationType.MEAN.name, step_size = step_size, max_step=step_size, min_step=step_size)

        e_all = [1.05*self.eccentricity, 0.99*self.eccentricity, 1.1*self.eccentricity, 1.01*self.eccentricity, 1.03*self.eccentricity]
        computed_eccs = []
        for e in e_all:
            sc = Spacecraft("SeRANIS", propagator_options=propagator_options)
            sc.set_central_body(self.body)
            sc.add_environment_forces(forces_options=forces_options)
            state_epoch = {
                "sma": self.sma,
                "inclination": self.inclination,
                "eccentricity": e,
                "raan": self.raan,
                "perigee": self.argument_perigee,
                "mean_anomaly": self.mean_anomaly 
            }
            sc.set_initial_state(self.epoch, state_epoch)

            sc.propagate(start_time, end_time)
            kep_propagated = sc.get_propagated_keplerian_elements()
            e_propagated = np.array(kep_propagated.get("eccentricity"))
            perigee_propagated = np.array(kep_propagated.get("perigee"))
            ex = np.multiply(e_propagated, np.cos(perigee_propagated))
            ey = np.multiply(e_propagated, np.sin(perigee_propagated))
            ecc = np.linalg.norm([np.mean(ex), np.mean(ey)])
            computed_eccs.append(ecc)
        self.eccentricity = np.mean(computed_eccs)


def frozen(body=None, **kwargs):
    """Computes keplerian elements of frozen orbit.
    All units are SI.
    """
    if body is None:
        body = Earth()
    a = kwargs["a"] if "a" in kwargs else None
    i = kwargs["i"] if "i" in kwargs else np.deg2rad(63.44)
    w = kwargs["w"] if "w" in kwargs else np.deg2rad(90)
    raan = kwargs["raan"] if "raan" in kwargs else None

    # constants
    R = body.get_equatorial_radius()
    J2 = 0.108265e-2
    J3 = -0.254503e-5
    mu = body.get_mu()

    time = kwargs["time"] if "time" in kwargs else datetime.datetime.utcnow()

    if a is not None:
        e = -0.5 * J3 / J2 * (R / a) * np.sin(i) * np.sin(w)
    else:
        raise ValueError(
            "Insufficient arguments to compute frozen orbit. Provide a.")

    print('J2/J3 frozen orbit theory: e=', e, ', i=', np.rad2deg(i), ', w=',
          np.rad2deg(w))
    e = 1.5 * e
    t_epoch = datetime.datetime.utcnow()
    state_epoch = {
        "sma": a,
        "inclination": i,
        "eccentricity": e,
        "raan": 0,
        "perigee": w,
        "mean_anomaly": 0
    }
    initial_state = SpacecraftState(
        EquinoctialOrbit(float(a), float(e * np.cos(w + 0)),
                         float(e * np.sin(w + 0)),
                         float(np.tan(i / 2) * np.cos(0)),
                         float(np.tan(i / 2) * np.sin(0)), float(0 + w + 0),
                         PositionAngle.MEAN, EME2000,
                         datetime_to_absolutedate(t_epoch), body.get_mu()),
        float(100.0))
    min_step = float(0.001)
    max_step = float(10.0)
    position_tolerance = float(1.0)
    orbit_type = OrbitType.EQUINOCTIAL
    tolerances = DSSTPropagator.tolerances(position_tolerance, float(1e-3),
                                           initial_state.getOrbit())
    integrator = DormandPrince853Integrator(min_step, max_step,
                                            JArray_double.cast_(tolerances[0]),
                                            JArray_double.cast_(tolerances[1]))

    propagator = DSSTPropagator(integrator, OrekitPropagationType.MEAN)
    # propagator.setOrbitType(orbit_type)
    zonal_force = DSSTZonal(GravityFieldFactory.getUnnormalizedProvider(
        21, 21))
    propagator.addForceModel(zonal_force)
    propagator.setInitialState(initial_state, OrekitPropagationType.MEAN)

    start_time = t_epoch
    step_size = 60.0
    duration = 100 * 24 * 60 * 60
    end_time = start_time + datetime.timedelta(seconds=duration)
    start_time_orekit = datetime_to_absolutedate(start_time)
    time_vec_orekit = [
        start_time_orekit.shiftedBy(float(dt))
        for dt in np.arange(0, duration, step_size)
    ]
    sc_data = [propagator.propagate(t) for t in time_vec_orekit]

    # plots
    e_propagated = []
    w_propagated = []
    raan_propagated = []
    sma_propagated = [s.getA() / 1e3 for s in sc_data]
    i_propagated = [s.getI() for s in sc_data]
    for state in sc_data:
        keplerian = KeplerianOrbit.cast_(
            OrbitType.KEPLERIAN.convertType(state.getOrbit()))
        e_propagated.append(keplerian.getE())
        w_propagated.append(keplerian.getPerigeeArgument())
        raan_propagated.append(keplerian.getRightAscensionOfAscendingNode())

    eq_ex = [s.getEquinoctialEx() for s in sc_data]
    eq_ey = [s.getEquinoctialEy() for s in sc_data]
    hx = [s.getHx() for s in sc_data]
    hy = [s.getHy() for s in sc_data]

    sum_w_raan = np.arctan2(eq_ey, eq_ex)
    raan = np.arctan2(hy, hx)

    w_propagated_equinoctial = sum_w_raan - raan

    e_propagated_equinoctial = np.divide(np.array(eq_ex),
                                         np.array(np.cos(sum_w_raan)))

    ex = np.multiply(np.array(e_propagated_equinoctial),
                     np.cos(np.array(w_propagated_equinoctial)))
    ey = np.multiply(np.array(e_propagated_equinoctial),
                     np.sin(np.array(w_propagated_equinoctial)))

    logger.info(f"total propagation steps: {len(sma_propagated)}")

    fig, axs = plt.subplots(3, 2)
    axs[0, 0].plot(np.rad2deg(w_propagated_equinoctial))
    axs[0, 0].set_title('w')
    axs[0, 1].plot(e_propagated_equinoctial)
    axs[0, 1].set_title('e')
    axs[1, 0].plot(eq_ex)
    axs[1, 0].set_title('equinoctial_ex')
    axs[1, 1].plot(eq_ey)
    axs[1, 1].set_title('equinoctial_ey')
    axs[2, 0].plot(np.rad2deg(w_propagated_equinoctial),
                   e_propagated_equinoctial)
    axs[2, 0].set_title('w vs e')
    axs[2, 1].plot(ex, ey)
    axs[2, 1].set_title('ex vs ey')

    #plt.plot(e_propagated_equinoctial)
    plt.show()

    return a, e, i, w
