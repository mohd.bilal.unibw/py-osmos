from org.orekit.geometry.fov import CircularFieldOfView as OrekitCircularFieldOfView
from org.orekit.geometry.fov import DoubleDihedraFieldOfView as OrekitDoubleDihedraFieldOfView
from org.orekit.geometry.fov import EllipticalFieldOfView as OrekitEllipticalFieldOfView
from org.orekit.geometry.fov import PolygonalFieldOfView as OrekitPolygonalFieldOfView
from org.orekit.geometry.fov import SmoothFieldOfView as OrekitSmoothFieldOfView
from org.orekit.frames import Transform as OrekitTransform

from org.hipparchus.geometry.euclidean.threed import Vector3D

import numpy as np
import enum
import ast
import logging

logger = logging.getLogger(__name__)


class FOVTypes(enum.Enum):
    CIRCULAR_FOV = 0
    DOUBLE_DIHEDRAL_FOV = 1
    ELLIPTICAL_FOV = 2
    POLYGONAL_FOV = 3
    SMOOTH_FOV = 4


class FOVFactory:
    @staticmethod
    def get_fov_by_type(fov_type, fov_options):
        if fov_type.upper() == FOVTypes.CIRCULAR_FOV.name:
            # get pointing direction in spacecraft body frame
            direction = fov_options["orientation_axis"]
            aperture = fov_options["half_cone_angle"]
            margin = fov_options["margin_angle"]
            base_direction = Vector3D(float(direction[0]), float(direction[1]),
                                      float(direction[2]))
            base_fov = OrekitCircularFieldOfView(base_direction,
                                                 float(aperture),
                                                 float(margin))
            return base_fov
        elif fov_type.upper() == FOVTypes.ELLIPTICAL_FOV.name:
            direction = fov_options["orientation_axis"]
            major_axis = fov_options["ellipse_major_axis"]
            aperture_x = fov_options["half_cone_x"]
            aperture_y = fov_options["half_cone_y"]
            margin = fov_options["margin_angle"]
            base_direction = Vector3D(float(direction[0]), float(direction[1]),
                                      float(direction[2]))
            base_major_axis = Vector3D(float(major_axis[0]),
                                       float(major_axis[1]),
                                       float(major_axis[2]))
            base_fov = OrekitEllipticalFieldOfView(base_direction,
                                                   base_major_axis,
                                                   float(aperture_x),
                                                   float(aperture_y),
                                                   float(margin))
            return base_fov
        else:
            raise NotImplementedError(
                f"{fov_type} has not been implemented yet. Request your patience."
            )


class SpaceSensor:
    def __init__(self, name, fov_type, fov_options, **kwargs):
        self.name = name
        self.fov_type = fov_type
        self.fov_options = fov_options

        # define base fov
        self.base_fov = FOVFactory.get_fov_by_type(fov_type, fov_options)

    def get_name(self):
        return self.name

    def get_fov_options(self):
        return self.fov_options

    def get_base_fov(self):
        return self.base_fov

    def compute_footprint(self, spacecraft, central_body, angular_step):
        t = spacecraft.get_base_current_time()
        inertial_2_central_body = central_body.get_inertial_frame(
        ).getTransformTo(central_body.get_body_frame(), t)
        fov_2_central_body = OrekitTransform(
            t,
            spacecraft.get_base_current_state().toTransform().getInverse(),
            inertial_2_central_body)
        footprint = self.base_fov.getFootprint(
            fov_2_central_body, central_body.get_base_shape(),
            float(angular_step))
        str_geodetic = str(footprint.get(0)).replace(' deg', '')
        str_geodetic = str_geodetic.replace('lat', "\'lat\'")
        str_geodetic = str_geodetic.replace('lon', "\'lon\'")
        str_geodetic = str_geodetic.replace('alt', "\'alt\'")
        footprint_list = ast.literal_eval(str_geodetic)
        fp_radians = [{
            "time": spacecraft.get_current_time(),
            "latitude": np.deg2rad(fp.get("lat")),
            "longitude": np.deg2rad(fp.get("lon"))
        } for fp in footprint_list]

        return fp_radians
