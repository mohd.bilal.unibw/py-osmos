"""`ground_objects` implements the objects on a Celestial Body surface by wrapping the orekit `GeodeticPoint` Class.
A GroundObject also holds an instance of the Topocentric Frame.
"""
from org.orekit.bodies import GeodeticPoint as OrekitGeodeticPoint
from org.orekit.frames import TopocentricFrame as OrekitTopocentricFrame

class GroundObject:
    def __init__(self, name, body, latitude, longitude, altitude, **kwargs):
        """Default constructor for Ground Object.
        Each ground object on a celestial body can be identified by its latitude, longitude, and name

        Parameters
        ----------
        name: str
            name of the object
        body: CelestialBody
            the celestial body on which the ground object lies
        latitude: float
            latitude of the object in radians
        longitude: float
            longitude of the object in radians

        Returns
        -------
        None
        """
        self.body = body
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.base_geodetic_point = None
        self.base_topocentric_frame = None
        self.minimum_elevation = 0.0
        self.is_initialized = False
        self.__initialize()

    def __initialize(self):
        self.base_geodetic_point = OrekitGeodeticPoint(float(self.latitude), float(self.longitude), float(self.altitude))
        self.base_topocentric_frame = OrekitTopocentricFrame(self.body.get_base_shape(),
                                                            self.base_geodetic_point,
                                                            self.name)
        self.is_initialized = True

    def get_body(self):
        return self.body
    
    def get_name(self):
        return self.name

    def get_latitude(self):
        return self.latitude

    def get_longitude(self):
        return self.longitude
    
    def get_altitude(self):
        return self.altitude
    
    def get_topocentric_frame(self):
        return self.base_topocentric_frame
    
    def get_geodetic_point(self):
        return self.base_geodetic_point

    def get_minimum_elevation(self):
        return self.minimum_elevation
    
    def set_body(self, body):
        self.body = body
    
    def set_name(self, name):
        self.name = name
    
    def set_latitude(self, latitude):
        self.latitude = latitude
    
    def set_longitude(self, longitude):
        self.longitude = longitude
    
    def set_altitude(self, altitude):
        self.altitude = altitude


# class GroundStation(GroundObject):
#     def __init__(self, name, body, latitude, longitude, altitude, **kwargs):
#         super().__init__(name, body, latitude, longitude, altitude, **kwargs)
        
