from org.orekit.bodies import CelestialBodyFactory as OrekitCelestialBodyFactory
from org.orekit.frames import FramesFactory as OrekitFramesFactory
from config import OREKIT_DATA
import orekit
from orekit.pyhelpers import setup_orekit_curdir
import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

logger.info("initiating orekit virtual machine...")
VM = orekit.initVM()
logger.info("orekit virtual machine initiated.")
setup_orekit_curdir(OREKIT_DATA)
SUN = OrekitCelestialBodyFactory.getSun()
MOON = OrekitCelestialBodyFactory.getMoon()
EME2000 = OrekitFramesFactory.getEME2000()