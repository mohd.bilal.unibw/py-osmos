from org.orekit.forces import BoxAndSolarArraySpacecraft as OrekitBoxAndSolarArraySpacecraft
from org.orekit.forces.drag import IsotropicDrag as OrekitIsotropicDragSpacecraft
from org.orekit.forces.radiation import IsotropicRadiationClassicalConvention as OrekitIsotropicRadiationClassical
from org.orekit.propagation import SpacecraftState as OrekitSpacecraftState
from org.orekit.orbits import PositionAngle as OrekitPositionAngle
from org.orekit.orbits import EquinoctialOrbit as OrekitEquinoctialOrbit
from org.orekit.orbits import OrbitType as OrekitOrbitType
from org.hipparchus.geometry.euclidean.threed import Vector3D
from org.orekit.forces.drag import DragForce as OrekitDragForce
from org.orekit.propagation.semianalytical.dsst.forces import DSSTAtmosphericDrag as OrekitDSSTAtmosphericDrag
from org.orekit.forces.gravity import ThirdBodyAttraction as OrekitThirdBodyAttraction
from org.orekit.propagation.semianalytical.dsst.forces import DSSTThirdBody as OrekitDSSTThirdBody
from org.orekit.forces.radiation import SolarRadiationPressure as OrekitSolarRadiationPressure
from org.orekit.propagation.semianalytical.dsst.forces import DSSTSolarRadiationPressure as OrekitDSSTSolarRadiationPressure
from org.orekit.orbits import KeplerianOrbit as OrekitKeplerianOrbit
from org.orekit.utils import ExtendedPVCoordinatesProvider as OrekitExtendedPVCoordinatesProvider

from orekit.pyhelpers import absolutedate_to_datetime, datetime_to_absolutedate
from utils.astro.thin_orekit_wrapper.globals import SUN, EME2000, MOON
from utils.astro.constants import AVG_SOLAR_FLUX_EARTH
from utils.astro.thin_orekit_wrapper.propagation import Propagator, PropagatorFactory, PropagatorTypes
from utils.astro.thin_orekit_wrapper.bodies import Earth
from utils.astro.thin_orekit_wrapper.events import EventFactory
from utils.astro.thin_orekit_wrapper.attitudes import AttitudeLawFactory, AttitudeLawType, orekit_attitude_to_quat
from utils.astro.thin_orekit_wrapper.sensors import SpaceSensor

from enum import Enum
from utils.decorators import initialization_required
from utils.attitude.helpers.attitude_parameters import quat2dcm
import numpy as np
import datetime
import logging

logger = logging.getLogger(__name__)

class SpacecraftShapeType(Enum):
    SPHERICAL = 0
    CUBOID = 1
    CUBOID_DEPLOYED_PANELS = 2 # panels are deployed but fixed
    CUBOID_DEPLOYED_ROTATING_PANELS = 3 # panels are deployed and linearly rotating
    CUBOID_DEPLOYED_ROTATING_PANELS_OPTIMAL_LIGHTING = 4 # panels are deployed and rotating for optimal lighting


class SpacecraftShape:
    def __init__(self, type_, shape_data: dict, drag_data: dict, spectral_data: dict):
        self.type_ = type_
        self.base_shape = None
        if self.type_.name.upper() == SpacecraftShapeType.CUBOID_DEPLOYED_PANELS.name:
            self.base_shape = SpacecraftShape.create_cuboid_shape(shape_data, drag_data, spectral_data)
        elif self.type_.name.upper() == SpacecraftShapeType.CUBOID_DEPLOYED_ROTATING_PANELS.name:
            self.base_shape = SpacecraftShape.create_cuboid_shape(shape_data, drag_data, spectral_data, rotating=True)
        elif self.type_.name.upper() == SpacecraftShapeType.CUBOID_DEPLOYED_ROTATING_PANELS_OPTIMAL_LIGHTING.name:
            self.base_shape = SpacecraftShape.create_cuboid_shape(shape_data, drag_data, spectral_data, rotating=True, optimal_lighting=True)
        elif self.type_.name.upper() == SpacecraftShapeType.CUBOID.name:
            self.base_shape = SpacecraftShape.create_cuboid_shape(shape_data, drag_data, spectral_data)
        elif self.type_.name.upper() == SpacecraftShapeType.SPHERICAL.name:
            self.base_shape = SpacecraftShape.create_spherical_shape(shape_data, drag_data, spectral_data)

    def get_base_shape(self):
        return self.base_shape

    @staticmethod
    def create_spherical_shape(shape_data: dict, drag_data: dict):
        radius = shape_data.get("radius")
        cross_section_area = np.pi*radius**2
        shape = OrekitIsotropicDragSpacecraft(float(cross_section_area), float(drag_data.get("drag_coeff")))
        return shape

    @staticmethod
    def create_cuboid_shape(shape_data: dict, drag_data: dict, spectral_data: dict, **kwargs):
        rotating = kwargs.get("rotating")
        optimal_lighting = kwargs.get("optimal_lighting")
        shape = None
        if rotating is not None and optimal_lighting is not None:
            shape = OrekitBoxAndSolarArraySpacecraft(float(shape_data.get("x_length")),
                                                    float(shape_data.get("y_length")),
                                                    float(shape_data.get("z_length")),
                                                    SUN,
                                                    float(shape_data.get("solar_array_area")),
                                                    shape_data.get("solar_array_axis"),
                                                    float(drag_data.get("drag_coeff")),
                                                    float(spectral_data.get("absorption_coeff")),
                                                    float(spectral_data.get("reflection_coeff")))
            return shape
        
        rotation_rate = shape_data.get("solar_array_rotation_rate") if rotating else 0.0
        shape = OrekitBoxAndSolarArraySpacecraft(float(shape_data.get("x_length")),
                                                float(shape_data.get("y_length")),
                                                float(shape_data.get("z_length")),
                                                SUN,
                                                float(shape_data.get("solar_array_area")),
                                                shape_data.get("solar_array_axis"),
                                                datetime_to_absolutedate(shape_data.get("reference_date")),
                                                shape_data.get("solar_array_normal"),
                                                float(rotation_rate),
                                                float(drag_data.get("drag_coeff")),
                                                float(spectral_data.get("absorption_coeff")),
                                                float(spectral_data.get("reflection_coeff")))

        return shape


class Spacecraft:
    def __init__(self, name, **kwargs):
        self.name = name

        ## physical characteristics

        # physical model
        self.physical_model = None

        # dimensions
        self.x_length = 0.0
        self.y_length = 0.0
        self.z_length = 0.0
        self.shape_type = None

        # solar array properties
        self.solar_array_area = 0.0
        self.solar_array_axis = Vector3D.PLUS_K
        self.solar_array_normal = Vector3D.PLUS_J
        self.solar_array_efficiency = kwargs.get("solar_array_efficiency") or 0.22

        # spectral properties
        self.reflection_coeff = 0.5
        self.absorption_coeff = 0.5

        # drag properties
        self.drag_coeff = 2.25 

        # inertial properties
        self.mass = 1.0
        self.cg = np.array([[0.0], [0.0], [0.0]])

        ## propagation properties

        # central body
        self.central_body = kwargs["central_body"] if "central_body" in kwargs else None

        # orbit type
        self.orbit_type = OrekitOrbitType.EQUINOCTIAL

        # initial state
        self.base_initial_state = None
        self.base_initial_epoch = None
        self.initial_keplerian_state = None

        # initial orbit
        self.base_initial_orbit = None

        # initial attitude
        self.base_initial_attitude = None

        # current state
        self.base_current_state = None
        self.base_current_time = None

        # current orbit
        self.base_current_orbit = None

        # current attitude
        self.current_attitude = None
        self.base_current_attitude = None

        # attitude law
        self.base_attitude_law = None

        # eclipse
        self.in_eclipse = False

        # geodetic
        self.current_geodetic_projection = None
        self.ground_track = []

        # power
        self.current_generated_power = None
        self.generated_power_history = []

        # propagator
        self.propagator = None
        self.propagator_options = kwargs.get("propagator_options") or dict(propagator_type=PropagatorTypes.NUMERICAL.name)
        self.base_propagated_states = []
        self.propagated_keplerian_elements = {
            "sma": [],
            "eccentricity": [],
            "raan": [],
            "inclination": [],
            "perigee": [],
            "mean_anomaly": [],
            "epoch": []
        }

        # cartesian states in inertial frame
        self.propagated_cartesian_states = {
            "x": [],
            "y": [],
            "z": [],
            "xdot": [],
            "ydot": [],
            "zdot": []
        }

        # propagated attitude
        self.base_propagated_attitude = []
        self.propagated_attitude = []

        ## components

        # sensors
        self.sensors = {}
        self.sensor_footprints = {}

        # object state
        self.is_initialized = False
        self.__initialize()
    
    def get_name(self):
        return self.name
    
    def get_x_length(self):
        return self.x_length

    def get_y_length(self):
            return self.y_length

    def get_z_length(self):
            return self.z_length

    def get_shape_type(self):
        return self.shape_type

    def get_mass(self):
        return self.mass

    def get_central_body(self):
        return self.central_body

    def get_base_initial_state(self):
        return self.base_initial_state

    def get_base_current_state(self):
        return self.base_current_state

    def get_initial_keplerian_state(self):
        return self.initial_keplerian_state

    def get_base_current_time(self):
        return self.base_current_time

    def get_current_time(self):
        return absolutedate_to_datetime(self.get_base_current_time())

    def get_base_initial_attitude(self):
        return self.base_initial_attitude

    def get_current_attitude(self):
        return self.current_attitude

    def get_current_attitude_dcm(self):
        q0, q = self.get_current_attitude()
        return quat2dcm(q0, q)

    def get_base_attitude_law(self):
        return self.base_attitude_law

    def get_base_current_attitude(self):
        return self.base_current_attitude

    def get_orbit_type(self):
        return self.orbit_type

    def get_base_propagated_states(self):
        return self.base_propagated_states
    
    def get_propagated_keplerian_elements(self):
        return self.propagated_keplerian_elements
    
    def get_propagated_cartesian_states(self):
        return self.propagated_cartesian_states
    
    def get_propagated_attitude(self):
        return self.propagated_attitude
    
    def get_propagator(self):
        return self.propagator

    def get_sensor_by_name(self, name):
        if self.sensors:
            return self.sensors.get(name)
        else:
            return None
    
    def get_in_eclipse(self):
        return self.in_eclipse

    def get_current_generated_power(self):
        return self.current_generated_power

    def get_generated_power_history(self):
        return self.generated_power_history

    def get_current_geodetic_projection(self):
        return self.current_geodetic_projection
    
    def get_ground_track(self):
        return self.ground_track

    def get_sensor_footprints_all(self):
        return self.sensor_footprints
    
    def set_x_length(self, length):
        self.x_length = length
        self._update_physical_model()

    def set_y_length(self, length):
        self.y_length = length
        self._update_physical_model()

    def set_z_length(self, length):
        self.z_length = length
        self._update_physical_model()

    def set_dimensions(self, x_length, y_length, z_length):
        self.x_length = x_length
        self.y_length = y_length
        self.z_length = z_length
        self._update_physical_model()

    def set_solar_array_area(self, area):
        self.solar_array_area = area
        self._update_physical_model()

    def set_solar_array_axis(self, vector):
        self.solar_array_axis = Vector3D(float(vector[0]), float(vector[1]), float(vector[2]))
        self._update_physical_model()
    
    def set_solar_array_normal(self, vector):
        self.solar_array_normal = Vector3D(float(vector[0]), float(vector[1]), float(vector[2]))
        self._update_physical_model()
    
    def set_solar_array_efficiency(self, efficiency):
        self.solar_array_efficiency = efficiency
        self._update_physical_model()

    def set_mass(self, mass):
        self.mass = mass

    def set_central_body(self, body):
        self.central_body = body
    
    def set_initial_state(self, epoch, state):
        self.base_initial_epoch = datetime_to_absolutedate(epoch)
        self.base_current_time = self.base_initial_epoch
        sma = state.get("sma")
        inclination = state.get("inclination")
        eccentricity = state.get("eccentricity")
        raan = state.get("raan")
        perigee = state.get("perigee")
        mean_anomaly = state.get("mean_anomaly")
        attitude_law = state.get("attitude_law") or AttitudeLawType.NADIR_POINTING.name

        self.initial_keplerian_state = state

        self.base_initial_orbit =  OrekitEquinoctialOrbit(float(sma),
                                  float(eccentricity*np.cos(perigee + raan)),
                                  float(eccentricity*np.sin(perigee + raan)), 
                                  float(np.tan(inclination/2)*np.cos(raan)),
                                  float(np.tan(inclination/2)*np.sin(raan)), 
                                  float(raan + perigee + mean_anomaly), 
                                  OrekitPositionAngle.MEAN,
                                  EME2000, self.base_initial_epoch, self.central_body.get_mu())
        self.base_current_orbit = self.base_initial_orbit
        self.set_initial_attitude_law(attitude_law)
        logger.debug(f"initial orbit date: {self.base_initial_orbit.getDate()}")
        logger.debug(f"initial attitude date: {self.base_initial_attitude.getDate()}")
        self.base_initial_state = OrekitSpacecraftState(self.base_initial_orbit, self.base_initial_attitude, float(self.mass))
        self.base_current_state = self.base_initial_state
        
        self.current_geodetic_projection = self.compute_current_geodetic_projection()
        if self.propagator is None:
            self.set_propagator_from_options(self.propagator_options)
        self.generated_power_history = []
        self.set_current_generated_power(self.compute_current_generated_power())
        # self.add_generated_power_history(epoch, self.compute_current_generated_power())
        self.ground_track = []
        self.add_groundtrack_point(epoch, self.current_geodetic_projection[0], self.current_geodetic_projection[1])

    def _update_base_initial_attitude(self):
        if self.base_initial_state is not None:
            print(f"updating initial attitude")
            self.base_initial_state = OrekitSpacecraftState(self.base_initial_state.getOrbit(), self.base_initial_attitude, float(self.mass))
            print(f"updated initial attitude")

    def _update_base_initial_orbit(self):
        if self.base_initial_state is not None:
            self.base_initial_state = OrekitSpacecraftState(self.base_initial_orbit, self.base_initial_state.getAttitude(), float(self.mass))

    def set_in_eclipse(self, in_eclipse):
        self.in_eclipse = in_eclipse

    def set_base_current_state(self, state):
        self.base_current_state = state
        self.set_base_current_attitude(self.base_current_state.getAttitude())

    def set_base_current_time(self, time):
        self.base_current_time = time

    def set_initial_attitude_law(self, attitude_law : str):
        self.set_current_attitude_law(attitude_law)
        self._set_base_initial_attitude(self.get_base_attitude_law().getAttitude(self.base_initial_orbit, self.base_initial_epoch, self.get_central_body().get_inertial_frame()))
        
    def set_current_attitude_law(self, attitude_law: str):
        base_attitude_law = AttitudeLawFactory.get_base_attitude_law(attitude_law, self)
        self._set_base_attitude_law(base_attitude_law)
        
    def _set_base_attitude_law(self, attitude_law):
        self.base_attitude_law = attitude_law
        self.set_base_current_attitude(self.get_base_attitude_law().getAttitude(self.base_current_orbit, self.base_current_time, self.get_central_body().get_inertial_frame()))
    
    def _set_base_initial_attitude(self, attitude):
        self.base_initial_attitude = attitude
        # self._update_base_initial_attitude()

    def set_base_current_attitude(self, attitude):
        self.base_current_attitude = attitude
        q0 = attitude.getRotation().getQ0()
        q = np.array([
            attitude.getRotation().getQ1(),
            attitude.getRotation().getQ2(),
            attitude.getRotation().getQ3()
        ])
        self.set_current_attitude((q0, q))

    def set_current_attitude(self, attitude):
        self.current_attitude = attitude
    
    def set_base_propagated_states(self, states):
        self.base_propagated_states = states
    
    def set_propagator(self, propagator):
        self.propagator = propagator

    def set_propagator_from_options(self, propagator_options):
        self.propagator_options = propagator_options
        propagator_type = propagator_options.get("propagator_type")
        options = propagator_options.copy()
        options.pop("propagator_type")
        self.propagator = PropagatorFactory.get_propagator_of_type(self, propagator_type, **options)

    def set_current_geodetic_projection(self, projection):
        self.current_geodetic_projection = projection
        self.add_groundtrack_point(absolutedate_to_datetime(self.base_current_time), projection[0], projection[1])

    def set_current_generated_power(self, power):
        self.current_generated_power = power
        self.add_generated_power_history(absolutedate_to_datetime(self.base_current_time), power)

    def add_sensor(self, name, fov_type, fov_options, **kwargs):
        sensor = SpaceSensor(name, fov_type, fov_options, **kwargs)
        self.sensors.update({name: sensor})
        return sensor
        
    def _update(self):
        raise NotImplementedError("Update option not implemented yet.")
    
    def __initialize(self):
        if not self.is_initialized:
            self.shape_type = SpacecraftShapeType.CUBOID_DEPLOYED_PANELS
        t_epoch = datetime.datetime.utcnow()
        self.base_initial_epoch = datetime_to_absolutedate(t_epoch)
        self.base_current_time = self.base_initial_epoch
        if self.central_body is None:
            self.central_body = Earth()
        state_epoch = {
            "sma": 6945.588024099e3,
            "inclination": np.deg2rad(97.7),
            "eccentricity": 0.00,
            "raan": np.deg2rad(0.0),
            "perigee": 0.0,
            "mean_anomaly": np.deg2rad(0.0),
            "attitude_law": AttitudeLawType.NADIR_POINTING.name 
        }
        self.set_initial_state(t_epoch, state_epoch)
        self.current_geodetic_projection = self.compute_current_geodetic_projection()
        if self.propagator is None:
            self.set_propagator_from_options(self.propagator_options)
        self.add_groundtrack_point(t_epoch, self.current_geodetic_projection[0], self.current_geodetic_projection[1])
        self._update_physical_model()
        self.is_initialized = True

    def _update_physical_model(self):
        shape_data = {
            "x_length": self.x_length,
            "y_length": self.y_length,
            "z_length": self.z_length,
            "reference_date": self.get_current_time(),
            "solar_array_area": self.solar_array_area,
            "solar_array_axis": self.solar_array_axis,
            "solar_array_normal": self.solar_array_normal
        }
        drag_data = {
            "drag_coeff": self.drag_coeff
        }
        spectral_data = {
            "reflection_coeff": self.reflection_coeff,
            "absorption_coeff": self.absorption_coeff
        }
        self.physical_model = SpacecraftShape(self.shape_type, shape_data, drag_data, spectral_data)
        self.generated_power_history = []
        self.set_current_generated_power(self.compute_current_generated_power())

    def add_base_propagated_states(self, state):
        self.base_propagated_states.append(state)
        attitude = state.getAttitude()
        self.base_propagated_attitude.append(attitude)
        self.propagated_attitude.append(orekit_attitude_to_quat(attitude))
        keplerian = OrekitKeplerianOrbit.cast_(OrekitOrbitType.KEPLERIAN.convertType(state.getOrbit()))
        elements = {
            "sma": keplerian.getA(),
            "eccentricity": keplerian.getE(),
            "raan": keplerian.getRightAscensionOfAscendingNode(),
            "inclination": keplerian.getI(),
            "perigee": keplerian.getPerigeeArgument(),
            "mean_anomaly": keplerian.getMeanAnomaly(),
            "epoch": absolutedate_to_datetime(state.getDate())   
        }
        pv = state.getPVCoordinates(self.get_central_body().get_inertial_frame())
        p = pv.getPosition()
        v = pv.getVelocity()
        cartesian ={
            "x":p.getX(),
            "y": p.getY(),
            "z": p.getZ(),
            "xdot": v.getX(),
            "ydot": v.getY(),
            "zdot": v.getZ()
        }
        self.add_propagated_keplerian_element(elements)
        self.add_propagated_cartesian_states(cartesian)
    
    def add_propagated_keplerian_element(self, elements):
        for element, values in self.propagated_keplerian_elements.items():
            values.append(elements.get(element))
    
    def add_propagated_cartesian_states(self, state):
        for element, values in self.propagated_cartesian_states.items():
            values.append(state.get(element))
    
    def add_environment_forces(self, forces_options=None, propagator_options=None):
        if propagator_options is None:
            propagator_options = {
                "propagator_type": PropagatorTypes.NUMERICAL.name
            }
        if self.propagator is None:
            self.set_propagator_from_options(propagator_options)
        
        forces_all = []
        if self.propagator.get_type() == PropagatorTypes.NUMERICAL:
            forces_all = self.add_environment_forces_numerical(forces_options)
        elif self.propagator.get_type() == PropagatorTypes.DSST:
            forces_all = self.add_environment_forces_dsst(forces_options)
        return forces_all
    
    def add_environment_forces_numerical(self, forces_options):
        gravity = None
        drag = None
        lunar_3b = None
        solar_3b = None
        srp = None # solar radiation pressure
        if forces_options is None:
            gravity = self.central_body.get_base_gravity_force()
            self.propagator.add_base_force_model(gravity)
            return [gravity]
        if forces_options.get("gravity"):
            gravity = self.central_body.get_base_gravity_force(degree = forces_options.get("gravity").get("degree"),
                                                            order = forces_options.get("gravity").get("order")
                                                            )
            self.propagator.add_base_force_model(gravity)
        if forces_options.get("drag") is not None:
            atmospheric_model = self.central_body.get_base_atmosphere_model(forces_options.get("drag"))
            drag = OrekitDragForce(atmospheric_model, self.physical_model.get_base_shape())
            self.propagator.add_base_force_model(drag)
        if forces_options.get("lunar_3b"):
            lunar_3b = OrekitThirdBodyAttraction(MOON)
            self.propagator.add_base_force_model(lunar_3b)
        if forces_options.get("solar_3b"):
            solar_3b = OrekitThirdBodyAttraction(SUN)
            self.propagator.add_base_force_model(solar_3b)
        if forces_options.get("srp"):
            srp = OrekitSolarRadiationPressure(SUN, self.central_body.get_equatorial_radius(), self.physical_model.get_base_shape())
            self.propagator.add_base_force_model(srp)
        forces_all = [gravity, drag, lunar_3b, solar_3b, srp]
        return [f for f in forces_all if f is not None]

    def add_environment_forces_dsst(self, forces_options):
        gravity = None
        drag = None
        lunar_3b = None
        solar_3b = None
        srp = None # solar radiation pressure
        if forces_options is None:
            gravity = self.central_body.get_base_gravity_force(propagator_type = PropagatorTypes.DSST.name)
            self.propagator.add_base_force_model(gravity)
            return [gravity]
        if forces_options.get("gravity"):
            zonal = self.central_body.get_base_gravity_force(degree = forces_options.get("gravity").get("degree"),
                                                            order = forces_options.get("gravity").get("order"),
                                                            propagator_type = PropagatorTypes.DSST.name)
            tesseral = self.central_body.get_tesseral_gravity_force(degree = forces_options.get("gravity").get("degree"),
                                                            order = forces_options.get("gravity").get("order"),
                                                            propagator_type = PropagatorTypes.DSST.name)
            self.propagator.add_base_force_model(zonal)
            # self.propagator.add_base_force_model(tesseral)
        if forces_options.get("drag") is not None:
            atmospheric_model = self.central_body.get_base_atmosphere_model(forces_options.get("drag"))
            drag = OrekitDSSTAtmosphericDrag(atmospheric_model, self.physical_model.get_base_shape(), self.central_body.get_mu())
            self.propagator.add_base_force_model(drag)
        if forces_options.get("lunar_3b"):
            lunar_3b = OrekitDSSTThirdBody(MOON, self.central_body.get_mu())
            self.propagator.add_base_force_model(lunar_3b)
        if forces_options.get("solar_3b"):
            solar_3b = OrekitDSSTThirdBody(SUN, self.central_body.get_mu())
            self.propagator.add_base_force_model(solar_3b)
        if forces_options.get("srp"):
            srp = OrekitDSSTSolarRadiationPressure(SUN, self.central_body.get_equatorial_radius(), self.physical_model.get_base_shape(), self.central_body.get_mu())
            self.propagator.add_base_force_model(srp)
        forces_all = [gravity, drag, lunar_3b, solar_3b, srp]
        return [f for f in forces_all if f is not None]

    @initialization_required
    def propagate(self, start_time, end_time, mode="slave", handlers=None):
        if self.propagator is None:
            self.propagator = Propagator(self)
        self.propagator.propagate(start_time, end_time, mode, handlers)

    @initialization_required
    def add_event_handler(self, event_type, event_handler, **kwargs):
        detector = EventFactory.get_base_detector_of_type(event_type, self, event_handler, **kwargs)
        self.propagator.attach_base_event_detector(detector)
        return detector

    @initialization_required
    def add_events(self,event_types, event_handlers):
        for event_type, handler in zip(event_types, event_handlers):
            self.add_event(event_type, handler)

    def compute_current_geodetic_projection(self):
        pv_inertial = self.base_current_state.getPVCoordinates()
        geodetic = self.central_body.get_base_shape().transform(pv_inertial.getPosition(), self.central_body.get_inertial_frame(), self.get_base_current_time())
        return (float(geodetic.getLatitude()), float(geodetic.getLongitude()))

    def compute_current_generated_power(self):
        if self.in_eclipse:
            return 0.0
        sun_pv = OrekitExtendedPVCoordinatesProvider.cast_(SUN)
        sun_coordinates = sun_pv.getPVCoordinates(self.base_current_state.getDate(), self.central_body.get_inertial_frame()).getPosition().normalize()
        sun_position = np.array([
            sun_coordinates.getX(),
            sun_coordinates.getY(),
            sun_coordinates.getZ()
        ])
        dcm = self.get_current_attitude_dcm()
        solar_array_normal = dcm.T@np.array([self.solar_array_normal.getX(), self.solar_array_normal.getY(), self.solar_array_normal.getZ()])
        watts = self.solar_array_efficiency * AVG_SOLAR_FLUX_EARTH * self.solar_array_area * np.dot(solar_array_normal, sun_position)
        logger.debug(f"solar panel area: {self.solar_array_area}")
        logger.debug(f"solar array eff: {self.solar_array_efficiency}")
        logger.debug(f"avg flux: {AVG_SOLAR_FLUX_EARTH}")
        logger.debug(f"dot product: {np.dot(solar_array_normal, sun_position)}")
        logger.debug(f"angle between sun and solar array normal: {np.rad2deg(np.arccos( np.dot(solar_array_normal, sun_position)))}")
        logger.debug(f"watts: {watts}")
        if watts < 0:
            watts = 0.0
        return watts
    
    def compute_sensor_footprint_all(self, angular_step=np.deg2rad(10)):
        fps = {}
        for sensor_name, sensor in self.sensors.items():
            fp = sensor.compute_footprint(self, self.get_central_body(), angular_step)
            fps.update({sensor_name: fp})
        return fps

    def add_groundtrack_point(self, time, latitude, longitude):
        self.ground_track.append((time, latitude, longitude))

    def add_generated_power_history(self, time, power):
        self.generated_power_history.append((time, power))

    def add_sensor_footprint_history_all(self, footprints):
        for sensor_name, fp in footprints.items():
            self.add_sensor_footprint_history(sensor_name, fp)

    def add_sensor_footprint_history(self, sensor_name, footprint):
        fp_existing = self.sensor_footprints.get(sensor_name)
        if not fp_existing:
            self.sensor_footprints.update({
                sensor_name: [footprint]
            })
            fp_existing = self.sensor_footprints.get(sensor_name)
            return
        fp_existing.append(footprint)
    


    