from org.orekit.propagation.events.handlers import PythonEventHandler as OrekitPythonEventHandler
from org.hipparchus.ode.events import Action as HipparchusAction
from org.orekit.propagation.events import EclipseDetector as OrekitEclipseDetector
from org.orekit.propagation.events import EventDetector as OrekitEventDetector
from org.orekit.propagation.events import ElevationDetector as OrekitElevationDetector
from org.orekit.propagation.events import ApsideDetector as OrekitApsideDetector


from utils.astro.thin_orekit_wrapper.globals import SUN

import enum
import logging

logger = logging.getLogger(__name__)

class EventType(enum.Enum):
    ECLIPSE_ENTER_DETECTOR = 0
    ECLIPSE_EXIT_DETECTOR = 1
    ELEVATION_DETECTOR = 2
    VISIBILITY_DETECTOR = 3
    RAAN_DETECTOR = 4
    APSIDE_DETECTOR = 5
    APOGEE_DETECTOR = 6
    PERIGEE_DETECTOR = 7

class EventFactory:
    @staticmethod
    def get_base_detector_of_type(event_type, spacecraft, handler, **kwargs):
        if event_type == EventType.ECLIPSE_ENTER_DETECTOR:
            orekit_handler = EclipseEnterDetector(spacecraft, handler).of_(OrekitEclipseDetector)
            base_detector = OrekitEclipseDetector(SUN,  float(696000000.0), spacecraft.get_central_body().get_base_shape()).withHandler(orekit_handler)
            return base_detector
        elif event_type == EventType.ECLIPSE_EXIT_DETECTOR:
            orekit_handler = EclipseExitDetector(spacecraft, handler).of_(OrekitEclipseDetector)
            base_detector = OrekitEclipseDetector(SUN,  float(696000000.0), spacecraft.get_central_body().get_base_shape()).withHandler(orekit_handler)
            return base_detector
        elif event_type == EventType.ELEVATION_DETECTOR:
            if "station" not in kwargs:
                raise ValueError("No station was passed to compute elevation from.")
            station = kwargs["station"]
            orekit_handler = ElevationDetectorHandler(spacecraft, handler, station=station).of_(OrekitElevationDetector)
            base_detector = OrekitElevationDetector(station.get_topocentric_frame()).withConstantElevation(float(station.get_minimum_elevation())).withHandler(orekit_handler)
            return base_detector
        elif event_type == EventType.APSIDE_DETECTOR:
            orekit_handler = ApsideDetectorHandler(spacecraft, handler).of_(OrekitApsideDetector)
            base_detector = OrekitApsideDetector(spacecraft.get_base_current_state().getOrbit()).withHandler(orekit_handler)
            return base_detector
        else:
            raise Exception("Unknown Event Type.")

class EclipseEnterDetector(OrekitPythonEventHandler):
    def __init__(self, spacecraft, handler) -> None:
        super(EclipseEnterDetector, self).__init__()
        self.spacecraft = spacecraft
        self.handler = handler
        self.name = f"EclipseEnterDetector_{spacecraft.get_name()}"
    
    def init(self, s, T):
        pass

    def eventOccurred(self, s, detector, increasing):
        if not increasing:
            logger.info(f"{self.spacecraft.get_name()} entered eclipse.")
            self.spacecraft.set_base_current_state(s)
            self.spacecraft.set_base_current_time(s.getDate())
            self.spacecraft.set_in_eclipse(True)
            self.handler(self.spacecraft)
        return HipparchusAction.CONTINUE
    
    def resetState(self, detector, oldState):
        return oldState
    
class EclipseExitDetector(OrekitPythonEventHandler):
    def __init__(self, spacecraft, handler) -> None:
        super(EclipseExitDetector, self).__init__()
        self.spacecraft = spacecraft
        self.handler = handler
        self.name = f"EclipseExitDetector_{spacecraft.get_name()}"
    
    def init(self, s, T):
        pass

    def eventOccurred(self, s, detector, increasing):
        if increasing:
            logger.info(f"{self.spacecraft.get_name()} exited eclipse.")
            self.spacecraft.set_base_current_state(s)
            self.spacecraft.set_base_current_time(s.getDate())
            self.spacecraft.set_in_eclipse(False)
            self.handler(self.spacecraft)
        return HipparchusAction.CONTINUE
    
    def resetState(self, detector, oldState):
        return oldState

class ElevationDetectorHandler(OrekitPythonEventHandler):
    def __init__(self, spacecraft, handler, **kwargs) -> None:
        super(ElevationDetectorHandler, self).__init__()
        self.spacecraft = spacecraft
        self.handler = handler
        self.station = kwargs["station"]
        self.name = f"ElevationDetector_{spacecraft.get_name()}_{self.station.get_name()}"

    def init(self, s, T):
        pass

    def eventOccurred(self, s, detector, increasing):
        self.spacecraft.set_base_current_state(s)
        self.spacecraft.set_base_current_time(s.getDate())
        if increasing:
            logger.info(f"{self.spacecraft.get_name()} is in view of Station {self.station.get_name()}")
            self.handler(self.spacecraft, station = self.station, status = True)
        else:
            logger.info(f"{self.spacecraft.get_name()} is out of view of Station {self.station.get_name()}")
            self.handler(self.spacecraft, station = self.station, status = False)
        return HipparchusAction.CONTINUE
    
    def resetState(self, detector, oldState):
        return oldState

class ApsideDetectorHandler(OrekitPythonEventHandler):
    def __init__(self, spacecraft, handler, **kwargs) -> None:
        super(ApsideDetectorHandler, self).__init__()
        self.spacecraft = spacecraft
        self.handler = handler
        self.name = f"ApsideDetector_{spacecraft.get_name()}"
    
    def init(self, s, T):
        pass

    def eventOccurred(self, s, detector, increasing):
        self.spacecraft.set_base_current_state(s)
        self.spacecraft.set_base_current_time(s.getDate())
        if increasing:
            self.handler(self.spacecraft, apside="perigee")
        else:
            self.handler(self.spacecraft, apside="apogee")
        return HipparchusAction.CONTINUE