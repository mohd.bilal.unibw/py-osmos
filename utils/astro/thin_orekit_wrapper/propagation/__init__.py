from org.hipparchus.ode.nonstiff import DormandPrince853Integrator
from org.orekit.propagation.numerical import NumericalPropagator as OrekitNumericalPropagator
from org.orekit.propagation.semianalytical.dsst import DSSTPropagator as OrekitDSSTPropagator
from org.orekit.propagation.sampling import PythonOrekitFixedStepHandler
from org.orekit.propagation import PropagationType as OrekitPropagationType
from orekit import JArray_double as OrekitJArray_double
from orekit.pyhelpers import absolutedate_to_datetime, datetime_to_absolutedate
import numpy as np
import logging
import enum
import abc

logger = logging.getLogger(__name__)

class PropagatorTypes(enum.Enum):
    NUMERICAL = 0
    DSST = 1

class PropagationType(enum.Enum):
    MEAN = 0
    OSCULATING = 1

class PropagatorFactory:
    @staticmethod
    def get_propagator_of_type(spacecraft, propagator_type, **kwargs):
        propagator = None
        if propagator_type.upper() == PropagatorTypes.NUMERICAL.name:
            propagator = Propagator(spacecraft, **kwargs)
        elif propagator_type.upper() == PropagatorTypes.DSST.name:
            propagator = DSSTPropagator(spacecraft, **kwargs)
        else:
            raise ValueError("The propagator type you requested is not available!")
        return propagator


class MasterModeHandler:
    def __init__(self, name) -> None:
        self.name = name
        self.data = None
    
    def execute(self, spacecraft):
        pass

class OrekitPropagationHandler(PythonOrekitFixedStepHandler):
    def __init__(self, spacecraft, handlers):
        self.handlers = handlers
        self.spacecraft = spacecraft 
        super(OrekitPropagationHandler, self).__init__()

    def init(self, s0, t, step):
        pass

    def handleStep(self, currentState, isLast):
        if isLast:
            logger.info("propagation ended.")
        self.spacecraft.set_base_current_state(currentState)
        self.spacecraft.set_base_current_time(currentState.getDate())
        for handler in self.handlers:
            handler.execute(self.spacecraft)

class IPropagator(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def initialize(self):
        pass

    @abc.abstractmethod
    def propagate(self, start_time, end_time, mode="slave", handlers=None):
        pass

class BasePropagator(IPropagator):
    def __init__(self, spacecraft, **kwargs):
        super().__init__()
        self.spacecraft = spacecraft
        self.min_step = kwargs["min_step"] if "min_step" in kwargs else 0.001
        self.max_step = kwargs["max_step"] if "max_step" in kwargs else 10.0
        self.step_size = kwargs["step_size"] if "step_size" in kwargs else 60
        self.position_tolerance = kwargs["tolerance_error"] if "tolerance_error" in kwargs else 1.0
        self.velocity_tolerance = kwargs["tolerance_error"] if "tolerance_error" in kwargs else 1e-3
        self.tolerances = None
        self.integrator = None
        self.base_propagator = None
        self.base_handler = None
        self.type_ = None

    def initialize(self):
        pass

    def propagate(self, start_time, end_time, mode="slave", handlers=None):
        pass

    def get_type(self):
        return self.type_

    def set_orbit_type(self):
        self.base_propagator.setOrbitType(self.spacecraft.get_orbit_type())
    
    def get_min_step(self):
        return self.min_step

    def add_base_force_model(self, force_model):
        self.base_propagator.addForceModel(force_model)

    def attach_base_event_detector(self, base_detector):
        self.base_propagator.addEventDetector(base_detector)

class Propagator(BasePropagator):
    def __init__(self, spacecraft, **kwargs) -> None:
        super().__init__(spacecraft, **kwargs)
        self.type_ = PropagatorTypes.NUMERICAL
        self.initialize()
    
    def initialize(self):
        self.tolerances = OrekitNumericalPropagator.tolerances(self.position_tolerance,
                                                               self.spacecraft.get_base_initial_state().getOrbit(),
                                                               self.spacecraft.get_orbit_type())
        self.integrator = DormandPrince853Integrator(self.min_step, self.max_step,
                                                     OrekitJArray_double.cast_(self.tolerances[0]),
                                                     OrekitJArray_double.cast_(self.tolerances[1]))
        self.base_propagator = OrekitNumericalPropagator(self.integrator, self.spacecraft.get_base_attitude_law())
        self.set_orbit_type()
        
    def propagate(self, start_time, end_time, mode="slave", handlers=None):
        duration = (end_time - start_time).total_seconds()
        start_time_orekit = datetime_to_absolutedate(start_time)
        end_time_orekit = datetime_to_absolutedate(end_time)
        if self.spacecraft.get_base_initial_state() is None:
            raise Exception("Spacecraft Epoch was not set."
                            "Set spacecraft epoch to propagate.")
        self.base_propagator.setInitialState(self.spacecraft.get_base_initial_state())
        if mode=="master":
            if handlers is None:
                raise ValueError("Please pass a list of handlers to execute.")
            self.base_handler = OrekitPropagationHandler(self.spacecraft, handlers)
            self.base_propagator.setMasterMode(self.step_size, self.base_handler)
            self.base_propagator.propagate(start_time_orekit, end_time_orekit)
            return None
        time_vec_orekit = [start_time_orekit.shiftedBy(float(dt)) for dt in np.arange(self.step_size, duration, self.step_size)]
        for t in time_vec_orekit:
            state = self.base_propagator.propagate(t)
            # print(f"state at {state.getDate().toString()}: {state} \n")
            self.spacecraft.set_base_current_state(state)
            self.spacecraft.set_base_current_time(state.getDate())
            self.spacecraft.add_base_propagated_states(state)
            self.spacecraft.set_current_geodetic_projection(self.spacecraft.compute_current_geodetic_projection())
            self.spacecraft.set_current_generated_power(self.spacecraft.compute_current_generated_power())
            self.spacecraft.add_sensor_footprint_history_all(self.spacecraft.compute_sensor_footprint_all())            
        return None


class DSSTPropagator(BasePropagator):
    def __init__(self, spacecraft, **kwargs):
        super().__init__(spacecraft, **kwargs)
        self.type_ = PropagatorTypes.DSST
        self.propagation_type = kwargs.get("propagation_type")
        self.initial_condition_type = kwargs.get("initial_condition_type") or PropagationType.MEAN.name
        self.base_propagation_type = OrekitPropagationType.MEAN
        self.base_initial_condition_type = OrekitPropagationType.MEAN
        self.initialize()

    def initialize(self):
        if self.propagation_type is not None and self.propagation_type.upper() == PropagationType.OSCULATING.name:
            self.base_propagation_type = OrekitPropagationType.OSCULATING
        if self.initial_condition_type.upper() == PropagationType.OSCULATING.name:
            self.base_initial_condition_type = OrekitPropagationType.OSCULATING
        self.tolerances = OrekitDSSTPropagator.tolerances(self.position_tolerance,
                                                               self.spacecraft.get_base_initial_state().getOrbit())
        self.integrator = DormandPrince853Integrator(self.min_step, self.max_step,
                                                     OrekitJArray_double.cast_(self.tolerances[0]),
                                                     OrekitJArray_double.cast_(self.tolerances[1]))
        self.base_propagator = OrekitDSSTPropagator(self.integrator)
    
    def propagate(self, start_time, end_time, mode="slave", handlers=None):
        duration = (end_time - start_time).total_seconds()
        start_time_orekit = datetime_to_absolutedate(start_time)
        end_time_orekit = datetime_to_absolutedate(end_time)
        if self.spacecraft.get_base_initial_state() is None:
            raise Exception("Spacecraft Epoch was not set."
                            "Set spacecraft epoch to propagate.")
        self.base_propagator.setInitialState(self.spacecraft.get_base_initial_state(), self.base_initial_condition_type)
        if mode=="master":
            if handlers is None:
                raise ValueError("Please pass a list of handlers to execute.")
            self.base_handler = OrekitPropagationHandler(self.spacecraft, handlers)
            self.base_propagator.setMasterMode(self.step_size, self.base_handler)
            self.base_propagator.propagate(start_time_orekit, end_time_orekit)
            return None
        time_vec_orekit = [start_time_orekit.shiftedBy(float(dt)) for dt in np.arange(self.step_size, duration, self.step_size)]
        for t in time_vec_orekit:
            state = self.base_propagator.propagate(t)
            # print(f"state at {state.getDate().toString()}: {state} \n")
            self.spacecraft.set_base_current_state(state)
            self.spacecraft.set_base_current_time(state.getDate())
            self.spacecraft.add_base_propagated_states(state)
            self.spacecraft.set_current_geodetic_projection(self.spacecraft.compute_current_geodetic_projection())
            self.spacecraft.set_current_generated_power(self.spacecraft.compute_current_generated_power())
            self.spacecraft.add_sensor_footprint_history_all(self.spacecraft.compute_sensor_footprint_all())            
        return None