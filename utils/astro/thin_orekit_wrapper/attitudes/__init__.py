from org.orekit.attitudes import NadirPointing as OrekitNadirPointing

from utils.attitude.helpers.attitude_parameters import quat2dcm
import numpy as np
import enum

def orekit_attitude_to_quat(attitude):
    q0 = attitude.getRotation().getQ0()
    q = np.array([
        attitude.getRotation().getQ1(),
        attitude.getRotation().getQ2(),
        attitude.getRotation().getQ3()
    ])
    return (q0, q)

def orekit_attitude_to_dcm(attitude):
    q0, q  = orekit_attitude_to_quat(attitude)
    dcm = quat2dcm(q0, q)
    return dcm
    

class AttitudeLawType(enum.Enum):
    BODY_CENTER_POINTING = 0
    CELESTIAL_BODY_POINTING = 1
    FIXED_RATE = 2
    GROUND_POINTING = 3
    INERTIAL_PROVIDER = 4
    LOF_OFFSET_POINTING = 5
    NADIR_POINTING = 6
    SPIN_STABILIZED = 7
    TARGET_POINTING = 8
    YAW_COMPENSATION = 9
    YAW_STEERING = 10


class AttitudeLawFactory:
    @staticmethod
    def get_base_attitude_law(law_type, spacecraft, **kwargs):
        base_law = None
        if law_type.upper() == AttitudeLawType.NADIR_POINTING.name:
            central_body = spacecraft.get_central_body()
            base_law = OrekitNadirPointing(central_body.get_inertial_frame(),
                                           central_body.get_base_shape())
            return base_law
        else:
            raise NotImplementedError(f"{law_type} has not been wrapped yet.")
