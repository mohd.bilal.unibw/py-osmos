from enum import Enum
import orekit
from org.orekit.bodies import CelestialBodyFactory as OrekitCelestialBodyFactory
from org.orekit.bodies import OneAxisEllipsoid
from org.orekit.utils import Constants
from org.orekit.utils import ExtendedPVCoordinatesProvider
from org.orekit.utils import IERSConventions
from org.orekit.frames import FramesFactory as OrekitFramesFactory
from org.orekit.forces.gravity.potential import GravityFieldFactory
from org.orekit.forces.gravity import HolmesFeatherstoneAttractionModel
from org.orekit.models.earth.atmosphere import HarrisPriester
from org.orekit.propagation.semianalytical.dsst.forces import DSSTZonal, DSSTTesseral
from orekit.pyhelpers import datetime_to_absolutedate
from utils.astro.thin_orekit_wrapper.globals import EME2000, SUN
from utils.astro.thin_orekit_wrapper.propagation import PropagatorTypes
from utils.astro.constants import SIDEREAL_DAY
import logging
import datetime
import numpy as np

logger = logging.getLogger(__name__)


class Bodies(Enum):
    SUN = 0
    MERCURY = 1
    VENUS = 2
    EARTH = 3
    MARS = 4
    JUPITER = 5
    SATURN = 6
    URANUS = 7
    NEPTUNE = 8
    PLUTO = 9
    MOON = 10


class CelestialBody:
    def __init__(self, name, **kwargs) -> None:
        self.name = name
        self.base_body = None
        self.mu = None
        self.rotation_rate = None
        self.body_frame = None
        self.inertial_frame = None
        self.base_shape = None
        self.equatorial_radius = None
        self.default_gravity_options = None
        self.default_gravity_force = None
        self.default_atmosphere_options = None
        self.default_atmosphere_model = None
        self.position = None
        self.velocity = None
        self._pv_provider = None
        self.is_initialized = False

    def __initialize(self):
        pass

    def get_name(self):
        return self.name
    
    def get_base_body(self):
        return self.base_body

    def get_mu(self):
        return self.mu

    def get_rotation_rate(self):
        return self.rotation_rate

    def get_body_frame(self):
        return self.body_frame

    def get_inertial_frame(self):
        return self.inertial_frame

    def get_base_shape(self):
        return self.base_shape
    
    def get_equatorial_radius(self):
        return self.base_shape.getEquatorialRadius()

    def get_default_gravity_force(self):
        return self.default_gravity_force

    def get_base_gravity_force(self, degree=2, order=2):
        pass

    def get_default_atmosphere_model(self):
        return self.default_atmosphere_model

    def get_base_atmosphere_model(self, options=None):
        pass
        
    def get_default_atmosphere_options(self):
        return self.default_atmosphere_options

    def get_base_position(self, time=None, frame=EME2000):
        if time is None:
            time = datetime.datetime.utcnow()
        base_position = self._pv_provider.getPVCoordinates(datetime_to_absolutedate(time), frame).getPosition()
        return base_position

    def get_position(self, time=None, frame=EME2000):
        base_position = self.get_base_position(time, frame)
        return np.array([
            base_position.getX(),
            base_position.getY(),
            base_position.getZ(),
        ])

    def set_name(self):
        return self.name

    def set_base_body(self, body):
        self.base_body = body

    def set_mu(self):
        return self.mu

    def set_body_frame(self, frame):
        self.body_frame = frame

    def set_inertial_frame(self, frame):
        self.inertial_frame = frame

    def set_base_shape(self, base_shape):
        self.base_shape = base_shape

    def set_gravity_order_degree(self, degree, order):
        self.default_gravity_options = {"degree": degree, "order": order}

    def set_default_gravity_force(self, force):
        self.default_gravity_force = force

    def set_default_atmosphere_options(self, options):
        self.default_atmosphere_options = options

    def set_default_atmosphere_model(self, model):
        self.default_atmosphere_model = model



class Earth(CelestialBody):
    def __init__(self, **kwargs) -> None:
        super().__init__(Bodies.EARTH.name, **kwargs)
        self.__initialize()
    
    def __initialize(self):
        logger.info("Loading Earth Data.")
        self.base_body = OrekitCelestialBodyFactory.getEarth()
        self.mu = float(self.base_body.getGM())
        self.rotation_rate = 2*np.pi/SIDEREAL_DAY
        self.body_frame = OrekitFramesFactory.getITRF(IERSConventions.IERS_2010, True)
        self.inertial_frame = OrekitFramesFactory.getEME2000()
        self.base_shape = OneAxisEllipsoid(Constants.WGS84_EARTH_EQUATORIAL_RADIUS,
                                    Constants.WGS84_EARTH_FLATTENING, self.body_frame)
        self.default_gravity_options = {"order": 2, "degree": 2} if self.default_gravity_options is None else self.default_gravity_options
        self.default_gravity_force = HolmesFeatherstoneAttractionModel(self.body_frame, GravityFieldFactory.getNormalizedProvider(self.default_gravity_options.get("degree"), self.default_gravity_options.get("order")))
        self.default_atmosphere_options = {"harris_priester": {"cosine": 4.0}} if self.default_atmosphere_options is None else self.default_atmosphere_options
        self.default_atmosphere_model = HarrisPriester(SUN, self.base_shape, float(self.default_atmosphere_options.get("harris_priester").get("cosine")))
        self._pv_provider = ExtendedPVCoordinatesProvider.cast_(self.base_body)
        self.position = None
        self.velocity = None
        self.is_initialized = True

    def get_base_gravity_force(self, degree=2, order=0, normalized=True, propagator_type=None):
        force = None
        if propagator_type is None:
            propagator_type = PropagatorTypes.NUMERICAL.name
        if propagator_type.upper() == PropagatorTypes.DSST.name:
            normalized = False
        if not normalized and propagator_type.upper() == PropagatorTypes.DSST.name:
            force = DSSTZonal(GravityFieldFactory.getUnnormalizedProvider(degree, order))
        elif not normalized and propagator_type.upper() == PropagatorTypes.NUMERICAL.name:
            force = HolmesFeatherstoneAttractionModel(self.body_frame, GravityFieldFactory.getUnnormalizedProvider(degree, order))
        else:
            force = HolmesFeatherstoneAttractionModel(self.body_frame, GravityFieldFactory.getNormalizedProvider(degree, order))
        return force
    
    def get_tesseral_gravity_force(self, degree=2, order=2, normalized=True, propagator_type=None):
        force = None
        if propagator_type.upper() == PropagatorTypes.DSST.name:
            normalized = False
        if not normalized and propagator_type.upper() == PropagatorTypes.DSST.name:
            force = DSSTTesseral(self.get_body_frame(), float(2*np.pi/86400), GravityFieldFactory.getUnnormalizedProvider(degree, order))
        return force
        
    def get_base_atmosphere_model(self, options=None):
        if options is None:
            return self.get_default_atmosphere_model()
        model = HarrisPriester(SUN, self.base_shape, float(options.get("harris_priester").get("cosine")))
        return model
    

class Sun(CelestialBody):
    def __init__(self, **kwargs):
        super().__init__(Bodies.SUN.name, **kwargs)
        self.__initialize()
    
    def __initialize(self):
        self.base_body = OrekitCelestialBodyFactory.getSun()
        self.mu = float(self.base_body.getGM())
        self.inertial_frame = OrekitFramesFactory.getEME2000()
        self._pv_provider = ExtendedPVCoordinatesProvider.cast_(self.base_body)
        self.is_initialized = True


    

        


    