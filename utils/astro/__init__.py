from enum import Enum

class AstroProviders(Enum):
    OREKIT = 0
    FREEFLYER = 1
    STK = 2