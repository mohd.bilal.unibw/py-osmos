import numpy as np

def time_period_to_sma(T, MU):
    sma = np.power(MU * T**2 / (4 * np.pi**2), 1.0 / 3.0)
    return sma

def sma_to_time_period(sma, MU):
    P = 2*np.pi*np.sqrt(sma**3/MU)
    return P

def raan_rate_j2(a, e, i, R, MU, J2):
    p = a*(1 - e**2)
    j2k = 1.5*J2*(R/p)**2
    n = 2*np.pi/sma_to_time_period(a, MU)
    return -j2k*n*np.cos(i)

def perigee_rate_j2(a, e, i, R, MU, J2):
    p = a*(1 - e**2)
    j2k = 1.5*J2*(R/p)**2
    n = 2*np.pi/sma_to_time_period(a, MU)
    return 0.5*j2k*n*(4 - 5*np.sin(i)**2)

def mean_anomaly_rate_j2(a, e, i, R, MU, J2):
    p = a*(1 - e**2)
    j2k = 1.5*J2*(R/p)**2
    n = 2*np.pi/sma_to_time_period(a, MU)
    return 0.5*j2k*np.sqrt(1 - e**2)*(2 - 3*np.sin(i)**2)  

def UTC2JD(UTC):
    """
    calculates the Julian Day given UTC
    parameters: UTC - dateUTC.dateUTC
    return : Julian day i.e., days from 4713 BC
    """
    JD = 367 * UTC.year - int(7 * (UTC.year + int(
        (UTC.month + 9) / 12)) / 4) + int(
            275 * UTC.month / 9) + UTC.day + 1721013.5 + (
                (UTC.second / 60.0 + UTC.minute) / 60.0 + UTC.hour) / 24.0
    return JD


def JDC2GMST(t_cent):
    """
    return: Greenwich Mean Sidereal Time given the centuries of the Julian Day of the UTC
    values returned in radians
    """
    theta_gmst = 67310.54841 + (876600 * 3600 +
                                8640184.812866) * t_cent + 0.093104 * (
                                    t_cent**2) - 6.2 * 1e-6 * (t_cent**3)
    theta_gmst = theta_gmst % 86400 - 86400
    theta_gmst_deg = theta_gmst / 240
    if theta_gmst < 0:
        theta_gmst_deg = 360 + theta_gmst_deg
    return np.deg2rad(theta_gmst_deg)


def get_julian_centuries(JD):
    return (JD - 2451545) / 36525

def mean_anomaly_2_true_anomaly(M,e):
    """
     output: true anomaly in radians.
     input: mean anomaly in radians.
    """
    E=M+e*np.sin(M)+e**2/2*np.sin(2*M)
    n=np.arctan2(np.sin(E)*np.sqrt(1-e**2),(np.cos(E)-e))
    return wrapto2pi(n)

def true_anomaly_2_mean_anomaly(n,e):
    """
     input: true anomaly in radians.
     output: mean anomaly in radians.
    """
    M=n-2*e*np.sin(n)+(3*e**2/4+e**4/8)*np.sin(2*n)-(e**3/3+e**5/8)*np.sin(3*n)+(5*e**4/32)*np.sin(4*n)
    return wrapto2pi(M)

def mean2osc(a,e,i,w,M,raan,R, MU, J2):
    """
        Converts the mean elements to osculating elements. Only J2 force model. Ref. VALLADO
        inputs: a[km],e,i[rad],w[rad],M[rad],raan[rad],R[km], MU, J2
    """
    n=mean_anomaly_2_true_anomaly(M,e)     
    u=wraptopi(w+n)

    p=a*(1-e**2)
    i_sp = J2*R**2*np.sin(i)*np.cos(i)/(4*p**2)*(3*np.cos(2*u)+3*e*np.cos(2*w+n)+e*np.cos(2*w+3*n))
    p_sp = J2*R**2*np.sin(i)**2/(2*p)*(3*np.cos(2*u)+3*e*np.cos(2*w+n)+e*np.cos(2*w+3*n))
    raan_sp = -J2*R**2*np.cos(i)/(4*p**2)/(6*(n-M+e*np.sin(n))-3*np.sin(2*u)-3*e*np.sin(2*w+n)-e*np.sin(2*w+3*n))
    r_sp = -J2*R**2/(4*p)*((3*np.cos(i)**2-1)*((2*np.sqrt(1-e**2))/(1+e*np.cos(n))+(e*np.cos(n)/(1+np.sqrt(1-e**2))))-np.sin(i)**2*np.cos(2*u))
    r_dot_sp = J2*np.sqrt(MU)*R**2/(4*p**(5/2))*((3*np.cos(i)**2-1)*e*np.sin(n)*(np.sqrt(1-e**2)+((1+e*np.cos(n))**2)/(1+np.sqrt(1-e**2)))-2*np.sin(i)**2*(1-e*np.cos(n)**2)*np.sin(2*u))
    u_sp = (J2*R**2)/(8*p**2)*((6-30*np.cos(i)**2)*(n-M)+4*e*np.sin(n)*((1-6*np.cos(i)**2)+(1-3*np.cos(i)**2)/(1+np.sqrt(1-e**2)))+\
           + (1-3*np.cos(i)**2)/(1+np.sqrt(1-e**2))*e**2*np.sin(2*n)+(5*np.cos(i)-2)*2*e*np.sin(n+2*w)+\
           + (7*np.cos(i)**2-1)*np.sin(2*u)+2*np.cos(i)**2*e*np.sin(3*n+2*w)) 
    r = p/(1+e*np.cos(n))
    r_dot = np.sqrt(MU/p)*e*np.sin(n)
    r_osc = r + r_sp
    r_dot_osc = r_dot + r_dot_sp
    p_osc = p + p_sp
    A = p_osc/r_osc - 1
    B = r_dot_osc*np.sqrt(p_osc/MU)
    e_osc = np.sqrt(A**2+B**2)
    a_osc = p_osc/(1-e_osc**2)
    i_osc = i + i_sp
    raan_osc = raan + raan_sp
    u_osc = u + u_sp
    n_osc = np.arctan2(B,A)
    w_osc = u_osc - n_osc
    #M_osc = M + M_sp

    M_osc = true_anomaly_2_mean_anomaly(n_osc,e_osc)

    return a_osc, e_osc, i_osc, w_osc, M_osc, raan_osc


def wraptopi(angle):
    """  Returns the angle (in radians) between -pi and +pi that corresponds to the input angle
    """
    x = np.fmod(angle + np.pi, 2*np.pi)
    if x < 0 :
        x = x +2*np.pi
    return x - np.pi

def wrapto2pi(angle):
    """  Returns the angle (in radians) between -pi and +pi that corresponds to the input angle
    """
    x = np.fmod(angle, 2*np.pi)
    if x < 0 :
        x = x +2*np.pi
    return x  


def true_anomaly_2_eccentric_anomaly(e, v):
    """Converts true anomaly to eccentric anomaly

    Parameters
    ----------
    e : float
        eccentricity
    v : float
        true anomaly in radians

    Returns
    -------
    E : float
        eccentric anomaly in radians
    """
    assert e < 1.0
    sin_E = np.sin(v) * np.sqrt(1 - e**2)/(1 + e * np.cos(v))
    cos_E = (e + np.cos(v))/(1 + e * np.cos(v))
    E = np.arctan2(sin_E, cos_E)
    return wrapto2pi(E)


def eccentric_anomaly_2_true_anomaly(e, E):
    """Converts eccentric anomaly to true anomaly

    Parameters
    ----------
    e : float
        eccentricity
    E : float
        eccentric anomaly in radians

    Returns
    -------
    v : float
        true anomaly in radians
    """
    assert e < 1.0
    sin_v = np.sin(E) * np.sqrt(1 - e**2)/(1 - e*np.cos(E))
    cos_v = (np.cos(E) - e)/(1 - e*np.cos(E))
    v = np.arctan2(sin_v, cos_v)
    return wrapto2pi(v)

def mean_anomaly_2_eccentric_anomaly(e, M):
    v = mean_anomaly_2_true_anomaly(M, e)
    E = true_anomaly_2_eccentric_anomaly(e, v)
    return E

def eccentric_anomaly_2_mean_anomaly(e, E):
    v = eccentric_anomaly_2_true_anomaly(e, E)
    M = true_anomaly_2_mean_anomaly(v, e)
    return M

