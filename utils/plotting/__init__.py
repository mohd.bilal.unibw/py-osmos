from config import TIME_FORMAT, SCREEN_H, SCREEN_W, SCREEN_DPI
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import cartopy.crs as ccrs
import cartopy.io.img_tiles as cimgt
import numpy as np
import datetime
import logging
import abc

XDATE_FORMAT = '%d-%b %H:%M'
logger = logging.getLogger(__name__)

class IPlot(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def create(self, **kwargs):
        pass


class BasePlot(IPlot):
    def __init__(self, data, **kwargs):
        super().__init__()
        self.data = data

        # extract some basic scenario data
        self.sim_start_time = self.data.get("scenario").get("start")
        self.sim_end_time = self.data.get("scenario").get("end")
        self.sim_duration = (self.sim_end_time -
                             self.sim_start_time).total_seconds()

        # figure settings
        self.figure = None
        self.figure_name = kwargs.get("figure_name") or ""
        self.figures = []

        # axes settings
        self.axes = []

        # style settings
        self.theme = kwargs.get("theme") or "light"
        self.theme_colors = {
            'light': {
                'bg': 'whitesmoke',
                'plot': ['royalblue']
            },
            'dark': {
                'bg': 'black',
                'plot': ['turquoise']
            }
        }

    def get_figure(self):
        return self.figure

    def get_axes(self):
        return self.axes

    def append_axes(self, axes):
        self.axes.append(axes)

    def maximize_figures(self):
        for f in self.figures:
            if f is not None:
                mng = f.canvas.manager
                mng.window.showMaximized()

    def create(self):
        raise NotImplementedError(
            "create method was not implemented by subclass.")


class AccessBasePlot(BasePlot):
    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)
        self.scenario = self.data["scenario"]
        self.satellite = self.data["satellite"]
        self.station = self.data["station"]
        self.access = self.data["access"]


class AccessBarPlot(AccessBasePlot):
    def __init__(self, data, plot_duration=None, **kwargs):
        super().__init__(data, **kwargs)
        self.plot_duration = plot_duration or self.sim_duration
        self.figure_name = f"Access Bar Plot {self.satellite.get('name')} - {self.station.get('name')}"
        self.figure = plt.figure(self.figure_name,
                                 facecolor=self.theme_colors.get(
                                     self.theme).get('bg'))

    def create(self):
        # get end time for plot based on duration
        plot_end_time = self.sim_start_time + datetime.timedelta(
            seconds=self.plot_duration)

        # get accesses within plot end time
        valid_accesses = [
            a for a in self.access if a.get("start") <= plot_end_time
        ]

        if not valid_accesses:
            logger.info(f"No valid accesses found to plot.")
            return None

        ## plotting starts here
        ax = self.figure.add_subplot(111)

        # make a horizontal broken bar: start points are seconds from sim start
        # duration of each bar is the duration of overpass
        bar_data = [((a.get("start") - self.sim_start_time).total_seconds(),
                     a.get("duration")) for a in valid_accesses]

        # plot the bar at y position 10 and of height 2
        ax.broken_barh(bar_data, (10, 2),
                       fc=self.theme_colors.get(self.theme).get('plot'))

        # compute datetime labels
        xticks_dt = np.array([a.get("start") for a in valid_accesses])
        xticks = [(xt - self.sim_start_time).total_seconds()
                  for xt in xticks_dt]

        # add labels
        xticklabels = [t.strftime('%d-%b %H:%M') for t in xticks_dt]

        # add text mentioning duration is below
        ax.text(1, 7, 'duration in minutes: ', fontsize='small')
        ax.plot([0, (plot_end_time - self.sim_start_time).total_seconds()],
                [6.75, 6.75],
                linewidth=1,
                linestyle='--',
                color='black')

        # duration appears as annotation to avoid overcrowding of data
        [
            ax.annotate(
                f'{a.get("duration")/60:0.2f}',
                ((a.get("start") - self.sim_start_time).total_seconds(), 5),
                fontsize='small',
                rotation='vertical') for a in valid_accesses
        ]

        # add text mentioning year
        ax.text(1, 3.25, f'year:', fontsize='small')
        ax.plot([0, (plot_end_time - self.sim_start_time).total_seconds()],
                [3, 3],
                linewidth=1,
                linestyle='--',
                color='black')
        [
            ax.annotate(
                f'{a.get("start").year}',
                ((a.get("start") - self.sim_start_time).total_seconds(), 1.25),
                fontsize='small',
                rotation='vertical') for a in valid_accesses
        ]
        # cutoff y axis
        ax.set_ylim(0, 15)

        # format x and y ticks
        ax.tick_params('x', direction='in', labelsize='small')
        ax.tick_params(axis='y',
                       which='both',
                       left=False,
                       right=False,
                       labelleft=False,
                       labelright=False)
        ax.set_xticks(xticks)

        # rotate datetime labels
        ax.set_xticklabels(xticklabels,
                           rotation=60,
                           horizontalalignment='right')

        # title
        ax.set_title(
            f"Access Slot Plot of {self.satellite.get('name')} | {self.station.get('name')}"
        )

        # resize the window to max window
        self.figure.set_size_inches(SCREEN_W/SCREEN_DPI, SCREEN_H/SCREEN_DPI)
        # fig_manager = self.figure.canvas.manager
        # fig_manager.window.showMaximized()
        self.append_axes(ax)


class AccessPolarPlot(AccessBasePlot):
    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)


class AccessElevationPlot(AccessBasePlot):
    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)


class AccessHistogramPlot(AccessBasePlot):
    def __init__(self, data, plot_duration=None, **kwargs):
        super().__init__(data, **kwargs)
        self.plot_duration = plot_duration or self.sim_duration
        self.figure_name = f"Access Histogram Plot {self.satellite.get('name')} - {self.station.get('name')}"
        self.figure = plt.figure(self.figure_name,
                                 facecolor=self.theme_colors.get(
                                     self.theme).get('bg'))

    def create(self):
        # get end time for plot based on duration
        plot_end_time = self.sim_start_time + datetime.timedelta(
            seconds=self.plot_duration)

        # get accesses within plot end time
        valid_accesses = [
            a for a in self.access if a.get("start") <= plot_end_time
        ]

        if not valid_accesses:
            logger.info(f"No valid accesses found to plot.")
            return None

        ## plotting starts here
        ax = self.figure.add_subplot(111)

        x_data = [(a.get("start") - self.sim_start_time).total_seconds()
                  for a in valid_accesses]
        heights = [a.get("duration") / 60 for a in valid_accesses]
        width = 2000
        ax.bar(x_data,
               heights,
               width,
               align='edge',
               color=self.theme_colors.get(self.theme).get('plot'))

        # compute datetime ticks
        xticks_dt = np.array([a.get("start") for a in valid_accesses])
        xticks = [(xt - self.sim_start_time).total_seconds()
                  for xt in xticks_dt]

        yticks = np.arange(0, np.max(heights) + 0.5, 0.5)

        # add labels
        xticklabels = [t.strftime('%d-%b %H:%M') for t in xticks_dt]

        # format x and y ticks
        ax.tick_params('x', direction='in', labelsize='small')
        ax.set_xticks(xticks)
        ax.set_yticks(yticks)

        # rotate datetime labels
        ax.set_xticklabels(xticklabels,
                           rotation=60,
                           horizontalalignment='right')

        # title
        ax.set_title(
            f"Access Histogram Plot of {self.satellite.get('name')} | {self.station.get('name')}"
        )
        ax.set_ylabel("Access duration [min]")

        # resize the window to max window
        self.figure.set_size_inches(SCREEN_W/SCREEN_DPI, SCREEN_H/SCREEN_DPI)
        # fig_manager = self.figure.canvas.manager
        # fig_manager.window.showMaximized()
        self.append_axes(ax)


class GroundTrackMapPlot(BasePlot):
    def __init__(self,
                 data,
                 show_sensors_footprints=False,
                 projection="flat",
                 **kwargs):
        super().__init__(data, **kwargs)
        self.projection = projection

        self.show_sensors_footprints = show_sensors_footprints
        self.central_longitude = kwargs.get("central_longitude") or 0
        self.scenario = self.data["scenario"]
        self.satellite = self.data["satellite"]
        self.ground_track = self.data["ground_track"]
        self.figure_name = f"Ground Track Flat {self.satellite.get('name')}"
        self.figure = plt.figure(self.figure_name)

    def create(self):
        projection = ccrs.PlateCarree()
        if self.projection.lower() == "polar_north":
            projection = ccrs.NorthPolarStereo(
                central_longitude=self.central_longitude)

        ax = self.figure.add_subplot(111, projection=projection)
        ax.stock_img()
        latitudes = [g.get("latitude") for g in self.ground_track]
        longitudes = [g.get("longitude") for g in self.ground_track]
        ax.plot(longitudes,
                latitudes,
                transform=ccrs.Geodetic(),
                alpha=0.6,
                color='green',
                zorder=3,
                linewidth=1)
        ax.plot(longitudes[0],
                latitudes[0],
                transform=ccrs.Geodetic(),
                marker="*",
                color="orange")
        ax.plot(longitudes[-1],
                latitudes[-1],
                transform=ccrs.Geodetic(),
                marker="*",
                color="blue")
        ax.set_title(f"Groundtrack of {self.satellite.get('name')}")

        self.figure.legend(['groundtrack', 'start', 'end'])
        gl = ax.gridlines(crs=ccrs.PlateCarree(),
                          draw_labels=True,
                          linewidth=1,
                          color='gray',
                          alpha=0.5,
                          linestyle='--')

        # resize the window to max window
        self.figure.set_size_inches(SCREEN_W/SCREEN_DPI, SCREEN_H/SCREEN_DPI)

        # fig_manager = self.figure.canvas.manager
        # fig_manager.window.showMaximized()
        self.append_axes(ax)

    def add_location(self, latitude, longitude, name, marker="o", color='red'):
        ax = self.get_axes()[0]
        l = ax.plot(longitude,
                    latitude,
                    transform=ccrs.Geodetic(),
                    marker=marker,
                    color=color,
                    label=name,
                    markersize=2)
        self.figure.legend(loc="upper left")


class GroundTrackPolarPlot(GroundTrackMapPlot):
    def __init__(self, data, show_sensors_footprints=False, **kwargs):
        super().__init__(data,
                         show_sensors_footprints=show_sensors_footprints,
                         projection="polar_north",
                         **kwargs)
        self.figure_name = f"Ground Track Polar {self.satellite.get('name')}"
        self.figure = plt.figure(self.figure_name)

    def create(self):
        super().create()


class SensorCoverageMapPlot(GroundTrackMapPlot):
    def __init__(self, data, filled=False, **kwargs):
        super().__init__(data,
                         show_sensors_footprints=True,
                         projection="flat",
                         **kwargs)
        self.sensor_data = self.data["sensor"]
        self.coverage = self.data["coverage"]
        self.filled = filled
        self.figure_name = f"Sensor Coverage Plot {self.satellite.get('name')}"
        self.figure = plt.figure(self.figure_name)

    def create(self):
        super().create()
        ax = self.get_axes()[0]
        for footprint in self.coverage:
            lats = []
            longs = []
            for point in footprint:
                lats.append(point["latitude"])
                longs.append(point["longitude"])
            if self.filled:
                ax.fill(longs,
                        lats,
                        transform=ccrs.Geodetic(),
                        alpha=0.2,
                        facecolor='plum',
                        edgecolor='magenta',
                        zorder=3,
                        linewidth=0.5)
            else:
                ax.plot(longs,
                        lats,
                        transform=ccrs.Geodetic(),
                        alpha=0.8,
                        color='magenta',
                        zorder=3,
                        linewidth=0.5)
        ax.set_title(f"Coverage of Sensor {self.sensor_data['name']}")


class EclipseBarPlot(BasePlot):
    def __init__(self, data, plot_duration=None, **kwargs):
        super().__init__(data, **kwargs)
        self.scenario = self.data["scenario"]
        self.satellite = self.data["satellite"]
        self.eclipse = self.data["eclipse"]
        self.plot_duration = plot_duration or self.sim_duration
        self.figure_name = f"Eclipse Duration Bar Plot {self.satellite.get('name')}"
        self.figure = plt.figure(self.figure_name,
                                 facecolor=self.theme_colors.get(
                                     self.theme).get('bg'))

    def create(self):
        # get end time for plot based on duration
        plot_end_time = self.sim_start_time + datetime.timedelta(
            seconds=self.plot_duration)

        # get eclipsees within plot end time
        valid_eclipses = [
            a for a in self.eclipse if a.get("start") <= plot_end_time
        ]

        if not valid_eclipses:
            logger.info(f"No valid eclipses found to plot.")
            return None

        ## plotting starts here
        ax = self.figure.add_subplot(111)

        # make a horizontal broken bar: start points are seconds from sim start
        # duration of each bar is the duration of overpass
        bar_data = [((a.get("start") - self.sim_start_time).total_seconds(),
                     a.get("duration")) for a in valid_eclipses]

        # plot the bar at y position 10 and of height 2
        ax.broken_barh(bar_data, (10, 2),
                       fc=self.theme_colors.get(self.theme).get('plot'),
                       label="In Eclipse")
        self.figure.legend()
        # compute datetime labels
        xticks_dt = np.array([a.get("start") for a in valid_eclipses])
        xticks = [(xt - self.sim_start_time).total_seconds()
                  for xt in xticks_dt]

        # add labels
        xticklabels = [t.strftime('%d-%b %H:%M') for t in xticks_dt]

        # add text mentioning duration is below
        ax.text(1, 7, 'duration in minutes: ', fontsize='small')
        ax.plot([0, (plot_end_time - self.sim_start_time).total_seconds()],
                [6.75, 6.75],
                linewidth=1,
                linestyle='--',
                color='black')

        # duration appears as annotation to avoid overcrowding of data
        [
            ax.annotate(
                f'{a.get("duration")/60:0.2f}',
                ((a.get("start") - self.sim_start_time).total_seconds(), 5),
                fontsize='small',
                rotation='vertical') for a in valid_eclipses
        ]

        # add text mentioning year
        ax.text(1, 3.25, f'year:', fontsize='small')
        ax.plot([0, (plot_end_time - self.sim_start_time).total_seconds()],
                [3, 3],
                linewidth=1,
                linestyle='--',
                color='black')
        [
            ax.annotate(
                f'{a.get("start").year}',
                ((a.get("start") - self.sim_start_time).total_seconds(), 1.25),
                fontsize='small',
                rotation='vertical') for a in valid_eclipses
        ]
        # cutoff y axis
        ax.set_ylim(0, 15)

        # format x and y ticks
        ax.tick_params('x', direction='in', labelsize='small')
        ax.tick_params(axis='y',
                       which='both',
                       left=False,
                       right=False,
                       labelleft=False,
                       labelright=False)
        ax.set_xticks(xticks)

        # rotate datetime labels
        ax.set_xticklabels(xticklabels,
                           rotation=60,
                           horizontalalignment='right')

        # title
        ax.set_title(
            f"Eclipse Duration Bar Plot of {self.satellite.get('name')}")
        # resize the window to max window
        self.figure.set_size_inches(SCREEN_W/SCREEN_DPI, SCREEN_H/SCREEN_DPI)
        # fig_manager = self.figure.canvas.manager
        # fig_manager.window.showMaximized()
        self.append_axes(ax)


class EclipseHistogramPlot(BasePlot):
    def __init__(self, data, plot_duration=None, **kwargs):
        super().__init__(data, **kwargs)
        self.scenario = self.data["scenario"]
        self.satellite = self.data["satellite"]
        self.eclipse = self.data["eclipse"]
        self.plot_duration = plot_duration or self.sim_duration
        self.figure_name = f"Eclipse Duration Histogram Plot {self.satellite.get('name')}"
        self.figure = plt.figure(self.figure_name,
                                 facecolor=self.theme_colors.get(
                                     self.theme).get('bg'))

    def create(self):
        # get end time for plot based on duration
        plot_end_time = self.sim_start_time + datetime.timedelta(
            seconds=self.plot_duration)

        # get eclipses within plot end time
        valid_eclipses = [
            a for a in self.eclipse if a.get("start") <= plot_end_time
        ]

        if not valid_eclipses:
            logger.info(f"No valid eclipses found to plot.")
            return None
            
        ## plotting starts here
        ax = self.figure.add_subplot(111)

        x_data = [(a.get("start") - self.sim_start_time).total_seconds()
                  for a in valid_eclipses]
        heights = [a.get("duration") / 60.0 for a in valid_eclipses]
        widths = np.array(heights) * 60
        ax.bar(x_data,
               heights,
               widths,
               align='edge',
               color=self.theme_colors.get(self.theme).get('plot'))

        # compute datetime ticks
        xticks_dt = np.array([a.get("start") for a in valid_eclipses])
        xticks = [(xt - self.sim_start_time).total_seconds()
                  for xt in xticks_dt]

        yticks = np.arange(0, np.max(heights) + 2, 2)

        # add labels
        xticklabels = [t.strftime('%d-%b %H:%M') for t in xticks_dt]

        # format x and y ticks
        ax.tick_params('x', direction='in', labelsize='small')
        ax.set_xticks(xticks)
        ax.set_yticks(yticks)

        # rotate datetime labels
        ax.set_xticklabels(xticklabels,
                           rotation=60,
                           horizontalalignment='right')

        # title
        ax.set_title(f"Eclipse Histogram Plot of {self.satellite.get('name')}")
        ax.set_ylabel("Eclipse duration [min]")

        # resize the window to max window
        self.figure.set_size_inches(SCREEN_W/SCREEN_DPI, SCREEN_H/SCREEN_DPI)
        # fig_manager = self.figure.canvas.manager
        # fig_manager.window.showMaximized()
        self.append_axes(ax)


class PowerTimeSeriesPlot(BasePlot):
    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)


class OrbitParametersPlot(BasePlot):
    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)
        self.scenario = self.data["scenario"]
        self.satellite = self.data["satellite"]
        self.ephemeris = self.data["ephemeris"]
        self.figure_name_aei = f"Ephemeris of {self.satellite.get('name')}: sma, eccentricity, inclination"
        self.figure_name_rpn = f"Ephemeris of {self.satellite.get('name')}: raan, argument perigee, mean anomaly"

        self.figure_aei = plt.figure(self.figure_name_aei,
                                     facecolor=self.theme_colors.get(
                                         self.theme).get('bg'))
        self.figure_rpn = plt.figure(self.figure_name_rpn,
                                     facecolor=self.theme_colors.get(
                                         self.theme).get('bg'))

    def create(self):
        t = self.ephemeris.get("epoch")
        figures = []
        # plot semi-major axis
        ax_a = self.figure_aei.add_subplot(311)
        ax_a.plot(self.ephemeris.get("epoch"),
                  np.array(self.ephemeris.get("sma")) / 1e3,
                  color=self.theme_colors.get(self.theme).get('plot')[0])
        ax_a.set_ylabel("Semi-major Axis [km]", fontsize='small')
        ax_a.grid(visible=True, which='both')
        ax_a.tick_params('x', labelbottom=False, labelsize='small')
        ax_a.tick_params('y', labelsize='small')
        self.append_axes(ax_a)

        # plot eccentricity
        ax_e = self.figure_aei.add_subplot(312, sharex=ax_a)
        ax_e.plot(self.ephemeris.get("epoch"),
                  self.ephemeris.get("eccentricity"),
                  color=self.theme_colors.get(self.theme).get('plot')[0])
        ax_e.set_ylabel("Eccentricity", fontsize='small')
        ax_e.grid(visible=True, which='both')
        ax_e.tick_params('x', labelbottom=False, labelsize='small')
        ax_e.tick_params('y', labelsize='small')
        self.append_axes(ax_e)

        # plot inclination
        ax_i = self.figure_aei.add_subplot(313, sharex=ax_a)
        ax_i.plot(self.ephemeris.get("epoch"),
                  np.rad2deg(np.array(self.ephemeris.get("inclination"))),
                  color=self.theme_colors.get(self.theme).get('plot')[0])
        ax_i.grid(visible=True, which='both')
        ax_i.set_ylabel(r"Inclination [$\degree$]", fontsize='small')
        ax_i.tick_params('x', labelsize='small')
        ax_i.tick_params('y', labelsize='small')
        ax_i.xaxis.set_major_formatter(mdates.DateFormatter(XDATE_FORMAT))
        [
            l.set(rotation=30, horizontalalignment='right')
            for l in ax_i.get_xticklabels(which='major')
        ]
        self.append_axes(ax_i)
        self.figure_aei.suptitle(f"Ephemeris of {self.satellite.get('name')}")
        self.figure_aei.set_size_inches(SCREEN_W/SCREEN_DPI, SCREEN_H/SCREEN_DPI)
       
        # fig_manager_aei = self.figure_aei.canvas.manager
        # fig_manager_aei.window.showMaximized()

        # plot raan
        from utils.astro.helpers import wrapto2pi
        ax_raan = self.figure_rpn.add_subplot(411)
        from utils.astro.helpers import wrapto2pi
        ax_raan.plot(self.ephemeris.get("epoch"),
                     np.rad2deg(np.array(list(map(wrapto2pi, self.ephemeris.get("raan"))))),
                     color=self.theme_colors.get(self.theme).get('plot')[0])
        ax_raan.grid(visible=True, which='both')
        ax_raan.set_ylabel(r'$\Omega$ [$\degree$]', fontsize='small')
        ax_raan.tick_params('x', labelbottom=False, labelsize='small')
        ax_raan.tick_params('y', labelsize='small')

        self.append_axes(ax_raan)

        # plot argument of perigee
        ax_arg_perigee = self.figure_rpn.add_subplot(412, sharex=ax_raan)
        ax_arg_perigee.plot(
            self.ephemeris.get("epoch"),
            np.rad2deg(np.array(self.ephemeris.get("perigee"))),
            color=self.theme_colors.get(self.theme).get('plot')[0])
        ax_arg_perigee.grid(visible=True, which='both')
        ax_arg_perigee.set_ylabel(r"$\omega$ [$\degree$]", fontsize='small')
        ax_arg_perigee.tick_params('x', labelbottom=False, labelsize='small')
        ax_arg_perigee.tick_params('y', labelsize='small')
        self.append_axes(ax_arg_perigee)

        # plot mean anomaly
        ax_ma = self.figure_rpn.add_subplot(413, sharex=ax_raan)
        ax_ma.plot(self.ephemeris.get("epoch"),
                   np.rad2deg(np.array(self.ephemeris.get("mean_anomaly"))),
                   color=self.theme_colors.get(self.theme).get('plot')[0])
        ax_ma.grid(visible=True, which='both')
        ax_ma.set_ylabel(r"M [$\degree$]", fontsize='small')
        ax_ma.tick_params('x', labelbottom=False, labelsize='small')
        ax_ma.tick_params('y', labelsize='small')
        self.append_axes(ax_ma)
        self.figure_rpn.suptitle(f"Ephemeris of {self.satellite.get('name')}")

        symbols = (f"Symbols \n"
                   "------------- \n"
                   r"$\Omega$: RAAN"
                   "\n"
                   r"$\omega$: Argument of Perigee"
                   "\n"
                   r"M: Mean Anomaly"
                   "\n"
                   r"$\lambda$: Argument of Latitude")
        self.figure_rpn.text(0.81,
                             0.94,
                             symbols,
                             ha='left',
                             va='center',
                             transform=self.figure_rpn.transFigure,
                             fontsize="x-small",
                             bbox=dict(boxstyle="round",
                                       fc="w",
                                       ec="0.5",
                                       lw=1))

        # plot argument of latitude
        aol = np.rad2deg(
            np.array(self.ephemeris.get("mean_anomaly")) +
            np.array(self.ephemeris.get("perigee")))
        ax_aol = self.figure_rpn.add_subplot(414, sharex=ax_raan)
        ax_aol.plot(self.ephemeris.get("epoch"),
                    aol,
                    color=self.theme_colors.get(self.theme).get('plot')[0])
        ax_aol.set_ylabel(r"$\lambda$ [$\degree$]", fontsize='small')
        ax_aol.tick_params('x', labelsize='small')
        ax_aol.tick_params('y', labelsize='small')
        ax_aol.grid(visible=True, which='both')
        ax_aol.xaxis.set_major_formatter(mdates.DateFormatter(XDATE_FORMAT))
        [
            l.set(rotation=30, horizontalalignment='right')
            for l in ax_aol.get_xticklabels(which='major')
        ]

        # resize the window to max window
        self.figure_rpn.set_size_inches(SCREEN_W/SCREEN_DPI, SCREEN_H/SCREEN_DPI)
        # fig_mng_rpn = self.figure_rpn.canvas.manager
        # fig_mng_rpn.window.showMaximized()


class LTANPlot(BasePlot):
    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)


class Orbit3DPlot(BasePlot):
    def __init__(self, data, **kwargs):
        super().__init__(data, **kwargs)
