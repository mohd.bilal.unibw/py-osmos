from utils.plotting import AccessBarPlot, AccessHistogramPlot
from utils.plotting import EclipseBarPlot, EclipseHistogramPlot
from utils.plotting import GroundTrackMapPlot, GroundTrackPolarPlot
from utils.plotting import SensorCoverageMapPlot
from utils.plotting import OrbitParametersPlot
from config import TIME_FORMAT
from utils.astro.helpers import sma_to_time_period
from utils.astro.constants import MU_EARTH
import numpy as np
import abc
import json
import datetime
import logging
import enum

logger = logging.getLogger(__name__)


class ReportTypes(enum.Enum):
    GS_ACCESS = 0
    ECLIPSE = 1
    SENSOR_COVERAGE = 2
    GROUND_TRACK = 3
    POWER = 4
    EPHEMERIS = 5


class IReport(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def export(self, fname="", fpath="", fmt="json"):
        pass

    @abc.abstractmethod
    def update(self, data, **kwargs):
        pass

    @abc.abstractmethod
    def to_json(self, **kwargs):
        pass


class BaseReport(IReport):
    def __init__(self, type_, name, scenario_info, report_data=None, **kwargs):
        self.type_ = type_
        self.name = name
        self.scenario_info = scenario_info
        self.report_data = report_data
        self.scenario_name = self.scenario_info['name']
        self.start_time = kwargs.get(
            "start_time") or self.scenario_info['start']
        self.end_time = kwargs.get("end_time") or self.scenario_info['end']
        self.show_plots = kwargs.get("show_plots") or True

    def get_scenario_info(self):
        return self.scenario_info

    def get_scenario_name(self):
        return self.scenario_info['name']

    def get_scenario_start(self):
        return self.scenario_info['start']

    def get_scenario_end(self):
        return self.scenario_info['end']

    def get_report_data(self):
        return self.report_data

    def set_report_data(self, data):
        self.report_data = data


class AccessReport(BaseReport):
    def __init__(self,
                 scenario_info,
                 satellite_data,
                 station_data,
                 report_data=None,
                 **kwargs):
        self.satellite_data = satellite_data
        self.station_data = station_data
        self.access_starts = self.report_data.get(
            "start").copy() if report_data else []
        self.access_ends = self.report_data.get(
            "end").copy() if report_data else []
        self.report_data = report_data if report_data is not None else {
            "start": self.access_starts,
            "end": self.access_ends
        }
        self.accesses = []
        self.bar_plot = None
        self.histogram_plot = None
        self.elevation_plot = None

        name = f"{ReportTypes.GS_ACCESS.name} Report {self.get_satellite_name()} - {self.get_station_name()}"
        super().__init__(type_=ReportTypes.GS_ACCESS,
                         name=name,
                         scenario_info=scenario_info,
                         report_data=self.report_data,
                         **kwargs)

    def get_satellite_data(self):
        return self.satellite_data

    def get_satellite_name(self):
        return self.satellite_data['name']

    def get_satellite_epoch_data(self):
        return self.get_satellite_data()['initial_conditions']

    def get_station_data(self):
        return self.station_data

    def get_station_name(self):
        return self.station_data['name']

    def get_station_location(self):
        return (self.station_data['latitude'], self.station_data['longitude'])

    def get_accesses(self, start=None, end=None):
        self._extract_accesses()
        if start is None and end is not None:
            start = self.start_time
        elif start is not None and end is None:
            end = self.end_time
        else:
            return self.accesses
        return [
            a for a in self.accesses
            if a.get("start") >= start and a.get("end") <= end
        ]

    def _extract_accesses(self):
        duration = []
        if not self.access_starts or not self.access_ends:
            logger.info("Failed to extract access data. No data available")
            return None
        if self.access_ends[0] < self.access_starts[0]:
            self.access_starts.insert(0, self.get_scenario_start())
        self.accesses = [{
            "start": s,
            "end": e,
            "duration": (e - s).total_seconds()
        } for s, e in zip(self.access_starts, self.access_ends)]

    def export(self, fname="", fpath="", fmt="json"):
        js_data = self.to_json()
        fname = fname if fname else f"Scenario_{self.get_scenario_name()}_{self.name}.{fmt}"
        fpath = fpath if fpath else "simulations/reports"
        with open(fpath + "/" + fname, 'w') as f:
            json.dump(json.loads(js_data), f, indent=4)

    def update(self, data, **kwargs):
        if "start" in kwargs:
            self.access_starts.append(data)
        elif "end" in kwargs:
            self.access_ends.append(data)
        else:
            raise Exception("Need to provide either start or end parameter.")

    def to_json(self, **kwargs):
        self._extract_accesses()
        accesses = [{
            "start": a.get("start").strftime(TIME_FORMAT),
            "end": a.get("end").strftime(TIME_FORMAT),
            "duration": a.get("duration")
        } for a in self.accesses]
        scenario_info = self.scenario_info.copy()
        scenario_info["start"] = scenario_info["start"].strftime(TIME_FORMAT)
        scenario_info["end"] = scenario_info["end"].strftime(TIME_FORMAT)
        satellite_data = self.satellite_data.copy()
        satellite_data['initial_conditions']['epoch'] = satellite_data[
            'initial_conditions']['epoch'].strftime(TIME_FORMAT)

        if not accesses:
            logger.info("No accesses were found during json conversion.")
        j = {
            "scenario": scenario_info,
            "satellite": satellite_data,
            "station": self.station_data.copy(),
            "access": accesses
        }
        return json.dumps(j)

    def to_dict(self, **kwargs):
        self._extract_accesses()
        accesses = [{
            "start": a.get("start"),
            "end": a.get("end"),
            "duration": a.get("duration")
        } for a in self.accesses]
        if not accesses:
            logger.info("No accesses were found during json conversion.")
        j = {
            "scenario": self.scenario_info,
            "satellite": self.satellite_data,
            "station": self.station_data,
            "access": accesses
        }
        return j

    def compute_access_duration(self):
        self._extract_accesses()
        if not self.accesses:
            return 0
        total_duration = np.sum([a.get("duration") for a in self.accesses])
        return total_duration

    def compute_fractional_access(self):
        total_duration = self.compute_access_duration()
        return total_duration / (self.get_scenario_end() -
                                 self.get_scenario_start()).total_seconds()

    def compute_mean_access(self):
        self._extract_accesses()
        if not self.accesses:
            return 0
        mean_duration = np.mean([a.get("duration") for a in self.accesses])
        return mean_duration

    def plot_access_bar(self, plot_duration=None, **kwargs):
        data = self.to_dict()
        if self.show_plots and data['access']:
            self.bar_plot = AccessBarPlot(data,
                                          plot_duration=plot_duration,
                                          **kwargs)
            self.bar_plot.create()
        else:
            logger.info("set show_plots to True for plots to be plotted.")

    def plot_access_histogram(self, plot_duration=None, **kwargs):
        data = self.to_dict()
        if self.show_plots and data['access']:
            self.bar_plot = AccessHistogramPlot(data,
                                                plot_duration=plot_duration,
                                                **kwargs)
            self.bar_plot.create()
        else:
            logger.info("set show_plots to True for plots to be plotted.")

    def plot_all(self, plot_duration=None, **kwargs):
        self._extract_accesses()
        self.plot_access_bar(plot_duration=plot_duration, **kwargs)
        self.plot_access_histogram(plot_duration=plot_duration, **kwargs)


class EclipseReport(BaseReport):
    def __init__(self,
                 scenario_info,
                 satellite_data,
                 report_data=None,
                 **kwargs):
        self.satellite_data = satellite_data
        self.eclipse_starts = self.report_data.get(
            "start").copy() if report_data else []
        self.eclipse_ends = self.report_data.get(
            "end").copy() if report_data else []
        self.report_data = report_data if report_data is not None else {
            "start": self.eclipse_starts,
            "end": self.eclipse_ends
        }
        self.eclipses = []
        self.bar_plot = None
        self.histogram_plot = None

        name = f"{ReportTypes.ECLIPSE.name} Report {self.get_satellite_name()}"
        super().__init__(type_=ReportTypes.ECLIPSE,
                         name=name,
                         scenario_info=scenario_info,
                         report_data=self.report_data,
                         **kwargs)

    def get_satellite_data(self):
        return self.satellite_data

    def get_satellite_name(self):
        return self.satellite_data['name']

    def get_satellite_epoch_data(self):
        return self.get_satellite_data()['initial_conditions']

    def get_eclipses(self, start=None, end=None):
        self._extract_eclipses()
        if start is None and end is not None:
            start = self.start_time
        elif start is not None and end is None:
            end = self.end_time
        else:
            return self.eclipses
        return [
            a for a in self.eclipses
            if a.get("start") >= start and a.get("end") <= end
        ]

    def _extract_eclipses(self):
        duration = []
        if not self.eclipse_starts or not self.eclipse_ends:
            logger.info("Failed to extract eclipse data. No data available")
            return None
        if self.eclipse_ends[0] < self.eclipse_starts[0]:
            self.eclipse_starts.insert(0, self.get_scenario_start())
        self.eclipses = [{
            "start": s,
            "end": e,
            "duration": (e - s).total_seconds()
        } for s, e in zip(self.eclipse_starts, self.eclipse_ends)]

    def export(self, fname="", fpath="", fmt="json"):
        js_data = self.to_json()
        fname = fname if fname else f"Scenario_{self.get_scenario_name()}_{self.name}.{fmt}"
        fpath = fpath if fpath else "simulations/reports"
        with open(fpath + "/" + fname, 'w') as f:
            json.dump(json.loads(js_data), f, indent=4)

    def update(self, data, **kwargs):
        if "start" in kwargs:
            self.eclipse_starts.append(data)
        elif "end" in kwargs:
            self.eclipse_ends.append(data)
        else:
            raise Exception("Need to provide either start or end parameter.")

    def to_json(self, **kwargs):
        self._extract_eclipses()
        eclipses = [{
            "start": a.get("start").strftime(TIME_FORMAT),
            "end": a.get("end").strftime(TIME_FORMAT),
            "duration": a.get("duration")
        } for a in self.eclipses]
        scenario_info = self.scenario_info.copy()
        scenario_info["start"] = scenario_info["start"].strftime(TIME_FORMAT)
        scenario_info["end"] = scenario_info["end"].strftime(TIME_FORMAT)
        satellite_data = self.satellite_data.copy()
        satellite_data['initial_conditions']['epoch'] = satellite_data[
            'initial_conditions']['epoch'].strftime(TIME_FORMAT)

        if not eclipses:
            logger.info("No eclipses were found during json conversion.")
        j = {
            "scenario": scenario_info,
            "satellite": satellite_data,
            "eclipse": eclipses
        }
        return json.dumps(j)

    def to_dict(self, **kwargs):
        self._extract_eclipses()
        eclipses = [{
            "start": a.get("start"),
            "end": a.get("end"),
            "duration": a.get("duration")
        } for a in self.eclipses]
        if not eclipses:
            logger.info("No eclipses were found during json conversion.")
        j = {
            "scenario": self.scenario_info,
            "satellite": self.satellite_data,
            "eclipse": eclipses
        }
        return j

    def compute_eclipse_duration(self):
        self._extract_eclipses()
        if not self.eclipses:
            return 0
        total_duration = np.sum([a.get("duration") for a in self.eclipses])
        return total_duration

    def compute_mean_eclipse_duration(self):
        self._extract_eclipses()
        if not self.eclipses:
            return 0
        mean_duration = np.mean([a.get("duration") for a in self.eclipses])
        return mean_duration

    def compute_fraction_eclipse_duration(self):
        return self.compute_eclipse_duration() / (
            self.get_scenario_end() - self.get_scenario_start()).total_seconds()

    def compute_orbit_average_eclipse(self, MU=MU_EARTH):
        orbit_period = sma_to_time_period(
            self.get_satellite_epoch_data().get("sma"), MU)
        self._extract_eclipses()
        if not self.eclipses:
            return 0
        n_bins = int(
            (self.get_scenario_end() -
             self.get_scenario_start()).total_seconds() / orbit_period)
        end_times = [
            self.get_scenario_start() +
            datetime.timedelta(seconds=n * orbit_period)
            for n in range(0, n_bins)
        ]
        binned_eclipses = []
        for i in range(len(end_times)):
            group = []
            et = end_times[i]
            st = end_times[i - 1] if i else self.get_scenario_start()
            for e in self.eclipses:
                if e.get("start") >= st and e.get("end") <= et:
                    group.append(e)
            binned_eclipses.append(group)
        orbit_averages = [
            np.sum([e.get("duration") for e in be]) for be in binned_eclipses
        ]
        return orbit_averages

    def plot_eclipse_bar(self, plot_duration=None, **kwargs):
        data = self.to_dict()
        if self.show_plots:
            self.bar_plot = EclipseBarPlot(data,
                                           plot_duration=plot_duration,
                                           **kwargs)
            self.bar_plot.create()
        else:
            logger.info("set show_plots to True for plots to be plotted.")

    def plot_eclipse_histogram(self, plot_duration=None, **kwargs):
        data = self.to_dict()
        if self.show_plots:
            self.bar_plot = EclipseHistogramPlot(data,
                                                 plot_duration=plot_duration,
                                                 **kwargs)
            self.bar_plot.create()
        else:
            logger.info("set show_plots to True for plots to be plotted.")

    def plot_all(self, plot_duration=None, **kwargs):
        self._extract_eclipses()
        self.plot_eclipse_bar(plot_duration=plot_duration, **kwargs)
        self.plot_eclipse_histogram(plot_duration=plot_duration, **kwargs)


class GroundTrackReport(BaseReport):
    def __init__(self,
                 scenario_info,
                 satellite_data,
                 station_data=None,
                 report_data=None,
                 **kwargs):
        self.satellite_data = satellite_data
        self.station_data = station_data or []
        self.report_data = report_data if report_data is not None else []
        self.ground_track = self.report_data
        self.flat_plot = None
        self.polar_plot = None

        name = f"{ReportTypes.GROUND_TRACK.name} Report {self.get_satellite_name()}"
        super().__init__(type_=ReportTypes.GROUND_TRACK,
                         name=name,
                         scenario_info=scenario_info,
                         report_data=self.report_data,
                         **kwargs)

    def get_satellite_data(self):
        return self.satellite_data

    def get_satellite_name(self):
        return self.satellite_data['name']

    def get_satellite_epoch_data(self):
        return self.get_satellite_data()['initial_conditions']

    def get_ground_track(self):
        return self.ground_track

    def export(self, fname="", fpath="", fmt="json"):
        js_data = self.to_json()
        fname = fname if fname else f"Scenario_{self.get_scenario_name()}_{self.name}.{fmt}"
        fpath = fpath if fpath else "simulations/reports"
        with open(fpath + "/" + fname, 'w') as f:
            json.dump(json.loads(js_data), f, indent=4)

    def to_json(self, **kwargs):
        ground_track = self.ground_track.copy()
        for g in ground_track:
            g["time"] = g["time"].strftime(TIME_FORMAT)
        scenario_info = self.scenario_info.copy()
        scenario_info["start"] = scenario_info["start"].strftime(TIME_FORMAT)
        scenario_info["end"] = scenario_info["end"].strftime(TIME_FORMAT)
        satellite_data = self.satellite_data.copy()
        satellite_data['initial_conditions']['epoch'] = satellite_data[
            'initial_conditions']['epoch'].strftime(TIME_FORMAT)

        if not ground_track:
            logger.info(
                "No ground track information was found during json conversion."
            )
        j = {
            "scenario": scenario_info,
            "satellite": satellite_data,
            "station": self.station_data,
            "ground_track": ground_track
        }
        return json.dumps(j)

    def to_dict(self, **kwargs):
        j = {
            "scenario": self.scenario_info,
            "satellite": self.satellite_data,
            "station": self.station_data,
            "ground_track": self.ground_track
        }
        return j

    def update(self, data, **kwargs):
        if kwargs.get("replace"):
            self.report_data = data
            self.ground_track = self.report_data
        else:
            self.report_data.append(data)

    def plot_flat_map(self, show_locations=True):
        data = self.to_dict()
        self.flat_plot = GroundTrackMapPlot(data)
        self.flat_plot.create()
        if show_locations:
            for s in self.station_data:
                self.flat_plot.add_location(s["latitude"], s["longitude"],
                                            s["name"])
        return self.flat_plot

    def plot_polar_map(self, show_locations=True):
        data = self.to_dict()
        self.polar_plot = GroundTrackPolarPlot(data)
        self.polar_plot.create()
        if show_locations:
            for s in self.station_data:
                self.polar_plot.add_location(s["latitude"], s["longitude"],
                                             s["name"])
        return self.polar_plot


class SensorCoverageReport(BaseReport):
    def __init__(self,
                 scenario_info,
                 satellite_data,
                 sensor_data,
                 station_data=None,
                 ground_track_data=None,
                 report_data=None,
                 **kwargs):
        self.satellite_data = satellite_data
        self.station_data = station_data or []
        self.sensor_data = sensor_data
        self.report_data = report_data if report_data is not None else []
        self.ground_track = ground_track_data or []
        self.coverage = self.report_data

        name = f"{ReportTypes.SENSOR_COVERAGE.name} Report {self.get_satellite_name()} - {self.get_sensor_name()}"
        super().__init__(type_=ReportTypes.GROUND_TRACK,
                         name=name,
                         scenario_info=scenario_info,
                         report_data=self.report_data,
                         **kwargs)

    def get_satellite_data(self):
        return self.satellite_data

    def get_satellite_name(self):
        return self.satellite_data['name']

    def get_satellite_epoch_data(self):
        return self.get_satellite_data()['initial_conditions']

    def get_ground_track(self):
        return self.ground_track

    def get_sensor_name(self):
        return self.sensor_data['name']

    def get_sensor_data(self):
        return self.sensor_data

    def export(self, fname="", fpath="", fmt="json"):
        js_data = self.to_json()
        fname = fname if fname else f"Scenario_{self.get_scenario_name()}_{self.name}.{fmt}"
        fpath = fpath if fpath else "simulations/reports"
        with open(fpath + "/" + fname, 'w') as f:
            json.dump(json.loads(js_data), f, indent=4)

    def to_json(self, **kwargs):
        ground_track = self.ground_track.copy()
        coverage = self.coverage.copy()
        for g in ground_track:
            g["time"] = g["time"].strftime(TIME_FORMAT)

        for c in coverage:
            for p in c:
                p['time'] = p['time'].strftime(TIME_FORMAT)

        scenario_info = self.scenario_info.copy()
        scenario_info["start"] = scenario_info["start"].strftime(TIME_FORMAT)
        scenario_info["end"] = scenario_info["end"].strftime(TIME_FORMAT)
        satellite_data = self.satellite_data.copy()
        satellite_data['initial_conditions']['epoch'] = satellite_data[
            'initial_conditions']['epoch'].strftime(TIME_FORMAT)

        if not ground_track:
            logger.info(
                "No ground track information was found during json conversion."
            )
        j = {
            "scenario": scenario_info,
            "satellite": satellite_data,
            "station": self.station_data,
            "sensor": self.sensor_data,
            "ground_track": ground_track,
            "coverage": coverage
        }
        return json.dumps(j)

    def to_dict(self, **kwargs):
        j = {
            "scenario": self.scenario_info,
            "satellite": self.satellite_data,
            "station": self.station_data,
            "sensor": self.sensor_data,
            "ground_track": self.ground_track,
            "coverage": self.coverage.copy()
        }
        return j

    def update(self, data, **kwargs):
        if kwargs.get("replace"):
            self.report_data = data
            self.coverage = self.report_data
        else:
            self.report_data.append(data)
            self.coverage = self.report_data

    def update_ground_track(self, data, **kwargs):
        if kwargs.get("replace"):
            self.ground_track = data
        else:
            self.ground_track.append(data)

    def plot_flat_map(self, show_locations=True, filled=False):
        data = self.to_dict()
        self.flat_plot = SensorCoverageMapPlot(data, filled=filled)
        self.flat_plot.create()
        if show_locations:
            for s in self.station_data:
                self.flat_plot.add_location(s["latitude"], s["longitude"],
                                            s["name"])
        return self.flat_plot


class EphemerisReport(BaseReport):
    def __init__(self,
                 scenario_info,
                 satellite_data,
                 report_data=None,
                 **kwargs):
        self.satellite_data = satellite_data
        self.report_data = report_data if report_data is not None else []
        self.ephemeris = self.report_data
        self.ephemeris_plot = None

        name = f"{ReportTypes.EPHEMERIS.name} Report {self.get_satellite_name()}"
        super().__init__(type_=ReportTypes.EPHEMERIS,
                         name=name,
                         scenario_info=scenario_info,
                         report_data=self.report_data,
                         **kwargs)

    def get_satellite_data(self):
        return self.satellite_data

    def get_satellite_name(self):
        return self.satellite_data['name']

    def get_satellite_epoch_data(self):
        return self.get_satellite_data()['initial_conditions']

    def export(self, fname="", fpath="", fmt="json"):
        js_data = self.to_json()
        fname = fname if fname else f"Scenario_{self.get_scenario_name()}_{self.name}.{fmt}"
        fpath = fpath if fpath else "simulations/reports"
        with open(fpath + "/" + fname, 'w') as f:
            json.dump(json.loads(js_data), f, indent=4)

    def to_json(self, **kwargs):
        ephemeris = self.ephemeris.copy()
        ephemeris.update({
            "epoch": [e.strftime(TIME_FORMAT) for e in ephemeris.get("epoch")]
        })
        scenario_info = self.scenario_info.copy()
        scenario_info["start"] = scenario_info["start"].strftime(TIME_FORMAT)
        scenario_info["end"] = scenario_info["end"].strftime(TIME_FORMAT)
        satellite_data = self.satellite_data.copy()
        satellite_data['initial_conditions']['epoch'] = satellite_data[
            'initial_conditions']['epoch'].strftime(TIME_FORMAT)

        j = {
            "scenario": scenario_info,
            "satellite": satellite_data,
            "ephemeris": ephemeris
        }
        return json.dumps(j)

    def to_dict(self, **kwargs):
        j = {
            "scenario": self.scenario_info,
            "satellite": self.satellite_data,
            "ephemeris": self.ephemeris
        }
        return j

    def update(self, data, **kwargs):
        if kwargs.get("replace"):
            self.report_data = data
            self.ephemeris = self.report_data
        else:
            self.report_data.append(data)

    def plot_ephemeris(self):
        data = self.to_dict()
        self.ephemeris_plot = OrbitParametersPlot(data)
        self.ephemeris_plot.create()
        return self.ephemeris_plot
