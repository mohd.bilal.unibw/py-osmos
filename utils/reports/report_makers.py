from pylatex import Document
from pylatex import Section
from pylatex import Subsection
from pylatex import Subsubsection
from pylatex import Command
from pylatex import LongTable
from pylatex import Tabular
from pylatex import Table
from pylatex import MultiColumn
from pylatex.utils import NoEscape
from pylatex import Figure
from pylatex import NewPage
from pylatex.utils import bold
from pylatex import Figure
from pylatex import NewPage
from pylatex.utils import italic, NoEscape
from pylatex.base_classes import SpecialOptions
from config import TIME_FORMAT
import matplotlib.pyplot as plt
import numpy as np
import datetime
import copy
import time


class BaseDocument(Document):
    """Base class for Document Maker.
    """
    def __init__(self, **kwargs):
        """Constructor.

        Parameters
        ----------
        kwargs: dict
            all the options that can be passed to the Parent Document class can be passed through kwargs.
        """
        super().__init__(**kwargs)

    def create_document(self, filepath=None, latex=False, pdf=True):
        """The common method to generate document.
        Each class should implement this method.
        """
        raise NotImplementedError(
            "implement the create_document method before calling it.")


class BaseSeRANISDocument(BaseDocument):
    def __init__(self,
                 title,
                 version,
                 author="",
                 person_responsible="",
                 status="Draft",
                 confidentiality="Confidential",
                 orientation='potrait',
                 **kwargs):
        self.title = title
        self.version = version
        self.author = author
        self.status = status
        self.confidentiality = confidentiality
        self.person_responsible = person_responsible or author
        self.compiler = kwargs.get("compiler") or 'pdflatex'
        self.orientation = orientation
        # geometry options to be forwarded to pylatex Document
        self.geometry_options = kwargs.get("geometry_options") or {
            "head": "40pt",
            "top": "2cm",
            "bottom": "2cm",
            "left": "3cm",
            "right": "3cm",
            "includeheadfoot": True
        }
        kwargs.update({"geometry_options": self.geometry_options})

        # pass options to Document
        super().__init__(**kwargs)

        # add options for document information
        self.document_info_table_fmt = NoEscape(
            r"|p{0.3\textwidth}|p{0.3\textwidth}|p{0.3\textwidth}|")
        self.document_info_table = None

        # add options for document version history
        self.version_table_fmt = NoEscape(
            r"|p{0.1\textwidth}|p{0.1\textwidth}|p{0.2\textwidth}|p{0.45\textwidth}|"
        )

        self.version_table_headers = [
            "Version No.", "Date", "Author(s)", "Comments"
        ]
        self.version_table = None

        # create base preamble
        self.documentclass = Command('documentclass',
                                     arguments=['seranis'],
                                     options=[self.orientation])
        self.preamble.append(Command('title', self.title))
        self.preamble.append(Command('date', NoEscape(r'\today')))
        self.append(NoEscape(r'\maketitle'))

        # create document info page
        self.document_info_section = Section("Document Information")
        self._add_document_info_page()
        with self.create(self.document_info_section):
            self.append(NewPage())

        # create version page
        self.version_section = Section("Version History")
        self._add_version_page()
        with self.create(self.version_section):
            self.append(NewPage())

        # create table of contents
        self.append(NoEscape(r'\tableofcontents'))
        self.append(NewPage())

    def _add_version_page(self):
        self.version_table = LongTable(self.version_table_fmt)
        self.version_table.add_hline()
        self.version_table.add_row(self.version_table_headers, mapper=[bold])
        self.version_table.add_hline()
        self.version_table.end_table_header()
        self.version_table.add_hline()
        self.version_section.append(self.version_table)

    def _add_document_info_page(self):
        self.document_info_table = Tabular(self.document_info_table_fmt)
        self.document_info_table.add_hline()
        self.document_info_table.add_row(
            ["Laboratory", "Laboratory Unit", "Laboratory Unit Head"],
            mapper=[bold])
        self.document_info_table.add_hline()
        self.document_info_table.add_row("Mission and System Engineering", 4.0,
                                         "Prof. Dr. Roger Förstner")
        self.document_info_table.add_hline()
        responsible_row = MultiColumn(3,
                                      align="|l|",
                                      data="Person Responsible")
        self.document_info_table.add_row((responsible_row, ))
        self.document_info_table.add_hline()
        self.document_info_table.add_row(
            (MultiColumn(3, align="|l|", data=self.person_responsible), ))
        self.document_info_table.add_hline()
        self.document_info_table.add_row(["Status", "Date", "Author"],
                                         mapper=[bold])
        self.document_info_table.add_hline()
        self.document_info_table.add_row(
            [self.status, NoEscape(r'\today'), self.author])
        self.document_info_table.add_hline()
        self.document_info_table.add_row(
            ["Version", "Confidentiality", "Filename"], mapper=[bold])
        self.document_info_table.add_hline()
        self.document_info_table.add_row(
            [self.version, self.confidentiality, f"{self.title}.pdf"])
        self.document_info_table.add_hline()
        self.document_info_section.append(self.document_info_table)

    def add_version_history(self, version, date, authors, comments):
        self.version_table.add_row([version, date, authors, comments])
        self.version_table.add_hline()

    def create_document(self, filepath=None, latex=False, pdf=True):
        if not pdf and not latex:
            raise Exception(
                "Oops! Not sure what you want. At least one of latex or pdf should be True."
            )
        if pdf:
            self.generate_pdf(filepath=filepath,
                              clean=False,
                              clean_tex=latex,
                              compiler=self.compiler)
        else:
            self.generate_tex(filepath=filepath)

    def set_person_responsible(self, name):
        self.person_responsible = name


class SingleSatMissionReportMaker(BaseSeRANISDocument):
    def __init__(self, scenario, title, version, **kwargs):
        super().__init__(title, version, **kwargs)
        self.scenario = scenario
        # create sections
        self.introduction_section = None
        self.scenario_satellite_section = None
        self.gs_access_section = None
        self.eclipse_section = None
        self.sensor_coverage_section = None
        self.ground_track_section = None
        self.power_section = None
        self.lifetime_section = None

    def _create_introduction_section(self):
        self.introduction_section = Section(f"Introduction")
        with self.create(self.introduction_section):
            self.append(NewPage())

    def _create_scenario_satellite_section(self):
        # add scenario information
        self.scenario_satellite_section = Section("General Information")
        self.scenario_info_table_flt = Table(position="h")
        self.scenario_info_table = Tabular(
            NoEscape(r"|p{0.45\textwidth}|p{0.45\textwidth}|"))
        self.scenario_info_table.add_hline()
        self.scenario_info_table.add_row(
            (MultiColumn(2, align="|l|", data="Scenario Information"), ))
        self.scenario_info_table.add_hline()
        self.scenario_info_table.add_row(
            ["Scenario Name", f"{self.scenario.get_name().replace('_', ' ')}"])
        self.scenario_info_table.add_hline()
        self.scenario_info_table.add_row([
            "Start Time [UTC]",
            f"{self.scenario.start_time.strftime(TIME_FORMAT)}"
        ])
        self.scenario_info_table.add_hline()
        self.scenario_info_table.add_row([
            "End Time [UTC]", f"{self.scenario.end_time.strftime(TIME_FORMAT)}"
        ])
        self.scenario_info_table.add_hline()
        self.scenario_info_table.add_row(
            ["Duration [mins]", f"{self.scenario.duration/60:0.2f}"])
        self.scenario_info_table.add_hline()
        self.scenario_info_table_flt.append(self.scenario_info_table)
        self.scenario_satellite_section.append(self.scenario_info_table_flt)

        # satellite information
        self.satellite_info_table_flt = Table(position="h")
        self.satellite_info_table = Tabular(
            NoEscape(r"|p{0.45\textwidth}|p{0.45\textwidth}|"))
        self.satellite_info_table.add_hline()
        self.satellite_info_table.add_row(
            (MultiColumn(2, align="|l|", data="Satellite Information"), ))
        self.satellite_info_table.add_hline()
        satellite = self.scenario.get_satellites()[0]
        self.satellite_info_table.add_row(["Name", satellite.get_name()])
        self.satellite_info_table.add_hline()
        self.satellite_info_table.add_row(
            ["X-Length [m]", satellite.get_x_length()])
        self.satellite_info_table.add_hline()
        self.satellite_info_table.add_row(
            ["Y-Length [m]", satellite.get_y_length()])
        self.satellite_info_table.add_hline()
        self.satellite_info_table.add_row(
            ["Z-Length [m]", satellite.get_z_length()])
        self.satellite_info_table.add_hline()
        self.satellite_info_table.add_row(["Mass [kg]", satellite.get_mass()])
        self.satellite_info_table.add_hline()
        self.satellite_info_table.add_row(
            ["Drag Coefficient", satellite.drag_coeff])
        self.satellite_info_table.add_hline()
        self.satellite_info_table.add_row([
            NoEscape("Solar Array Area [$m^2$]"),
            f"{satellite.solar_array_area: 0.2f}"
        ])
        self.satellite_info_table.add_hline()
        self.satellite_info_table.add_row(
            ["Solar Array Efficiency", satellite.solar_array_efficiency])
        self.satellite_info_table.add_hline()
        self.satellite_info_table.add_row(
            ["Solar Array Normal", satellite.solar_array_normal])
        self.satellite_info_table.add_hline()
        self.satellite_info_table_flt.append(self.satellite_info_table)
        self.scenario_satellite_section.append(self.satellite_info_table_flt)

        # add orbit epoch information
        initial_conditions = copy.deepcopy(
            satellite.get_initial_keplerian_state())
        self.orbit_ic_table_flt = Table(position="h")
        self.orbit_ic_table = Tabular(
            NoEscape(r"|p{0.45\textwidth}|p{0.45\textwidth}|"))
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table.add_row(
            (MultiColumn(2, align="|l|", data="Orbit Initial Conditions"), ))
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table.add_row([
            "Epoch [UTC]",
            initial_conditions.get("epoch").strftime(TIME_FORMAT)
        ])
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table.add_row(
            ["Semi-major Axis [km]",
             initial_conditions.get("sma") / 1e3])
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table.add_row(
            ["Eccentricity",
             initial_conditions.get("eccentricity")])
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table.add_row([
            NoEscape("Inclination [$^\circ$]"),
            np.rad2deg(initial_conditions.get("inclination"))
        ])
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table.add_row([
            NoEscape("RAAN [$^\circ$]"),
            np.rad2deg(initial_conditions.get("raan"))
        ])
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table.add_row([
            NoEscape("Arugment of Perigee [$^\circ$]"),
            np.rad2deg(initial_conditions.get("perigee"))
        ])
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table.add_row([
            NoEscape("Mean Anomaly [$^\circ$]"),
            np.rad2deg(initial_conditions.get("mean_anomaly"))
        ])
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table.add_row([
            NoEscape("Attitude Law"),
            initial_conditions.get("attitude_law").replace("_", " ")
        ])
        self.orbit_ic_table.add_hline()
        self.orbit_ic_table_flt.append(self.orbit_ic_table)
        self.scenario_satellite_section.append(self.orbit_ic_table_flt)
        with self.create(self.scenario_satellite_section):
            self.append(NewPage())

    def _create_ephemeris_section(self):
        self.ephemeris_section = Section(f"Ephemeris Reports")
        reports = self.scenario.ephemeris_reports
        for report in reports:
            subsection = Subsection(
                f"Ephemeris Report of {report.get_satellite_name()}")
            ephemeris_plot = report.plot_ephemeris()
            f_ep = Figure(position="htbp")
            f_ep.add_plot(width=NoEscape(r'\textwidth'))
            f_ep.add_caption(
                f"Ephemeris Time Series of {report.get_satellite_name()}")
            subsection.append(f_ep)
            self.ephemeris_section.append(f_ep)
        with self.create(self.ephemeris_section):
            self.append(NewPage())

    def _create_gs_access_section(self):
        self.gs_access_section = Section(f"Access Reports")
        access_reports = self.scenario.access_reports
        for report in access_reports:

            subsection = Subsection(
                f"{report.get_satellite_name()} | {report.get_station_name()}")

            # add access data table
            dt_flt = Table(position="h")
            dt = Tabular(NoEscape(r"|p{0.45\textwidth}|p{0.45\textwidth}|"))
            dt.add_hline()
            dt.add_row([
                "Total Access Duration [mins]",
                f"{report.compute_access_duration()/60: 0.2f}"
            ])
            dt.add_hline()
            dt.add_row([
                "Mean Access Duration [mins]",
                f"{report.compute_mean_access()/60: 0.2f}"
            ])
            dt.add_hline()
            dt.add_row([
                "Percentage Access Duration [%]",
                f"{report.compute_fractional_access()*100: 0.2f}"
            ])
            dt.add_hline()
            dt_flt.add_caption(
                f"Access statistics of {report.get_satellite_name()} satellite from {report.get_station_name()} station."
            )
            subsection.append(dt)

            # access bar plot
            report.plot_access_bar()
            f_bar = Figure(position='htbp')
            f_bar.add_plot(width=NoEscape(r'\textwidth'))
            f_bar.add_caption(
                f"Access Bar Plot {report.get_satellite_name()} | {report.get_station_name()} "
            )
            subsection.append(f_bar)

            # access histogram
            report.plot_access_histogram()
            f_hist = Figure(position='htpbp')
            f_hist.add_plot(width=NoEscape(r'\textwidth'))
            f_hist.add_caption(
                f"Access Histogram Plot {report.get_satellite_name()} | {report.get_station_name()} "
            )
            subsection.append(f_hist)

            # add subsection to section
            self.gs_access_section.append(subsection)
        with self.create(self.gs_access_section):
            self.append(NewPage())

    def _create_eclipse_section(self):
        self.eclipse_section = Section(f"Eclipse Reports")
        eclipse_reports = self.scenario.eclipse_reports
        for report in eclipse_reports:
            subsection = Subsection(
                f"Eclipse Report of {report.get_satellite_name()}")

            # add eclipse table
            # add access data table
            dt_flt = Table(position="h")
            dt = Tabular(NoEscape(r"|p{0.45\textwidth}|p{0.45\textwidth}|"))
            dt.add_hline()
            dt.add_row([
                "Total Eclipse Duration [mins]",
                f"{report.compute_eclipse_duration()/60: 0.2f}"
            ])
            dt.add_hline()
            dt.add_row([
                "Mean Eclipse Duration [mins]",
                f"{report.compute_mean_eclipse_duration()/60: 0.2f}"
            ])
            dt.add_hline()
            dt.add_row([
                "Mean Orbit Average Eclipse Duration [min]",
                f"{np.mean(report.compute_orbit_average_eclipse())/60: 0.2f}"
            ])
            dt.add_hline()
            dt.add_row([
                "Percentage Eclipse Duration [%]",
                f"{report.compute_fraction_eclipse_duration()*100: 0.2f}"
            ])
            dt.add_hline()
            dt_flt.add_caption(
                f"Eclipse statistics of {report.get_satellite_name()} satellite."
            )
            subsection.append(dt)

            # eclipse bar plot
            report.plot_eclipse_bar()
            f_bar = Figure(position='htbp')
            f_bar.add_plot(width=NoEscape(r'\textwidth'))
            f_bar.add_caption(
                f"Eclipse Bar Plot {report.get_satellite_name()} ")
            subsection.append(f_bar)

            # eclipse histogram
            report.plot_eclipse_histogram()
            f_hist = Figure(position='htbp')
            f_hist.add_plot(width=NoEscape(r'\textwidth'))
            f_hist.add_caption(
                f"Eclipse Histogram Plot {report.get_satellite_name()}")
            subsection.append(f_hist)

            # add subsection to section
            self.eclipse_section.append(subsection)

        with self.create(self.eclipse_section):
            self.append(NewPage())

    def _create_sensor_coverage_section(self):
        self.sensor_coverage_section = Section(f"Sensor Coverage Report")
        reports = self.scenario.sensor_coverage_reports
        for report in reports:
            subsection = Subsection(
                f"Coverage of {report.get_satellite_name()} | {report.get_sensor_name()}"
            )
            report.plot_flat_map(filled=True)
            f_flat = Figure(position="htbp")
            f_flat.add_plot(width=NoEscape(r'\textwidth'))
            f_flat.add_caption(f"Coverage of {report.get_sensor_name()}")
            subsection.append(f_flat)
            self.sensor_coverage_section.append(subsection)

        with self.create(self.sensor_coverage_section):
            self.append(NewPage())

    def _create_ground_track_section(self):
        self.ground_track_section = Section(f"Ground Track Report")
        reports = self.scenario.ground_track_reports
        for report in reports:
            subsection = Subsection(
                f"Ground Track of {report.get_satellite_name()}")
            report.plot_flat_map()
            f_flat = Figure(position="htbp")
            f_flat.add_plot(width=NoEscape(r'\textwidth'))
            f_flat.add_caption(
                f"2D Ground Track of {report.get_satellite_name()}")
            subsection.append(f_flat)

            # add polar plot
            report.plot_polar_map()
            f_polar = Figure(position="htbp")
            f_polar.add_plot(width=NoEscape(r'\textwidth'))
            f_polar.add_caption(
                f"Polar Ground Track of {report.get_satellite_name()}")
            subsection.append(f_polar)

            # append subsection to section
            self.ground_track_section.append(subsection)

        with self.create(self.ground_track_section):
            self.append(NewPage())

    def _create_power_section(self):
        self.power_section = Section(f"Power Report")

    def _create_lifetime_section(self):
        self.lifetime_section = Section(f"Lifetime Report")

    def _add_subsubsection(self, subsection, subsubsection):
        subsection.append(subsubsection)

    def create_document(self, filepath=None, latex=False, pdf=True):
        self._create_introduction_section()
        self._create_scenario_satellite_section()
        self._create_ephemeris_section()
        self._create_lifetime_section()
        self._create_eclipse_section()
        self._create_power_section()
        self._create_gs_access_section()
        self._create_ground_track_section()
        self._create_sensor_coverage_section()
        super().create_document(filepath, latex, pdf)


class MultiSatMissionReportMaker(BaseSeRANISDocument):
    def __init__(self, scenario, title, version, **kwargs):
        super().__init__(title, version, **kwargs)
        self.scenario = scenario

        # declare sections
        self.introduction_sections = None
        self.gs_access_sections = None
        self.eclipse_sections = None
        self.sensor_coverage_sections = None
        self.ground_track_sections = None
        self.power_sections = None
        self.lifetime_sections = None

        # declare comparative study sections

    def create_report(self):
        pass