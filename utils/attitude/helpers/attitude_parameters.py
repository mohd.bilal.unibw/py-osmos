import numpy as np


def quat2dcm(q0, q):
    q1 = q[0]
    q2 = q[1]
    q3 = q[2]

    return np.array([
        [
            q0**2 + q1**2 - q2**2 - q3**2,
            2 * (q1 * q2 + q0 * q3),
            2 * (q1 * q3 - q0 * q2)
        ],
        [
            2 * (q1 * q2 - q0 * q3),
            q0**2 - q1**2 + q2**2 - q3**2,
            2 * (q2 * q3 + q0 * q1)
        ],
        [
            2 * (q1 * q3 + q0 * q2),
            2 * (q2 * q3 - q0 * q1),
            q0**2 - q1**2 - q2**2 + q3**2
        ],
    ])


def euler_angle_1(theta):
    return np.array([
        [1,              0,             0],
        [0,  np.cos(theta), np.sin(theta)],
        [0, -np.sin(theta), np.cos(theta)]
    ])
    

def euler_angle_3(theta):
    return np.array([
        [ np.cos(theta), np.sin(theta), 0],
        [-np.sin(theta), np.cos(theta), 0],
        [             0,             0, 1]
    ])

