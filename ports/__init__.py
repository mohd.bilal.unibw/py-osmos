import logging
import abc

logger = logging.getLogger(__name__)


class IPort(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def send(self):
        pass

    @abc.abstractmethod
    def receive(self, data):
        pass


class Port(IPort):
    def __init__(self, parent_block, parent_property, *args, **kwargs):
        super().__init__()
        assert hasattr(parent_block, parent_property)
        self.parent_block = parent_block
        self.parent_property = parent_property
        self.name = self.parent_property
        self.connecting_port = None
        self.is_connected = False

    def send(self):
        logger.info("sending data.")

    def receive(self, data):
        logger.info(f"received data: {data}.")
        print(f"received data: {data}.")

    def set_parent_block(self, block):
        self.parent_block = block

    def set_parent_property(self, parent_property):
        self.parent_property = parent_property

    def set_connection(self, port):
        self.connecting_port = port


class InputPort(Port):
    def __init__(self, parent_block, parent_property, *args, **kwargs):
        super().__init__(parent_block, parent_property, *args, **kwargs)
        self.parent_block.add_input_port(self)

    def send(self):
        raise Exception("Input Ports are data receivers."
                        "Cannot send data from Input Port.")

    def receive(self, data):
        super().receive(data)
        self.parent_block.on_receive(self, data)

    def set_connection(self, port):
        assert isinstance(port, OutputPort)
        self.connecting_port = port


class OutputPort(Port):
    def __init__(self, parent_block, parent_property, *args, **kwargs):
        super().__init__(parent_block, parent_property, *args, **kwargs)
        self.parent_block.add_output_port(self)

    def send(self):
        super().send()
        data = getattr(self.parent_block, self.parent_property)
        self.connecting_port.receive(data)

    def receive(self, data):
        raise Exception("Output Ports are data senders."
                        "Cannot receive data on Output Port.")

    def set_connection(self, port):
        assert isinstance(port, InputPort)
        self.connecting_port = port